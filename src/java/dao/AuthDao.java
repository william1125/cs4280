/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class AuthDao {
    private Connection connection;
    
    public AuthDao(){
        connection = DbUtil.getConnection();
    } 

    public User login(String email, String password){

        try{
            String sql = "select * from [USER] where LoginEmail=? and Password=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                User user = new User();
                user.setUserID(rs.getInt("UserID"));
                user.setIsAdmin(rs.getInt("IsAdmin"));
                user.setFirstName(rs.getString("FirstName"));
                user.setLastName(rs.getString("LastName"));
                return user;
            } else {
                return null;
            }
        }   catch (SQLException ex) {
            System.out.println("login SQL Exception: "+ex.getMessage());
        }
        return null;
    }
}
