<%-- 
    Document   : header
    Created on : Apr 13, 2016, 11:45:16 PM
    Author     : Nam
--%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Toy Bear Admin Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
<!--    <link href="css/plugins/morris.css" rel="stylesheet">-->

    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand-logo" href="CategoryController" >
		<div class="logo">
			<img class="media-object" src="images/logo1 smallv2.png" alt="Toy Bear">
		</div>
	    </a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li class="active">
                        <a href="CategoryController?action=list"><i class="fa fa-book"></i> Product Category</a>
                    </li>
                    <li>
                        <a href="ItemController?action=list"><i class="fa fa-list"></i> Product</a>
                    </li>
                    <li>
                        <a href="OrderController?action=list"><i class="fa fa-newspaper-o"></i> Order</a>
                    </li>
                    <li>
                        <a href="RecycleController?action=list"><i class="fa fa-check-square-o"></i> Recycle Toy Application</a>
                    </li>
                    <li>
                        <a href="ReviewController?action=list"><i class="fa fa-comments"></i> Product Comment</a>
                    </li>
                    <li>
                        <a href="TimeSlotController?action=list"><i class="fa fa-calendar"></i> Shipping Timeslot</a>
                    </li>
                    <li>
                        <a href="UserController?action=list"><i class="fa fa-users"></i> User</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
        <link type="text/css" href="css/bootstrap.css" rel="stylesheet"/>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
        <title>ToyBear | Admin Area</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
             Brand and toggle get grouped for better mobile display 
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Brand</a>
            </div>

             Collect the nav links, forms, and other content for toggling 
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class=""><a href="/asgp2/CategoryController?action=list">Category</a></li>  
                <li class=""><a href="/asgp2/UserController?action=list">User</a></li>  
                <li class=""><a href="/asgp2/ItemController?action=list">Toy</a></li>  
                <li class=""><a href="/asgp2/RecycleController?action=list">Recycle Toy Approval</a></li>
                <li class=""><a href="/asgp2/OrderController?action=list">Order</a></li>
                <li class=""><a href="/asgp2/ReviewController?action=list">Review</a></li>
                <li class=""><a href="/asgp2/TimeSlotController?action=list">Time Slot</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Logout</a></li>
              </ul>
            </div> /.navbar-collapse 
          </div> /.container-fluid 
        </nav>-->
