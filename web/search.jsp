<jsp:include page="inc/head.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String scheme = request.getScheme();
	String serverName = request.getServerName();
	int serverPort = request.getServerPort();
	String contextPath = request.getContextPath();  // includes leading forward slash

	String resultPath = scheme + "://" + serverName + ":" + serverPort;

%>
<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> Product		    
		</p>

		<div class="category-form">

			<div class="col-sm-3">
				<p ></p>
				<form action="#" class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label">Categories</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="categories?action=list&cid=10">Boys</a><br>
								<a href="categories?action=list&cid=9">Girls</a><br>
								<a href="categories?action=list&cid=11">Baby</a><br>
								<a href="categories?action=list&cid=12">Toddler</a><br>
								<a href="categories?action=list&cid=13">Kids</a><br>
								<a href="categories?action=list&cid=14">Teenager</a><br>
								<a href="categories?action=list&cid=15">Action Figures</a><br>
								<a href="categories?action=list&cid=16">Dolls</a><br>
								<a href="categories?action=list&cid=17">Games &amp; Puzzle</a><br>
								<a href="categories?action=list&cid=18">LEGO</a><br>
								<a href="categories?action=list&cid=19">Plush</a><br>
								<a href="categories?action=list&cid=20">Play Sets</a></p>
					</fieldset>
				</form>
			</div>

			<div class="col-sm-9">
				<div>
					<div class="category-bar ">
						<p ><c:out value="${itemNum}"/> items</p>
					</div>  
				</div>
				<div role="tabpanel" class="" id="tab1">
					<div class="">
						<div class="product-listing" style="padding-top:0px">
							<ul class="cp-product-list row">
								<c:forEach items="${products}" var="product">
									<li class="col-md-4 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${product.itemID}"><img alt="" src="<%= resultPath%>/data/${product.itemImageURL}"></a></div>
											<div class="text">
												<!--QUESTION HERE same as index-->
												<div class="pro-name">
													<h4>${product.itemName}</h4>
													<p class="price">$<c:out value="${product.price}"/></p>
												</div>
												<!--<c:set var="description" value="${product.fullDescription}"/>
												<p><c:out value="${fn:substring(description, 0, 30)}..."/></p>-->
											</div>
											<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a>
												<div class="rating"> 
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i>
													<i class="fa fa-star-o"></i> 
												</div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>              
						</div>
					</div>
				</div>
			</div>  
		</div>        
	</div>
</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>