/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Nam
 */
public class Order {
    private int OrderID;
    private int UserID;
    private String Username;
    private int TimeSlotID;
    private String FromTime;
    private String ToTime;
    private String OrderStatus;
    private Date OrderDate;
    private Date DeliveryDate;
    private String BankName;
    private String AccountNumber;
    private String AccountHolder;
    private String ShippingAddress;
    private float TotalAmount;
    /**
     * @return the OrderID
     */
    public int getOrderID() {
        return OrderID;
    }

    /**
     * @param OrderID the OrderID to set
     */
    public void setOrderID(int OrderID) {
        this.OrderID = OrderID;
    }

    /**
     * @return the UserID
     */
    public int getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the TimeSlotID
     */
    public int getTimeSlotID() {
        return TimeSlotID;
    }

    /**
     * @param TimeSlotID the TimeSlotID to set
     */
    public void setTimeSlotID(int TimeSlotID) {
        this.TimeSlotID = TimeSlotID;
    }

    /**
     * @return the OrderStatus
     */
    public String getOrderStatus() {
        return OrderStatus;
    }

    /**
     * @param OrderStatus the OrderStatus to set
     */
    public void setOrderStatus(String OrderStatus) {
        this.OrderStatus = OrderStatus;
    }

    /**
     * @return the OrderDate
     */
    public Date getOrderDate() {
        return OrderDate;
    }

    /**
     * @param OrderDate the OrderDate to set
     */
    public void setOrderDate(Date OrderDate) {
        this.OrderDate = OrderDate;
    }

    /**
     * @return the DeliveryDate
     */
    public Date getDeliveryDate() {
        return DeliveryDate;
    }

    /**
     * @param DeliveryDate the DeliveryDate to set
     */
    public void setDeliveryDate(Date DeliveryDate) {
        this.DeliveryDate = DeliveryDate;
    }

    /**
     * @return the BankName
     */
    public String getBankName() {
        return BankName;
    }

    /**
     * @param BankName the BankName to set
     */
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    /**
     * @return the AccountNumber
     */
    public String getAccountNumber() {
        return AccountNumber;
    }

    /**
     * @param AccountNumber the AccountNumber to set
     */
    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    /**
     * @return the AccountHolder
     */
    public String getAccountHolder() {
        return AccountHolder;
    }

    /**
     * @param AccountHolder the AccountHolder to set
     */
    public void setAccountHolder(String AccountHolder) {
        this.AccountHolder = AccountHolder;
    }

    /**
     * @return the Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username the Username to set
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return the FromTime
     */
    public String getFromTime() {
        return FromTime;
    }

    /**
     * @param FromTime the FromTime to set
     */
    public void setFromTime(String FromTime) {
        this.FromTime = FromTime;
    }

    /**
     * @return the ToTime
     */
    public String getToTime() {
        return ToTime;
    }

    /**
     * @param ToTime the ToTime to set
     */
    public void setToTime(String ToTime) {
        this.ToTime = ToTime;
    }

	/**
	 * @return the ShippingAddress
	 */
	public String getShippingAddress() {
		return ShippingAddress;
	}

	/**
	 * @param ShippingAddress the ShippingAddress to set
	 */
	public void setShippingAddress(String ShippingAddress) {
		this.ShippingAddress = ShippingAddress;
	}

	/**
	 * @return the TotalAmount
	 */
	public float getTotalAmount() {
		return TotalAmount;
	}

	/**
	 * @param TotalAmount the TotalAmount to set
	 */
	public void setTotalAmount(float TotalAmount) {
		this.TotalAmount = TotalAmount;
	}
}
