/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import model.Review;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class ReviewDao {
    private Connection connection;
    
    public ReviewDao(){
        connection = DbUtil.getConnection();
    }
    
    public void addReview(Review review) {
        try{
            String sql = "insert into [REVIEW](ItemID, UserID, ReplyID, CreatedDate, Title, Content, ItemRating, ReviewImageURL) values(?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, review.getItemID());
            preparedStatement.setInt(2, review.getUserID());
            if(review.getReplyID() == 0)
                preparedStatement.setNull(3, java.sql.Types.INTEGER);
            else    
                preparedStatement.setInt(3, review.getReplyID());
            java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
            preparedStatement.setDate(4, timeNow);
            preparedStatement.setString(5, review.getTitle());
            preparedStatement.setString(6, review.getContent());
            preparedStatement.setFloat(7, review.getItemRating());
            preparedStatement.setString(8, review.getReviewImageURL());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("addReview SQL Exception: "+ex.getMessage());        
        }
    }

    public void deleteReview(int reviewID) {
        try {
            String sql = "delete from [REVIEW] where ReviewID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, reviewID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("deleteReview SQL Exception: "+ex.getMessage());
        }
    }

    public void updateReview(Review review) {
        try {
            String sql = "update [REVIEW] set  ItemID=?, UserID=?, ReplyID=?, Title=?, Content=?, ItemRating=?, ReviewImageURL=? where ReviewID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, review.getItemID());
            preparedStatement.setInt(2, review.getUserID());
            if(review.getReplyID() == 0)
                preparedStatement.setNull(3, java.sql.Types.INTEGER);
            else    
                preparedStatement.setInt(3, review.getReplyID());
            preparedStatement.setString(4, review.getTitle());
            preparedStatement.setString(5, review.getContent());
            preparedStatement.setFloat(6, review.getItemRating());
            preparedStatement.setString(7, review.getReviewImageURL());
            preparedStatement.setInt(8, review.getReviewID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("updateReview SQL Exception: "+ex.getMessage());
        }
    }

    public List<Review> getAllReviews(){
        List<Review> reviews = new ArrayList<Review>();
        try{
            Statement statement = connection.createStatement();
            String sql = "select [REVIEW].*, [USER].FirstName, [USER].LastName, [ITEM].itemName from [REVIEW]"
                    +" inner join [USER] on [USER].UserID=[REVIEW].UserID"
                    +" inner join [ITEM] on [ITEM].ItemID=[REVIEW].ItemID";
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()){
                Review review = new Review();
                review.setReviewID(rs.getInt("ReviewID"));
                review.setItemID(rs.getInt("ItemID"));
                review.setItemName(rs.getString("ItemName"));
                review.setUserID(rs.getInt("UserID"));
                review.setFirstName(rs.getString("FirstName"));
                review.setLastName(rs.getString("LastName"));
                review.setReplyID(rs.getInt("ReplyID"));
                review.setCreatedDate(rs.getDate("CreatedDate"));
                review.setTitle(rs.getString("Title"));
                review.setContent(rs.getString("Content"));
                review.setItemRating(rs.getFloat("ItemRating"));
                review.setReviewImageURL(rs.getString("ReviewImageURL"));
                reviews.add(review);
            }
        } catch (SQLException ex) {
            System.out.println("getAllReviews SQL Exception: "+ex.getMessage());
        }
        return reviews;
    }

        public Review getReviewById(int reviewID){
        Review review = new Review();
        try{
            String sql = "select [REVIEW].*, [USER].FirstName, [USER].LastName, [ITEM].itemName from [REVIEW]"
                    +" inner join [USER] on [USER].UserID=[REVIEW].UserID"
                    +" inner join [ITEM] on [ITEM].ItemID=[REVIEW].ItemID"
                    +" where [REVIEW].ReviewID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, reviewID);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                review.setReviewID(rs.getInt("ReviewID"));
                review.setItemID(rs.getInt("ItemID"));
                review.setItemName(rs.getString("ItemName"));
                review.setUserID(rs.getInt("UserID"));
                review.setFirstName(rs.getString("FirstName"));
                review.setLastName(rs.getString("LastName"));
                review.setReplyID(rs.getInt("ReplyID"));
                review.setCreatedDate(rs.getDate("CreatedDate"));
                review.setTitle(rs.getString("Title"));
                review.setContent(rs.getString("Content"));
                review.setItemRating(rs.getFloat("ItemRating"));
                review.setReviewImageURL(rs.getString("ReviewImageURL"));
            }
        }   catch (SQLException ex) {
            System.out.println("getReviewById SQL Exception: "+ex.getMessage());
        }
        return review;
    }      

	public ArrayList<Review> getAllReviewsByItemID(int productID) {
        ArrayList<Review> reviews = new ArrayList<Review>();
        try{
 
            String sql = "select [REVIEW].*, [USER].FirstName, [USER].LastName, [ITEM].itemName from [REVIEW]"
                    +" inner join [USER] on [USER].UserID=[REVIEW].UserID"
                    +" inner join [ITEM] on [ITEM].ItemID=[REVIEW].ItemID"
				+" where [REVIEW].ItemID=?";
		              PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,productID);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                Review review = new Review();
                review.setReviewID(rs.getInt("ReviewID"));
                review.setItemID(rs.getInt("ItemID"));
                review.setItemName(rs.getString("ItemName"));
                review.setUserID(rs.getInt("UserID"));
                review.setFirstName(rs.getString("FirstName"));
                review.setLastName(rs.getString("LastName"));
                review.setReplyID(rs.getInt("ReplyID"));
                review.setCreatedDate(rs.getDate("CreatedDate"));
                review.setTitle(rs.getString("Title"));
                review.setContent(rs.getString("Content"));
                review.setItemRating(rs.getFloat("ItemRating"));
                review.setReviewImageURL(rs.getString("ReviewImageURL"));
                reviews.add(review);
            }
        } catch (SQLException ex) {
            System.out.println("getAllReviews SQL Exception: "+ex.getMessage());
        }
        return reviews;
	}
}
