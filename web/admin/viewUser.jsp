<jsp:include page="../inc/header.jsp"/>
<jsp:useBean id="user" type="model.User" scope="request"/>
<div id="page-wrapper">

	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					View User
				</h3>
				<ol class="breadcrumb">

					<li class="active">
						<i class="fa fa-users"></i> <a href="UserController?action=list">User</a>
					</li>
					<li class="active">
						<i class="fa fa-search-plus"></i> View User
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='UserController' class="form-horizontal">
			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Login Information</legend>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Login Email : </label>
					<div class="col-lg-5">
						<input disabled name="loginEmail" type="email" class="form-control" value="<%=user.getLoginEmail()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Password : </label>
					<div class="col-lg-5">
						<input disabled name="password" type="password" class="form-control">
					</div>
				</div>
			</fieldset>			 
			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Default Payment Method</legend>

				<div class="form-group">
					<label class="control-label col-lg-4 insert">Bank Name : </label>
					<div class="col-lg-5">
						<select disabled name="defaultBankName" class="form-control">
							<option value="Bank of China (Hong Kong)" <% if (user.getDefaultBankName().equalsIgnoreCase("Bank of China (Hong Kong)")) {%>selected<%}%>>Bank of China (Hong Kong)</option>
							<option value="Bank of East Asia" <% if (user.getDefaultBankName().equalsIgnoreCase("Bank of East Asia")) {%>selected<%}%>>Bank of East Asia</option>
							<option value="Hang Seng Bank" <% if (user.getDefaultBankName().equalsIgnoreCase("Hang Seng Bank")) {%>selected<%}%>>Hang Seng Bank</option>
							<option value="Hongkong and Shanghai Banking Corporation" <% if (user.getDefaultBankName().equalsIgnoreCase("Hongkong and Shanghai Banking Corporation")) {%>selected<%}%>>Hongkong and Shanghai Banking Corporation</option>
							<option value="Industrial and Commercial Bank of China (Asia)" <% if (user.getDefaultBankName().equalsIgnoreCase("Industrial and Commercial Bank of China (Asia)")) {%>selected<%}%>>Industrial and Commercial Bank of China (Asia)</option>
							<option value="Standard Chartered Bank (Hong Kong)" <% if (user.getDefaultBankName().equalsIgnoreCase("Standard Chartered Bank (Hong Kong)")) {%>selected<%}%>>Standard Chartered Bank (Hong Kong)</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Account Number : </label>
					<div class="col-lg-5">
						<input disabled name="defaultAccountNumber" type="number" class="form-control" value="<%=user.getDefaultAccountNumber()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Account Holder : </label>
					<div class="col-lg-5">
						<input disabled name="defaultAccountHolder" type="text" class="form-control" value="<%=user.getDefaultAccountHolder()%>">
					</div>
				</div>
			</fieldset>

			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Personal Information</legend>		   

				<div class="form-group">
					<label class="control-label col-lg-4 insert">Last Name : </label>
					<div class="col-lg-5">
						<input disabled name="lastName" type="text" class="form-control" value="<%= user.getLastName()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">First Name : </label>
					<div class="col-lg-5">
						<input disabled name="firstName" type="text" class="form-control" value="<%= user.getFirstName()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Gender : </label>
					<div class="col-lg-5">
						<select disabled name="gender" class="form-control">
							<option value="M" <% if (user.getGender().equalsIgnoreCase("M")) {%> selected <% } %>>Male</option>
							<option value="F" <% if (user.getGender().equalsIgnoreCase("F")) {%> selected <% }%>>Female</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Contact Number : </label>
					<div class="col-lg-5">
						<input disabled name="contactNumber" type="number" class="form-control" value="<%= user.getContactNumber()%>">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-4 insert">Shipping Address 1 : </label>
					<div class="col-lg-5">
						<input disabled name="shippingAddress1" type="text" class="form-control" value="<%= user.getShippingAddress1()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Shipping Address 2 : </label>
					<div class="col-lg-5">
						<input disabled name="shippingAddress2" type="text" class="form-control" value="<%= user.getShippingAddress2()%>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Is Admin : </label>
					<div class="col-lg-5">
						<select disabled name="isAdmin" class="form-control">
							<option value="0" <% if (user.getIsAdmin() == 0) {%> selected <% } %>>No</option>
							<option value="1" <% if (user.getIsAdmin() == 1) {%> selected <% }%>>Yes</option>
						</select>
					</div>
				</div>
			</fieldset>
			<input type="hidden" name="userID" value="<%=user.getUserID()%>">
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-5">

					<a class="btn btn-default col-lg-3 save-btn" href="UserController?action=list" role="button">Back</a>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>



