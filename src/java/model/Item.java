package model;


import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nam
 */
public class Item {
    private int ItemID;
    private int UserID;
    private String ItemName;
    private double Price;
    private int AvailableQuantity;
    private String FullDescription;
    private String MagicDescription;
    private String BareNecessities;
    private String DeliveryDetail;
    private String ItemImageURL;
    private Date CreatedDate;
    private String State;
    private String ItemType;
    private String Color;
    private float Width;
    private float Height;
    private String Material;

    /**
     * @return the ItemID
     */
    public int getItemID() {
        return ItemID;
    }

    /**
     * @param ItemID the ItemID to set
     */
    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    /**
     * @return the UserID
     */
    public int getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the ItemName
     */
    public String getItemName() {
        return ItemName;
    }

    /**
     * @param ItemName the ItemName to set
     */
    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

    /**
     * @return the Price
     */
    public double getPrice() {
        return Price;
    }

    /**
     * @param Price the Price to set
     */
    public void setPrice(double Price) {
        this.Price = Price;
    }

    /**
     * @return the AvailableQuantity
     */
    public int getAvailableQuantity() {
        return AvailableQuantity;
    }

    /**
     * @param AvailableQuantity the AvailableQuantity to set
     */
    public void setAvailableQuantity(int AvailableQuantity) {
        this.AvailableQuantity = AvailableQuantity;
    }

    /**
     * @return the FullDescription
     */
    public String getFullDescription() {
        return FullDescription;
    }

    /**
     * @param FullDescription the FullDescription to set
     */
    public void setFullDescription(String FullDescription) {
        this.FullDescription = FullDescription;
    }

    /**
     * @return the MagicDescription
     */
    public String getMagicDescription() {
        return MagicDescription;
    }

    /**
     * @param MagicDescription the MagicDescription to set
     */
    public void setMagicDescription(String MagicDescription) {
        this.MagicDescription = MagicDescription;
    }

    /**
     * @return the BareNecessities
     */
    public String getBareNecessities() {
        return BareNecessities;
    }

    /**
     * @param BareNecessities the BareNecessities to set
     */
    public void setBareNecessities(String BareNecessities) {
        this.BareNecessities = BareNecessities;
    }

    /**
     * @return the DeliveryDetail
     */
    public String getDeliveryDetail() {
        return DeliveryDetail;
    }

    /**
     * @param DeliveryDetail the DeliveryDetail to set
     */
    public void setDeliveryDetail(String DeliveryDetail) {
        this.DeliveryDetail = DeliveryDetail;
    }

    /**
     * @return the ItemImageURL
     */
    public String getItemImageURL() {
        return ItemImageURL;
    }

    /**
     * @param ItemImageURL the ItemImageURL to set
     */
    public void setItemImageURL(String ItemImageURL) {
        this.ItemImageURL = ItemImageURL;
    }

    /**
     * @return the CreatedDate
     */
    public Date getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate the CreatedDate to set
     */
    public void setCreatedDate(Date CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     * @return the State
     */
    public String getState() {
        return State;
    }

    /**
     * @param State the State to set
     */
    public void setState(String State) {
        this.State = State;
    }

    /**
     * @return the ItemType
     */
    public String getItemType() {
        return ItemType;
    }

    /**
     * @param ItemType the ItemType to set
     */
    public void setItemType(String ItemType) {
        this.ItemType = ItemType;
    }

    /**
     * @return the Color
     */
    public String getColor() {
        return Color;
    }

    /**
     * @param Color the Color to set
     */
    public void setColor(String Color) {
        this.Color = Color;
    }

    /**
     * @return the Width
     */
    public float getWidth() {
        return Width;
    }

    /**
     * @param Width the Width to set
     */
    public void setWidth(float Width) {
        this.Width = Width;
    }

    /**
     * @return the Height
     */
    public float getHeight() {
        return Height;
    }

    /**
     * @param Height the Height to set
     */
    public void setHeight(float Height) {
        this.Height = Height;
    }

    /**
     * @return the Material
     */
    public String getMaterial() {
        return Material;
    }

    /**
     * @param Material the Material to set
     */
    public void setMaterial(String Material) {
        this.Material = Material;
    }
}
