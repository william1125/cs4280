/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.TimeSlot;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class TimeSlotDao {
    private Connection connection;
    
    public TimeSlotDao(){
        connection = DbUtil.getConnection();
    }

    public void addTimeSlot(TimeSlot timeSlot) {
        try{
            String sql = "insert into [TIMESLOT] (FromTime, ToTime) values(?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, timeSlot.getFromTime());
            preparedStatement.setString(2, timeSlot.getToTime());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("addTimeSlot SQL Exception: "+ex.getMessage());        
        }
    } 
    
    public void deleteTimeSlot(int timeSlotID) {
        try {
            String sql = "delete from [TIMESLOT] where TimeSlotID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, timeSlotID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("deleteTimeSlot SQL Exception: "+ex.getMessage());
        }
    }

    public void updateTimeSlot(TimeSlot timeSlot) {
        try {
            String sql = "update [TIMESLOT] set FromTime=?, ToTime=? where TimeSlotID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, timeSlot.getFromTime());
            preparedStatement.setString(2, timeSlot.getToTime());
            preparedStatement.setInt(3, timeSlot.getTimeSlotID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("updateTimeSlot SQL Exception: "+ex.getMessage());
        }
    }
    public List<TimeSlot> getAllTimeSlots(){
        List<TimeSlot> timeSlots = new ArrayList<TimeSlot>();
        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from [TIMESLOT]");
            while(rs.next()){
                TimeSlot timeSlot = new TimeSlot();
                timeSlot.setTimeSlotID(rs.getInt("TimeSlotID"));
                timeSlot.setFromTime(rs.getString("FromTime"));
                timeSlot.setToTime(rs.getString("ToTime"));
                timeSlots.add(timeSlot);
            }
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
        return timeSlots;
    }

    public TimeSlot getTimeSlotById(int timeSlotID){
        TimeSlot timeSlot = new TimeSlot();
        try{
            String sql = "select * from [TIMESLOT] where TimeSlotID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, timeSlotID);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                timeSlot.setTimeSlotID(rs.getInt("TimeSlotID"));
                timeSlot.setFromTime(rs.getString("FromTime"));
                timeSlot.setToTime(rs.getString("ToTime"));
            }
        } catch (SQLException ex) {
            System.out.println("getTimeSlotById SQL Exception: "+ex.getMessage());
        }
        return timeSlot;
    }
}
