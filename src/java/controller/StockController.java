/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.StockDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Stock;

/**
 *
 * @author Nam
 */
public class StockController extends HttpServlet {

    private static String INSERT = "admin/insertStock.jsp";
    private static String EDIT = "admin/editStock.jsp";
    private static String LIST = "admin/listStock.jsp";
    private static String VIEW = "admin/viewStock.jsp";
    private StockDao stockDAO;

    public StockController() {
        super();
        stockDAO = new StockDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("delete")) {
                int itemID = Integer.parseInt(request.getParameter("itemID"));
                int stockID = Integer.parseInt(request.getParameter("stockID"));
                stockDAO.deleteStock(stockID);
                forward = LIST;
                request.setAttribute("stocks", stockDAO.getAllStocksByItemId(itemID));
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                int stockID = Integer.parseInt(request.getParameter("stockID"));
//		   int itemID = Integer.parseInt(request.getParameter("itesmID"));
                Stock stock = stockDAO.getStockById(stockID);
                request.setAttribute("stock", stock);

            } else if (action.equalsIgnoreCase("view")) {
                forward = VIEW;
                int stockID = Integer.parseInt(request.getParameter("stockID"));
//		   int itemID = Integer.parseInt(request.getParameter("itesmID"));
                Stock stock = stockDAO.getStockById(stockID);
                request.setAttribute("stock", stock);

            } else if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                int itemID = Integer.parseInt(request.getParameter("itemID"));
                request.setAttribute("itemID", itemID);
                request.setAttribute("stocks", stockDAO.getAllStocksByItemId(itemID));
            } else {
                forward = INSERT;
                int itemID = Integer.parseInt(request.getParameter("itemID"));
                request.setAttribute("itemID", itemID);
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Stock stock = new Stock();
        stock.setItemID(Integer.parseInt(request.getParameter("itemID")));
        if (request.getParameter("orderID") != null) {
            stock.setOrderID(Integer.parseInt(request.getParameter("orderID")));
        }
        if (request.getParameter("serialNumber") != null) {
            stock.setSerialNumber(request.getParameter("serialNumber"));
        }
        if (request.getParameter("salesStatus") != null) {
            stock.setSalesStatus(request.getParameter("salesStatus"));
        }
        stock.setRemarks(request.getParameter("remarks"));

        int amount = request.getParameter("amount") != null ? Integer.parseInt(request.getParameter("amount")) : 0;
        String stockID = request.getParameter("stockID");
        if (stockID == null || stockID.isEmpty()) {
            System.out.println("---POST: Insert---");
            for (int i = 0; i < amount; i++) {
                Stock newStock = new Stock();
                newStock.setItemID(stock.getItemID());
                newStock.setSerialNumber(UUID.randomUUID().toString());
                newStock.setSalesStatus("Available");
                stockDAO.addStock(newStock);
            }
        } else {
            System.out.println("---POST: update---");
            stock.setStockID(Integer.parseInt(stockID));
            stockDAO.updateStock(stock);
        }
	   response.sendRedirect("StockController?action=list&itemID="+request.getParameter("itemID"));
//        RequestDispatcher view = request.getRequestDispatcher(LIST);
//        request.setAttribute("stocks", stockDAO.getAllStocksByItemId(stock.getItemID()));
//        view.forward(request, response);
    }
}
