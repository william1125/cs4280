<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Toy Bear</title>
		<!-- Style Sheet Files -->
		<link href="css/custom.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/jquery.bxslider.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/color.css" rel="stylesheet">
		<link href="css/owl.carousel.css" rel="stylesheet">

		<!--[if lt IE 9]>
			    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		    <![endif]-->
	</head>
	<body>

		<!--Wrapper Start-->
		<div id="child-care" class="wrapper index"> 

			<!--Header Start-->
			<header id="cp-child-header" class="cp-child-header">
				<div class="cp-child-topbar">
					<div class="container"> 
						<!--SELECT LANGUAGE START-->
						<div class="theme-language-currency">
							<!--QUESTION HERE insert name when logined and logout use head-logout-->
							<p style="color: rgb(135,135,135);font-size: 14px;font-family: Arial;margin: 2px;">Welcome,please enjoy the shopping!</p>
						</div>
						<!--SELECT LANGUAGE END--> 
						<div class="cart-menu" style="padding-top: 4px;">
							<ul >
								<li><a href="profile" style="color: rgb(135,135,135);font-size: 13px;font-family: Arial;margin: 2px;">My Account</a></li>
								<li><a href="cart" style="color: rgb(135,135,135);font-size: 13px;font-family: Arial;margin: 2px;">Shopping Cart</a></li>
									<% if (session.getAttribute("userid") != null) { %>
								<li><a href="logout" style="color: rgb(135,135,135);font-size: 13px;font-family: Arial;margin: 2px;">Logout</a></li>
									<% } else { %>
								<li><a href="login" style="color: rgb(135,135,135);font-size: 13px;font-family: Arial;margin: 2px;">Login</a></li>
									<% }%>
							</ul>
						</div>
					</div>
				</div>
				<div class="logo-nav">
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<nav class="navbar navbar-default main-nav">
									<div class="navbar-header">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
										<a class="logo" href="LandingController"><img src="images/logo1.png" alt="" height="71" width="210"></a> </div>
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<ul class="nav navbar-nav menu">
											<li class="m1 dropdown"> <a href="product-category.jsp">Product</span></a>
											</li>
											<li class="m2 dropdown"> <a href="categories?action=list&cid=9">Girls</span></a>
											</li>
											<li class="m3 dropdown"> <a href="categories?action=list&cid=10">Boys</span></a>
											</li>
											<li class="m4 dropdown"> <a href="categories?action=list&cid=11">Baby</span></a>
											</li>
											<li class="m5 dropdown"> <a href="categories?action=list&cid=14">Teen</span></a>
											</li>
											<li class="m6"><a href="categories?action=list&cid=22">&nbsp;</a></li>
										</ul>
									</div>
								</nav>
							</div>
							<div class="col-md-3 ">
								<div class="login-container small-layout">
									<div class="cart-option">
										<div  style="margin-right:15px;"> 
											<!--QUESTION HERE cart items count included like review in product detail-->
											<a id="cart" href="cart"  class="small-layout"><img src="images/cart-icon.png" alt=""><p>Cart items </p></a>
										</div>
									</div>
									<div class="search-panel">
										<div  style="margin-right:15px;">
											<a href="order" class="small-layout"><img src="images/fav-icon.png" alt="" style="margin-left: 10px;"><p style="color: #AC9CAD;font-size: 14px;font-family: Calibri, 'Courgette', cursive; ">My Orders</p></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!--Header End--> 

			<!-- Search Bar Start -->
			<div id="search-bar">
				<div class="container">
					<form class="form-horizontal" method="GET" action="search">
						<div class="row">
							<label class="col-md-3 control-label ">
								<div style="padding-right: 0">
									<!--QUESTION HERE searching-->
									<p style="color: rgb(135,135,135);font-size: 14px;font-family: Arial; margin: 2px"  class="small-layout">
										<img src="images/search-icon.png" class="search-icon" style="width:25px; height: 25px;">Project Search : 
									</p>
								</div>
							</label>
							<div class="col-md-5">
								<input type="text" name="keywords" class="col-lg-12 small-layout" style="font-family: Arial; margin: 4px" ></input>
							</div>
							<input type="submit" value="Go !" class="btn-sm btn-info btn-lg  small-layout" class="col-md-1">  
						</div>
					</form>
				</div>
			</div>
			<!-- Search Bar End -->
