<!--Partner Logo Slider-->
	<div class="partner-logos">
		<div class="plogo-slider">
			<div class="item"><img src="images/plogo1.jpg" alt=""></div>
			<div class="item"><img src="images/plogo2.jpg" alt=""></div>
			<div class="item"><img src="images/plogo3.jpg" alt=""></div>
			<div class="item"><img src="images/plogo4.jpg" alt=""></div>
			<div class="item"><img src="images/plogo5.jpg" alt=""></div>
			<div class="item"><img src="images/plogo6.jpg" alt=""></div>
			<div class="item"><img src="images/plogo1.jpg" alt=""></div>
			<div class="item"><img src="images/plogo2.jpg" alt=""></div>
			<div class="item"><img src="images/plogo3.jpg" alt=""></div>
			<div class="item"><img src="images/plogo4.jpg" alt=""></div>
			<div class="item"><img src="images/plogo5.jpg" alt=""></div>
			<div class="item"><img src="images/plogo6.jpg" alt=""></div>
		</div>
	</div>
	<!--Partner Logo Slider End--> 

</div>

<!--Footer Start-->
<div id="footer" class="cp-footer">
	<div class="footer-top">
		<div class="container">
			<div class="row" style="margin-left: 30px">
				<div class="col-md-3" style="margin-right: 30px">
					<div class="widget_text" >
						<h3 >INFORMATION</h3>
						<p style="margin:0px"><a>&gt;About Us</a></p>
						<p style="margin:0px"><a>&gt;Terms and Conditions</a></p>
						<p style="margin:0px"><a>&gt;Privacy Policy</a></p>
					</div>
				</div>
				<div class="col-md-2"style="margin-right: 110px">
					<div class="widget_text">
						<h3>CATEGORIES</h3>
						<p style="margin:0px"><a>&gt;Action Figures</a></p>
						<p style="margin:0px"><a>&gt;Dolls</a></p>
						<p style="margin:0px"><a>&gt;Games &amp; Puzzle</a></p>
						<p style="margin:0px"><a>&gt;LEGO</a></p>
						<p style="margin:0px"><a>&gt;Plush</a></p>
						<p style="margin:0px"><a>&gt;Play Sets</a></p>
						<p style="margin:0px"><a>&gt;Recycle Toy</a></p>
					</div>
				</div>
				<div class="col-md-3"style="margin-right: 20px">
					<div class="widget_text">
						<h3>ENQUIRIES</h3>
						<p style="margin:0px"><a>&gt;Shipping Information</a></p>
						<p style="margin:0px"><a>&gt;Return &amp; Exchange</a></p>
					</div>
				</div>
				<div class="col-md-2">
					<div class="widget_text">
						<h3>MY ACCOUNT</h3>
						<p style="margin:0px"><a>&gt;My Account</a></p>
						<p style="margin:0px"><a>&gt;Order History</a></p>
						<p style="margin:0px"><a>&gt;Wishlist</a></p>
						<p style="margin:0px"><a>&gt;Shopping Cart</a></p>
					</div>            
				</div>
			</div>
		</div>
	</div>
</div>
<!--Footer End--> 

</div>
<!--Wrapper End--> 
<!-- JS Files --> 
<script src="js/jquery.min.js"></script><!-- Jquery File --> 
<script src="js/jquery-migrate-1.2.1.min.js"></script> <!-- Jquery Migrate --> 
<script src="js/bootstrap.min.js"></script> <!-- Bootstrap --> 
<script src="js/jquery.easing.1.3.js"></script> <!-- easing --> 
<script src="js/jquery.bxslider.min.js"></script> <!-- Bx Slider --> 
<script src="js/owl.carousel.min.js"></script> <!-- easing --> 
<script src="js/custom.js"></script> <!-- easing -->
<script src="js/recycle.js"></script>
</body>
</html>
