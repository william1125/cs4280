jQuery(document).ready(function ($) {
               var category_list = "";
//        recycle
        $("img.category-img").click(function(e){
           var img = $(this);
           var value = $(this).attr('target');
           if($(this).hasClass("active")){
               $(this).css({opacity:0.5});
               $(this).removeClass("active");
               var string = $(this).attr("cat")+",";
               category_list = category_list.replace(string, "");
           } else {
               $(this).css({opacity:1});
               $(this).addClass("active");
               category_list += $(this).attr('cat')+',';
           }
           console.log(category_list)
           $("input[name=categories]").each(function(i){
               if(this.value == value){
                   this.checked = this.checked?false:true;
                   this.checked = this.checked?$(this).prop("checked", false):$(this).prop("checked", true);
               }
           });
        });
        
    $('.next-btn').on('click', function (e) {
        e.preventDefault();
        var nextPage = $(this).attr('step');
        $("body, html").animate({
            scrollTop: $('#recycle-form-top').offset().top
        }, 600);
        $('.steps').hide();
        $('.steps[target=' + nextPage + ']').show();
        if(nextPage == 3){
            $('#review-product-name').empty().text($('input[name=itemName]').val());
            $("#review-color").empty().text($("select[name=color]").val());
            $('#review-size').empty().text($('input[name=width]').val()+"cm x "+$("input[name=height]").val()+"cm");
            $('#review-material').empty().text($('input[name=material]').val());
            $('#review-description').empty().text($("textarea[name=fullDescription]").val());
            console.log($("input[name=price").val())
            $("#review-price").empty().text("$ "+$("input[name=price]").val());
            $("#review-categories").empty().text(category_list.slice(0, -1));
        }
    });
//End Tag
});