<%@page import="model.Category"%>
<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="item" type="model.Item" scope="request"/>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Recycle Toy Approval
				</h3>
				<ol class="breadcrumb">

					<li class="active">
						<i class="fa fa-check-square-o"></i> <a href="RecycleController?action=list">Recycle Toy Application</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Recycle Toy Approval
					</li>
				</ol>
			</div>
		</div>
		<form method="POST" action='RecycleController' class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-lg-4 insert">User : </label>
				<div class="col-lg-5">

					<p class="form-control-static">
						<%
							ArrayList<User> users = (ArrayList<User>) request.getAttribute("users");
							for (User user : users) {
						%>
						<% if (user.getUserID() == item.getUserID()) {%> 
						<%= user.getFirstName()%> <%=user.getLastName()%> [ID : <%= user.getUserID()%>]
						<% } %>
						<% }%>
					</p>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Item Name : </label>
				<div class="col-lg-5">
					<p class="form-control-static"><%=item.getItemName()%>[ID : <%= item.getItemID()%>]</p>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Price : </label>
				<div class="col-lg-5">
					<p class="form-control-static"><%= item.getPrice()%></p>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Full Description : </label>
				<div class="col-lg-5">
					<p class="form-control-static"><%= item.getFullDescription()%></p>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Item Image URL : </label>
				<div class="col-lg-5">
					<p class="form-control-static"><%= item.getItemImageURL()%></p>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">State : </label>
				<div class="col-lg-5">
					<select name="state" class="form-control">
						<option value="Approved">Approved</option>
						<option value="Rejected">Rejected</option>
					</select>
				</div>
			</div>

			<input type="hidden" name="itemID" value="<%= item.getItemID()%>">
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-5">

					<a class="btn btn-default col-lg-3 save-btn" href="RecycleController?action=list" role="button">Cancel</a>
					<input type="submit" value="Save" class="btn btn-primary col-lg-3 save-btn"/>

				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
