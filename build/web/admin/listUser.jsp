<%@page import="java.util.ArrayList"%>
<%@page import="model.User"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">

<div class="container-fluid">

	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">
					   User
			</h3>
			<ol class="breadcrumb">
				
				<li class="active">
					<i class="fa fa-users"></i> User
				 </li>
			</ol>
		</div>
	</div>	
	
<p >
	<a href="UserController?action=insert" class="btn btn-primary fa fa-plus">     Add User</a>
	<a  class="pull-right btn btn-danger fa fa-trash-o">     Delete</a>
	<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
	<a class="pull-right btn btn-success fa fa-search-plus">     View</a>
</p>

<div class= table-responsive">
	<table class="table table-striped table-hover">
	<thead class="table-head">
		<tr>
			<th> ID</th>
			 <th>Login Email Address</th>
			<th>User Name</th>
			<th>Gender</th>
			<th>Contact Number</th>
			<th>Is Admin</th>
			<th colspan=3>Action</th>
		</tr>
	</thead>
	<tbody>
		<%
		    ArrayList<User> users = (ArrayList<User>)request.getAttribute("users");
		    for(User user: users) {
		%>
			<tr>
			    <td><%= user.getUserID() %></td>
			    <!--<td>[<%= user.getDefaultBankName()%>] <%= user.getDefaultAccountNumber() %></td>-->
			    <td><%= user.getLoginEmail() %></td>
			    <td><%= user.getLastName() %> <%= user.getFirstName() %></td>
			    <td><%= user.getGender()%></td>
			    <td><%= user.getContactNumber()%></td>
			    <td>
				<% if((user.getIsAdmin()) == 1) { %>
				<i class="fa fa-check" aria-hidden="true"></i>
				<% } else{%>
				<i class="fa fa-times" aria-hidden="true"></i>				
				<% }%>
			    </td>
			    <td>
				<a href="UserController?action=view&userID=<%= user.getUserID()%>" class="btn btn-success fa fa-search-plus"></a>
				 <a href="UserController?action=edit&userID=<%= user.getUserID()%>" class="btn btn-info fa fa-pencil-square-o"></a>
				 <a href="UserController?action=delete&userID=<%= user.getUserID()%>" class="btn btn-danger fa fa-trash-o"></a>
			    </td>
			</tr>
            <%
                }
            %>
        </tbody>
    </table>
</div>

</div>
</div>
</body>
</html>