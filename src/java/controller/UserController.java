/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author wnpoon4
 */
public class UserController extends HttpServlet {

    private static String INSERT = "admin/insertUser.jsp";
    private static String EDIT = "admin/editUser.jsp";
    private static String LIST = "admin/listUser.jsp";
    private static String VIEW = "admin/viewUser.jsp";
    private UserDao userDAO;

    public UserController() {
        super();
        userDAO = new UserDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("delete")) {
                int userID = Integer.parseInt(request.getParameter("userID"));
                userDAO.deleteUser(userID);
                forward = LIST;
                request.setAttribute("users", userDAO.getAllUsers());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                int userID = Integer.parseInt(request.getParameter("userID"));
                User user;
                user = userDAO.getUserById(userID);
                request.setAttribute("user", user);
            } else if (action.equalsIgnoreCase("view")) {
                forward = VIEW;
                int userID = Integer.parseInt(request.getParameter("userID"));
                User user;
                user = userDAO.getUserById(userID);
                request.setAttribute("user", user);
            } else if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                request.setAttribute("users", userDAO.getAllUsers());
            } else {
                forward = INSERT;
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = new User();
        user.setLoginEmail(request.getParameter("loginEmail"));
        user.setLastName(request.getParameter("lastName"));
        user.setFirstName(request.getParameter("firstName"));
        user.setGender(request.getParameter("gender"));
        user.setContactNumber(Integer.parseInt(request.getParameter("contactNumber")));
        user.setPassword(request.getParameter("password"));
        user.setDefaultBankName(request.getParameter("defaultBankName"));
        user.setDefaultAccountNumber(request.getParameter("defaultAccountNumber"));
        user.setDefaultAccountHolder(request.getParameter("defaultAccountHolder"));
        user.setShippingAddress1(request.getParameter("shippingAddress1"));
        user.setShippingAddress2(request.getParameter("shippingAddress2"));
        user.setIsAdmin(Integer.parseInt(request.getParameter("isAdmin")));
        String userID = request.getParameter("userID");
        if (userID == null || userID.isEmpty()) {
            System.out.println("---POST: Insert---");
            userDAO.addUser(user);
        } else {
            System.out.println("---POST: update---");
            user.setUserID(Integer.parseInt(userID));
            userDAO.updateUser(user);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST);
        request.setAttribute("users", userDAO.getAllUsers());
        view.forward(request, response);
    }
}
