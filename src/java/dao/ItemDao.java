/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Item;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class ItemDao {

	private Connection connection;

	public ItemDao() {
		connection = DbUtil.getConnection();
	}

	public int addItem(Item item) {
		try {
			String sql = "insert into ITEM (UserID, ItemName, Price, AvailableQuantity, FullDescription, MagicDescription, BareNecessities, DeliveryDetail, ItemImageURL, CreatedDate, State, ItemType, Color, Width, Height, Material) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			if (item.getUserID() == 0) {
				preparedStatement.setNull(1, java.sql.Types.INTEGER);
			} else {
				preparedStatement.setInt(1, item.getUserID());
			}
			preparedStatement.setString(2, item.getItemName());
			preparedStatement.setDouble(3, item.getPrice());
			preparedStatement.setInt(4, item.getAvailableQuantity());
			preparedStatement.setString(5, item.getFullDescription());
			preparedStatement.setString(6, item.getMagicDescription());
			preparedStatement.setString(7, item.getBareNecessities());
			preparedStatement.setString(8, item.getDeliveryDetail());
			preparedStatement.setString(9, item.getItemImageURL());
			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
			preparedStatement.setDate(10, timeNow);
			preparedStatement.setString(11, item.getState());
			preparedStatement.setString(12, item.getItemType());
			if (item.getColor().equals("N/A")) {
				preparedStatement.setNull(13, java.sql.Types.VARCHAR);
			} else {
				preparedStatement.setString(13, item.getColor());
			}
			if (item.getWidth() == 0) {
				preparedStatement.setNull(14, java.sql.Types.VARCHAR);
			} else {
				preparedStatement.setString(14, Float.toString(item.getWidth()));
			}
			if (item.getHeight() == 0) {
				preparedStatement.setNull(15, java.sql.Types.VARCHAR);
			} else {
				preparedStatement.setString(15, Float.toString(item.getHeight()));
			}
			if (item.getMaterial().equals("N/A")) {
				preparedStatement.setNull(16, java.sql.Types.VARCHAR);
			} else {
				preparedStatement.setString(16, item.getMaterial());
			}
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException ex) {
			System.out.println("addItem SQL Exception: " + ex.getMessage());
		}
		return 0;
	}

	public void deleteItem(int itemID) {
		try {
			String sql = "delete from [ITEM] where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("deleteItem SQL Exception: " + ex.getMessage());
		}
	}

	public void updateItem(Item item) {
		try {
			String sql = "update ITEM set UserID=?, ItemName=?, Price=?, AvailableQuantity=?, FullDescription=?, MagicDescription=?, BareNecessities=?, DeliveryDetail=?, ItemImageURL=?, State=?, ItemType=?, Color=?, Width=?, Height=?, Material=? where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			if (item.getUserID() == 0) {
				preparedStatement.setNull(1, java.sql.Types.INTEGER);
			} else {
				preparedStatement.setInt(1, item.getUserID());
			}
			preparedStatement.setString(2, item.getItemName());
			preparedStatement.setDouble(3, item.getPrice());
			preparedStatement.setInt(4, item.getAvailableQuantity());
			preparedStatement.setString(5, item.getFullDescription());
			preparedStatement.setString(6, item.getMagicDescription());
			preparedStatement.setString(7, item.getBareNecessities());
			preparedStatement.setString(8, item.getDeliveryDetail());
			preparedStatement.setString(9, item.getItemImageURL());
			preparedStatement.setString(10, item.getState());
			preparedStatement.setString(11, item.getItemType());
			preparedStatement.setString(12, item.getColor());
			preparedStatement.setFloat(13, item.getWidth());
			preparedStatement.setFloat(14, item.getHeight());
			preparedStatement.setString(15, item.getMaterial());
			preparedStatement.setInt(16, item.getItemID());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("SQL Exception: " + ex.getMessage());
		}
	}

	public List<Item> getAllItems() {
		List<Item> users = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from [ITEM]");
			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));
				users.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getAllItem SQL Exception: " + ex.getMessage());
		}
		return users;
	}

	public Item getItemById(int itemID) {
		Item item = new Item();
		try {
			String sql = "select * from [ITEM] where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			ResultSet rs = preparedStatement.executeQuery();

			if (rs.next()) {
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));
			}
		} catch (SQLException ex) {
			System.out.println("getItemById SQL Exception: " + ex.getMessage());
		}
		return item;
	}

	public void addCategory(int itemID, int categoryID) {
		try {
			String sql = "insert into ITEM_CATEGORY (ItemID, CategoryID) values(?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			preparedStatement.setInt(2, categoryID);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("ItemDao::addCategory SQL Exception: " + ex.getMessage());
		}
	}

	public void deleteCategory(int itemID) {
		try {
			String sql = "delete ITEM_CATEGORY where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("ItemDao::deleteCategory SQL Exception: " + ex.getMessage());
		}
	}

	public ArrayList<Integer> getCategories(int itemID) {
		ArrayList<Integer> categories = new ArrayList<Integer>();
		try {
			String sql = "select * from ITEM_CATEGORY where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				categories.add(rs.getInt("CategoryID"));
			}
		} catch (SQLException ex) {
			System.out.println("ItemDao::getCategories SQL Exception: " + ex.getMessage());
		}
		return categories;
	}

	public List<Item> getAllRecycleItems() {
		List<Item> users = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from [ITEM] where ItemType='Recycle'");
			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));
				users.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getAllRecycleItem SQL Exception: " + ex.getMessage());
		}
		return users;
	}

	public void approveRecycleItem(int itemID, String state) {
		try {
			String sql = "update ITEM set State=? where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, state);
			preparedStatement.setInt(2, itemID);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("approveRecycle SQL Exception: " + ex.getMessage());
		}
	}

	public List<Item> getItemsByCategory(int categoryID, int limit) {
		List<Item> items = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			String sql = "select  * from [ITEM_CATEGORY]"
				   + " inner join [ITEM] on [ITEM].ItemID=[ITEM_CATEGORY].ItemID"
				   + " where [ITEM_CATEGORY].CategoryID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, categoryID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));

				items.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getItemsByCategory SQL Exception: " + ex.getMessage());
		}
		return items;
	}

	public List<Item> getApprovedItemsByCategory(int categoryID, int limit) {
		List<Item> items = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			String sql = "select  * from [ITEM_CATEGORY]"
				   + " inner join [ITEM] on [ITEM].ItemID=[ITEM_CATEGORY].ItemID"
				   + " where [ITEM_CATEGORY].CategoryID=? and [ITEM].State like '%Approved%'";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, categoryID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));
				System.out.println("itemID: " + rs.getInt("ItemID"));
				items.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getItemsByCategory SQL Exception: " + ex.getMessage());
		}
		return items;
	}

	public ArrayList<Item> getApprovedItemsByCategories(int categoryID, int subCategoryID) {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			String sql = "select * from [ITEM] where[ITEM].State like '%Approved%' and [ITEM].ItemID in("
				   + " select distinct a.ItemID from [ITEM_CATEGORY] as a"
				   + " inner join [ITEM_CATEGORY] as b ON a.ItemID = b.ItemID"
				   + " where a.CategoryID=? and b.CategoryID=?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, categoryID);
			preparedStatement.setInt(2, subCategoryID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));
				System.out.println("itemID: " + rs.getInt("ItemID"));
				items.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getItemsByCategories SQL Exception: " + ex.getMessage());
		}
		return items;
	}

	public ArrayList<Category> getCategoryNames(int itemID) {
		ArrayList<Category> categories = new ArrayList<Category>();
		try {
			String sql = "select [CATEGORY].* from [ITEM_CATEGORY] inner join [CATEGORY]on [ITEM_CATEGORY].CategoryID=[CATEGORY].CategoryID where ItemID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, itemID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Category category = new Category();
				category.setCategoryID(rs.getInt("CategoryID"));
				category.setCategoryName(rs.getString("CategoryName"));
				categories.add(category);
			}
		} catch (SQLException ex) {
			System.out.println("ItemDao::getCategoryNames SQL Exception: " + ex.getMessage());
		}
		return categories;
	}

	public ArrayList<Item> getItemsByKeywords(String keywords) {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			String sql = "select * from [ITEM] where ItemName like ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, '%' + keywords + '%');
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));

				items.add(item);
			}

		} catch (SQLException ex) {
			Logger.getLogger(ItemDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		return items;
	}

	public List<Item> getApprovedItemsByCategoryWithLimit(int categoryID, int limit) {
		List<Item> items = new ArrayList<Item>();
		try {
			Statement statement = connection.createStatement();
			String sql = "select TOP " + limit + " * from [ITEM_CATEGORY]"
				   + " inner join [ITEM] on [ITEM].ItemID=[ITEM_CATEGORY].ItemID"
				   + " where [ITEM_CATEGORY].CategoryID=? and [ITEM].State like '%Approved%'";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, categoryID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Item item = new Item();
				item.setItemID(rs.getInt("ItemID"));
				item.setUserID(rs.getInt("UserID"));
				item.setItemName(rs.getString("ItemName"));
				item.setPrice(rs.getDouble("Price"));
				item.setAvailableQuantity(rs.getInt("AvailableQuantity"));
				item.setFullDescription(rs.getString("FullDescription"));
				item.setMagicDescription(rs.getString("MagicDescription"));
				item.setBareNecessities(rs.getString("BareNecessities"));
				item.setDeliveryDetail(rs.getString("DeliveryDetail"));
				item.setItemImageURL(rs.getString("ItemImageURL"));
				item.setCreatedDate(rs.getDate("CreatedDate"));
				item.setState(rs.getString("State"));
				item.setItemType(rs.getString("ItemType"));
				item.setColor(rs.getString("Color"));
				item.setWidth(rs.getFloat("Width"));
				item.setHeight(rs.getFloat("Height"));
				item.setMaterial(rs.getString("Material"));

				items.add(item);
			}
		} catch (SQLException ex) {
			System.out.println("getItemsByCategory SQL Exception: " + ex.getMessage());
		}
		return items;
	}

}
