/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Stock;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class StockDao {
    private Connection connection;
    
    public StockDao(){
        connection = DbUtil.getConnection();
    } 

    public void addStock(Stock stock) {
        try{
            String sql = "insert into STOCK (ItemID, OrderID, SerialNumber, SalesStatus, Remarks) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            System.out.println(stock.toString());
            preparedStatement.setInt(1, stock.getItemID());
            if (stock.getOrderID() == 0)
                preparedStatement.setNull(2, java.sql.Types.INTEGER);
            else
                preparedStatement.setInt(2, stock.getOrderID());
            preparedStatement.setString(3, stock.getSerialNumber());
            preparedStatement.setString(4, stock.getSalesStatus());
            preparedStatement.setString(5, stock.getRemarks());
            preparedStatement.executeUpdate();
            
            sql = "update ITEM set availableQuantity = availableQuantity + 1 where ItemID=?";
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, stock.getItemID());
            pstmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println("addStock SQL Exception: "+ex.getMessage());        
        }
    }
    
    public void deleteStock(int stockID) {
        try {
            String sql = "delete from [STOCK] where StockID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stockID);
            preparedStatement.executeUpdate();
            
            sql = "update ITEM set availableQuantity = availableQuantity - 1 where ItemID=?";
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, stockID);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("deleteStock SQL Exception: "+ex.getMessage());
        }
    }

    public void updateStock(Stock stock) {
        try {
            String sql = "update STOCK set ItemID=?, OrderID=?, SerialNumber=?, SalesStatus=?, Remarks=? where StockID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stock.getItemID());
            if (stock.getOrderID() == 0)
                preparedStatement.setNull(2, java.sql.Types.INTEGER);
            else
                preparedStatement.setInt(2, stock.getOrderID());
            preparedStatement.setString(3, stock.getSerialNumber());
            preparedStatement.setString(4, stock.getSalesStatus());
            preparedStatement.setString(5, stock.getRemarks());
            preparedStatement.setInt(6, stock.getStockID());
            preparedStatement.executeUpdate();
            
            sql = "update ITEM set availableQuantity = (select count(*) from STOCK where STOCK.ItemID =? and STOCK.SalesStatus like '%Available%') where ITEM.ItemID=?";
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, stock.getItemID());
            pstmt.setInt(2, stock.getItemID());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("updateStock SQL Exception: "+ex.getMessage());
        }
    }
    public List<Stock> getAllStocksByItemId(int itemID){
        List<Stock> stocks = new ArrayList<Stock>();
        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select ITEM.ItemName, STOCK.* from [STOCK],[ITEM] where STOCK.ItemID=ITEM.ItemID and [STOCK].ItemID="+itemID);
            while(rs.next()){
                Stock stock = new Stock();
                stock.setStockID(rs.getInt("StockID"));
                stock.setItemID(rs.getInt("ItemID"));
                stock.setOrderID(rs.getInt("OrderID"));
                stock.setSerialNumber(rs.getString("SerialNumber"));
                stock.setSalesStatus(rs.getString("SalesStatus"));
                stock.setRemarks(rs.getString("Remarks"));
                stocks.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("getAllStocks SQL Exception: "+ex.getMessage());
        }
        return stocks;
    }
    public List<Stock> getAllStocksByOrderId(int orderID){
        List<Stock> stocks = new ArrayList<Stock>();
        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select ITEM.ItemName, ITEM.Price, STOCK.* from [STOCK],[ITEM] where STOCK.ItemID=ITEM.ItemID and [STOCK].OrderID="+orderID);
            while(rs.next()){
                Stock stock = new Stock();
                stock.setStockID(rs.getInt("StockID"));
                stock.setItemID(rs.getInt("ItemID"));
			 stock.setItemName(rs.getString("ItemName"));
                stock.setOrderID(rs.getInt("OrderID"));
			 stock.setPrice(rs.getFloat("Price"));
                stock.setSerialNumber(rs.getString("SerialNumber"));
                stock.setSalesStatus(rs.getString("SalesStatus"));
                stock.setRemarks(rs.getString("Remarks"));
                stocks.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("getAllStocks SQL Exception: "+ex.getMessage());
        }
        return stocks;
    }
    public Stock getStockById(int stockID){
        Stock stock = new Stock();
        try{
            String sql = "select ITEM.ItemName,STOCK.* from [STOCK], [ITEM] where STOCK.ItemID=ITEM.ItemID and StockID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stockID);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                stock.setStockID(rs.getInt("StockID"));
                stock.setItemID(rs.getInt("ItemID"));
                stock.setOrderID(rs.getInt("OrderID"));
                stock.setSerialNumber(rs.getString("SerialNumber"));
                stock.setSalesStatus(rs.getString("SalesStatus"));
                stock.setRemarks(rs.getString("Remarks"));
            }
        } catch (SQLException ex) {
            System.out.println("getStockById SQL Exception: "+ex.getMessage());
        }
        return stock;
    }    
}
