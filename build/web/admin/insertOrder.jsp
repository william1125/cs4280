<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <section class="container-fluid">
        <h2>Add Time Slot</h2>
        <form method="POST" action='OrderController' class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-lg-4">User : </label>
                <div class="col-lg-5">
                    <select name="userID" class="form-control">
                        <c:forEach items="${users}" var="user">
                            <option value="<c:out value="${user.userID}"/>"><c:out value="${user.firstName} ${user.lastName} [ID: ${user.userID}]"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Delivery Date : </label>
                <div class="col-lg-5">
                    <input type="text" name="deliveryDate" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Time Slot : </label>
                <div class="col-lg-5">
                    <select name="timeSlotID" class="form-control">
                        <c:forEach items="${timeSlots}" var="timeSlot">
                            <option value="<c:out value="${timeSlot.timeSlotID}"/>"><c:out value="${timeSlot.fromTime} - ${timeSlot.toTime}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Order Status : </label>
                <div class="col-lg-5">
                    <select name="orderStatus" class="form-control">
                        <option value="Pending">Pending</option>
                        <option value="Completed">Completed</option>
                        <option value="Cancel">Cancel</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Bank Name : </label>
                <div class="col-lg-5">
                    <input type="text" name="bankName" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Account Number : </label>
                <div class="col-lg-5">
                    <input type="number" name="accountNumber" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Account Holder : </label>
                <div class="col-lg-5">
                    <input type="text" name="accountHolder" class="form-control">
                </div>
            </div>
           <div class="form-group">
                <label class="control-label col-lg-4">Shipping Address : </label>
                <div class="col-lg-5">
                    <input type="text" name="shippingAddress" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-5">
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </div>
        </form>
    </section>
</body>
</html>
