<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="inc/head.jsp"/>  
<jsp:useBean id="user" type="model.User" scope="request"/>  
<jsp:useBean id="order" type="model.Order" scope="request"/>
<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> Order History
		</p>

		<h2 class="new-sec-title">Order History</h2>
		<div class="history-detail-form">
			<div class="col-sm-3"style="margin-top: 10px;">
				<form action="profile"  method="POST"class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label" style="color: #888; font-size: 14px; font-family: Arial;">My Account</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="profile">Account Profile</a><br>
								<a href="payment">Payment Method</a><br>
								<a href="order">Order History</a><br>
								<a href="recycle">Recycle Toy</a>
							</p>
					</fieldset>
				</form>
			</div>
			<div class="col-sm-9">
				<form action="#" class="horizontal-form">
					<fieldset class="account-profile-border registration-border ">
						<legend class="registration-border" >Order Information</legend>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 form-control-label">Order Id:</label>
							<div class="col-sm-5 data">
								${order.orderID}
							</div>
							<div class="col-sm-5 status-label">
								<% if (order.getOrderStatus().equalsIgnoreCase("Pending")) {%>
								<h4><span class="label label-primary"style="font-size: 19px">${order.orderStatus}</span></h4>
									<%} else if (order.getOrderStatus().equalsIgnoreCase("Completed")) {%>
								<h4><span class="label label-success"style="font-size: 19px">${order.orderStatus}</span></h4>
									<%} else if (order.getOrderStatus().equalsIgnoreCase("Cancel")) {%>
								<h4><span class="label label-warning"style="font-size: 19px">${order.orderStatus}</span></h4>
									<%}%>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-2 form-control-label">Order Date:</label>
							<div class="col-sm-3 data">
								${order.orderDate}
							</div>
							<label for="inputPassword3" class="col-sm-3 form-control-label">Delivery Date:</label>
							<div class="col-sm-4 data">
								${order.deliveryDate}
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-2 form-control-label">Shipping To:</label>
							<div class="col-sm-10 data">
								${order.shippingAddress}
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-2 form-control-label">Payment By:</label>
							<div class="col-sm-10 data">
								${order.accountHolder}
							</div>
						</div>
					</fieldset>
					<fieldset class="category-border"style="margin-top: 10px;">
						<div class="history-detail-heading form-control-label row"style="margin-top: 20px;">
							<div class="col-sm-2" style="margin-left: 45px;">
								<p >Serial Number</p>
							</div>
							<div class="col-sm-2"style="margin-left: 178px;">
								<p >Product Name</p>
							</div> 
							<div class="col-sm-1"style="margin-left: 140px;">
								<p >Status</p>
							</div>
							<div class="col-sm-1">
								<p >Price</p>
							</div>
							
							      
						</div>
						<c:forEach items="${stocks}" var="stock">
						<div class="row">
							<div class="col-sm-5">
								<p class="history-inner-data" style="margin-left: 45px;">${stock.serialNumber}</p>
							</div>
							<div class="col-sm-4">
								<p class="history-inner-data">${stock.itemName}</p>
							</div>
							<div class="col-sm-1">
								<p class="history-inner-data">${stock.salesStatus}</p>
							</div>
							<div class="col-sm-1">
								<p class="history-inner-data">${stock.price}</p>
							</div>
							
						</div>
						</c:forEach>
					</fieldset>



					<div class=" pull-right ">
						<input type="submit" value="Back" class="btn btn-primary btn-lg reg-btn">          
					</div>
				</form>
			</div>  
		</div>        
	</div>
</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/> 