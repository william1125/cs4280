<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<jsp:include page="inc/head.jsp"/>
<%
    String scheme = request.getScheme();
    String serverName = request.getServerName();
    int serverPort = request.getServerPort();
    String contextPath = request.getContextPath();  // includes leading forward slash

    String resultPath = scheme + "://" + serverName + ":" + serverPort;

%>
<!--Main Content Start-->
<div class="cp-page-content woocommerce bg">
    <div class="container" style="margin-bottom: 20px;">
        <p class="navigation">
            <a href="LandingController">Home</a>
            > 	
            <a href="product-category.jsp">Product</a>
            > Product Detail
        </p>
        <!-- product name -->
        <h2 class="new-sec-title"style="margin-top: 20px;">${product.itemName}</h2>
        <div class="product-detail">
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-5">
                    <div class='zoom thumb' id='product-zoom' style="margin-top: 20px;"> <img src="<%= resultPath%>/data/${product.itemImageURL}" alt=""> </div>
                </div>
                <!--DETAIL TEXT START-->
                <div class="col-md-7">
                    <div class="price-bar">
                        <!--ITEM PRICE START-->
                        <div class="price">$ ${product.price} </div>
                        <div class="product-id text-right"><span>Product ID: </span>${product.itemID}</div>
                        <!--ITEM PRICE AND DISCOUNT END--> 
                    </div>
                    <!--RATING START-->


                    <div class="rating-bar">
                        <div class="rating">
                            <c:set var="rating" value="0"/>
                            <c:forEach items="${reviews}" var="review">
                                <c:set var="rating"value = "${review.itemRating +rating}"/>	
                            </c:forEach>
                            <c:set var="count" value="0"/>
                            <c:forEach items="${reviews}" var="review">
                                <c:set var="count"value = "${1 +count}"/>	
                            </c:forEach>
                            <c:forEach begin="1" end="${rating / count}" step="1">
                                <i class="fa fa-star"></i>
                            </c:forEach>
                        </div>
                        <div class="review">
                            ${count} comments | <a href="#link1">Write a comment</a>
                        </div>
                    </div>
                    <!--RATING END--> 
                    <div class="text">
                        <!-- Full Description Start -->
                        <p style="text-align: justify; color: #888; font-size: 14px; font-family: Arial;">${product.fullDescription}</p>
                        <!-- Full Description End -->
                        <!-- Magic Description Start -->
                        <h4 style="color:#00B7DB; margin-top: 20px;">Magic in the details...</h4>
                        <p style="padding-top: 5px; padding-left: 15px; text-align: justify; color: #888; font-size: 14px; font-family: Arial;">${product.magicDescription}</p>
                        <!-- Magic Descrption End -->
                        <!--ITEM DETAIL START-->
                        <div class="item-detail">
                            <ul>
                                <li><span>Available Quantity:</span>${product.availableQuantity}</li>
                                <li><span>Category:</span>
                                    <c:forEach items="${categories}" var="category">
                                        <a href="categories?action=list&cid=${category.categoryID}">${category.categoryName}</a>
                                    </c:forEach>
                                </li>
                            </ul>
                        </div>
                        <!--ITEM DETAIL END--> 
                        <!--QUANTITY START-->
                        <!--QUESTION HERE need to add more than 1 item-->
                        <div class="quantity">
                            <form id='myform' method='POST' action="cart" style="float: right;">
                                <input type='button' value='-' class='qtyminus' />
                                <input type='text' name='quantity' value='1' class='qty' min="1" max="1" step="1"/>
                                <input type='button' value='+' class='qtyplus'/>
                                <input type="hidden" name="itemID" value="${product.itemID}">
                                <input type="hidden" name="action" value="insert">
                                <button><i><img src="images/cart-icon2.png" alt=""></i>Add To Cart</button>
                            </form>
                        </div>
                        <!--QUANTITY END-->

                    </div>
                </div>
                <!--DETAIL TEXT END--> 
            </div>
            <div class="minor-detail" style="margin-bottom: 20px;">
                <div class="col-sm-6">
                    <h4 style="color:#00B7DB">The Bare Necessities</h4>
                    <p style="padding-top: 5px; padding-left: 15px; text-align: justify; color: #888; font-size: 14px; font-family: Arial; font-weight: normal;">${product.bareNecessities}</p>
                </div>
                <div class="col-sm-6">
                    <h4 style="color:#00B7DB">Delivery Detail</h4>
                    <p style="padding-top: 5px; text-align: justify; color: #888; font-size: 14px; font-family: Arial; ">${product.deliveryDetail}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="product-listing-container bg">
        <div class="container">
            <h2 class="new-sec-title" style="font-size:30px; margin-top:50px;">Similar Product</h2>
            <div class="product-listing" style="margin-top:10px; padding-top: 0px">
                <ul class="row">
                    <!--LIST ITEM START-->
                    <c:forEach items="${similar_products}" var="product">
                        <li class="col-md-3 col-sm-6">
                            <div class="pro-list">
                                <!-- <div class="saletag">Sale</div> -->
                                <div class="thumb"><a href="product?pid=${product.itemID}"><img src="<%= resultPath%>/data/${product.itemImageURL}" alt=""></a></div>
                                <div class="text">
                                    <div class="pro-name">
                                        <h4>${product.itemName}</h4>
                                        <p class="price">$ ${product.price}</p>
                                    </div>
                                    <c:set var="description" value="${product.fullDescription}"/>
                                    <p><c:out value="${fn:substring(description, 0, 30)}..."/></p>
                                </div>
                                <div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a>
                                    <div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
                                </div>
                            </div>
                        </li>
                    </c:forEach>
                    <!--LIST ITEM END--> 

                </ul>
                <div class="paging" style="opacity: 1;">
                    <ul class="pagination">
                        <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                        <li class="active"> <span>1 <span class="sr-only">(current)</span></span> </li>
                        <!--						<li><a href="#">2</a></li>
                                                                        <li><a href="#">3</a></li>
                                                                        <li><a href="#">4</a></li>
                                                                        <li><a href="#">5</a></li>-->
                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- review start -->

    <div class="product-detail-review">
        <div class="container">
            <h2 class="new-sec-title" style="font-size:30px;  margin-top:30px; margin-bottom: 0px;">Customer Comments</h2>
            <div class="rating-bar">
                <div class="pull-left rating" style="margin-left: 20px; font-size: 14px; font-family: Arial;padding-top: 20px;">Overall rating:
                    <c:set var="rating" value="0"/>
                    <c:forEach items="${reviews}" var="review">
                        <c:set var="rating"value = "${review.itemRating +rating}"/>	
                    </c:forEach>
                    <c:set var="count" value="0"/>
                    <c:forEach items="${reviews}" var="review">
                        <c:set var="count"value = "${1 +count}"/>	
                    </c:forEach>
                    <c:forEach begin="1" end="${rating / count}" step="1">
                        <i class="fa fa-star"></i>
                    </c:forEach>
                </div>

                <div id="link1" class="pull-right write-review-btn btn btn-success" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <a><i class="fa fa-pencil-square-o"></i>
                        Write a Comment</a></div>
                </p>

            </div>
            <div class="collapse" id="collapseExample">
                <form action="product" method="POST" class="registration-form horizontal-form" style="border-bottom: 1px #ddd solid;">
                    <fieldset class="registration-border">
                        <legend class="registration-border" >Comment Detail</legend>
                        <div class="form-group row">
                            <h4 >Comment Title</h4>
                            <div >
                                <input name="Title" type="text" class="form-control" style="font-weight: normal;">
                            </div>
                            <h4 style="padding-top: 15px">Product Rating</h4>
                            <div >
                                <input name="ItemRating" type="number" class="form-control" style="font-weight: normal;">
                            </div>
                            <h4 style="padding-top: 15px">Content</h4>
                            <div >
                                <textarea name="Content" type="text" class="form-control" rows="3" style="font-weight: normal;"></textarea>
                            </div>
                        </div>
                        <div class="row pull-right ">
                            <input type="submit" value="Cancel" class="btn btn-lg reg-btn" style="font-size: 14px">
                            <input type="submit" value="Submit" class="btn btn-primary btn-lg reg-btn" style="font-size: 14px">          
                        </div>
                    </fieldset>
                    <input type="hidden" name="ItemID" value="${product.itemID}">
                    <input type="hidden" name="UserID" value="${userID}">
                    <input type="hidden" name="ReviewImageURL" value="N/A">

                </form>
            </div>
            <div class="reviews" style="padding-bottom: 30px;">
                <c:forEach items="${reviews}" var="review">
                    <div class="review">
                        <div class="col-md-3" style="padding-top: 10px; ">
                            <p style="font-size: 16px; font-family: Arial; font-weight: bold;padding-left: 50px;">${review.firstName} ${review.lastName}</p>
                            <p style="font-size: 14px; font-family: Arial; padding-left: 50px;">${review.createdDate}</p>
                        </div>
                        <div class="col-md-7" style="padding-top: 10px; padding-left: 30px; border-left:solid 1px #cccccc;">
                            <div class="rating">
                                <c:forEach begin="1" end="${review.itemRating}" step="1">
                                    <i class="fa fa-star"></i>
                                </c:forEach>
                            </div>
                            <p class="title" style="padding-top: 10px;font-size: 16px; font-family: Arial; font-weight: bold;">${review.title}</p>
                            <p class="content" style="font-size: 14px; font-family: Arial; ">${review.content}</p>
                        </div>
                        <div class="col-md-2" style="padding-top: 10px; ">
                            <p>Comment ID: ${review.reviewID}</p>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <!-- review end -->

    <!--Main Content End--> 
    <jsp:include page="inc/foot.jsp"/>