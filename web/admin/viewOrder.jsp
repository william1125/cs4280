<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					View Order
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-newspaper-o"></i> <a href="OrderController?action=list">Order</a>
					</li>
					<li class="active">
						<i class="fa fa-search-plus"></i> View Order
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='OrderController' class="form-horizontal">

			<div class="form-group">
				<label class="control-label col-lg-4 insert">User : </label>
				<div class="col-lg-5">
					<select disabled name="userID" class="form-control">
						<c:forEach items="${users}" var="user">
							<option value="<c:out value="${user.userID}"/>"
								   <c:if test="${user.userID==order.userID}">selected</c:if>
									   >
								   <c:out value="${user.firstName} ${user.lastName} [ID: ${user.userID}]"/>
							</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Order Status : </label>
				<div class="col-lg-5">
					<select disabled name="orderStatus" class="form-control">
						<option value="Pending" <c:if test="${order.orderStatus == 'Pending'}">selected</c:if>>Pending</option>
						<option value="Completed" <c:if test="${order.orderStatus == 'Completed'}">selected</c:if>>Completed</option>
						<option value="Cancel" <c:if test="${order.orderStatus=='Cancel'}">selected</c:if>>Cancel</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Delivery Date : </label>
					<div class="col-lg-5">
						<input disabled type="text" name="deliveryDate" class="form-control" value="<c:out value="${order.deliveryDate}"/>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Time Slot : </label>
				<div class="col-lg-5">
					<select disabled name="timeSlotID" class="form-control">
						<c:forEach items="${timeSlots}" var="timeSlot">
							<option value="<c:out value="${timeSlot.timeSlotID}"/>"
								   <c:if test="${timeSlot.timeSlotID==order.timeSlotID}">selected</c:if>
									   >
								   <c:out value="${timeSlot.fromTime} - ${timeSlot.toTime}"/>
							</option>
						</c:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-4 insert">Bank Name : </label>
				<div class="col-lg-5">
					<select disabled name="bankName" class="form-control">
						<option value="Bank of China (Hong Kong)" <c:if test="${order.bankName == 'Bank of China (Hong Kong)'}">selected</c:if>>Bank of China (Hong Kong)</option>
						<option value="Bank of East Asia"  <c:if test="${order.bankName == 'Bank of East Asia'}">selected</c:if>>Bank of East Asia</option>
						<option value="Hang Seng Bank"  <c:if test="${order.bankName == 'Hang Seng Bank'}">selected</c:if>>Hang Seng Bank</option>
						<option value="Hongkong and Shanghai Banking Corporation"  <c:if test="${order.bankName == 'Hongkong and Shanghai Banking Corporation'}">selected</c:if>>Hongkong and Shanghai Banking Corporation</option>
						<option value="Industrial and Commercial Bank of China (Asia)"  <c:if test="${order.bankName == 'Industrial and Commercial Bank of China (Asia)'}">selected</c:if>>Industrial and Commercial Bank of China (Asia)</option>
						<option value="Standard Chartered Bank (Hong Kong)"  <c:if test="${order.bankName == 'Standard Chartered Bank (Hong Kong)'}">selected</c:if>>Standard Chartered Bank (Hong Kong)</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Account Number : </label>
					<div class="col-lg-5">
						<input disabled name="accountNumber" type="number" class="form-control" value="${order.accountNumber}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Account Holder : </label>
				<div class="col-lg-5">
					<input disabled name="accountHolder" type="text" class="form-control" value="${order.accountHolder}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Shipping Address : </label>
				<div class="col-lg-5">
					<input disabled name="shippingAddress" type="text" class="form-control" value="${order.shippingAddress}">
				</div>
			</div>

			<input type="hidden" name="orderID" value="<c:out value="${order.orderID}"/>"
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="OrderController?action=list" role="button">Back</a>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
