/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nam
 */
public class User {
    private int UserID;
    private String LoginEmail;
    private String LastName;
    private String FirstName;
    private String Gender;
    private int ContactNumber;
    private String Password;
    private String DefaultBankName;
    private String DefaultAccountNumber;
    private String DefaultAccountHolder;
    private String ShippingAddress1;
    private String ShippingAddress2;
    private int IsAdmin;
    /**
     * @return the UserID
     */
    public int getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the LoginEmail
     */
    public String getLoginEmail() {
        return LoginEmail;
    }

    /**
     * @param LoginEmail the LoginEmail to set
     */
    public void setLoginEmail(String LoginEmail) {
        this.LoginEmail = LoginEmail;
    }

    /**
     * @return the LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName the LastName to set
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return the FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     * @param FirstName the FirstName to set
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     * @return the Gender
     */
    public String getGender() {
        return Gender;
    }

    /**
     * @param Gender the Gender to set
     */
    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    /**
     * @return the ContactNumber
     */
    public int getContactNumber() {
        return ContactNumber;
    }

    /**
     * @param ContactNumber the ContactNumber to set
     */
    public void setContactNumber(int ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    /**
     * @return the Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     * @param Password the Password to set
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     * @return the DefaultBankName
     */
    public String getDefaultBankName() {
        return DefaultBankName;
    }

    /**
     * @param DefaultBankName the DefaultBankName to set
     */
    public void setDefaultBankName(String DefaultBankName) {
        this.DefaultBankName = DefaultBankName;
    }

    /**
     * @return the DefaultAccountNumber
     */
    public String getDefaultAccountNumber() {
        return DefaultAccountNumber;
    }

    /**
     * @param DefaultAccountNumber the DefaultAccountNumber to set
     */
    public void setDefaultAccountNumber(String DefaultAccountNumber) {
        this.DefaultAccountNumber = DefaultAccountNumber;
    }

    /**
     * @return the DefaultAccountHolder
     */
    public String getDefaultAccountHolder() {
        return DefaultAccountHolder;
    }

    /**
     * @param DefaultAccountHolder the DefaultAccountHolder to set
     */
    public void setDefaultAccountHolder(String DefaultAccountHolder) {
        this.DefaultAccountHolder = DefaultAccountHolder;
    }

    /**
     * @return the IsAdmin
     */
    public int getIsAdmin() {
        return IsAdmin;
    }

    /**
     * @param IsAdmin the IsAdmin to set
     */
    public void setIsAdmin(int IsAdmin) {
        this.IsAdmin = IsAdmin;
    }

    /**
     * @return the ShippingAddress1
     */
    public String getShippingAddress1() {
        return ShippingAddress1;
    }

    /**
     * @param ShippingAddress1 the ShippingAddress1 to set
     */
    public void setShippingAddress1(String ShippingAddress1) {
        this.ShippingAddress1 = ShippingAddress1;
    }

    /**
     * @return the ShippingAddress2
     */
    public String getShippingAddress2() {
        return ShippingAddress2;
    }

    /**
     * @param ShippingAddress2 the ShippingAddress2 to set
     */
    public void setShippingAddress2(String ShippingAddress2) {
        this.ShippingAddress2 = ShippingAddress2;
    }
}
