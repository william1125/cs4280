/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.AuthDao;
import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Category;
import model.User;

/**
 *
 * @author Nam
 */
public class LoginController extends HttpServlet {

	private static String ADMIN_INDEX = "admin/listUser.jsp";
	private static String USER_INDEX = "index.jsp";
	private static String LAND = "login.jsp";
	private AuthDao authDAO;
	private CategoryDao categoryDAO;
	private ItemDao itemDAO;

	public LoginController() {
		super();
		authDAO = new AuthDao();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";

		forward = LAND;

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		String email = request.getParameter("loginEmail");
		String password = request.getParameter("password");
		System.out.println(email + " " + password);
		User user = new User();
		user = authDAO.login(email, password);
		if (user != null) {
			HttpSession session = request.getSession(true);
			session.setAttribute("userid", user.getUserID());
			session.setAttribute("firstName", user.getFirstName());
			session.setAttribute("lastName", user.getLastName());
			session.setAttribute("isAdmin", user.getIsAdmin());
			if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
				forward = USER_INDEX;
				response.sendRedirect("LandingController");
			} else {
				forward = ADMIN_INDEX;
				response.sendRedirect("UserController");
			}
		} else {
			forward = LAND;
			request.setAttribute("error", "Invalid email/password");
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}

	}
}
