<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--<jsp:useBean id="category" type="model.Category" scope="request"/>--%>
<div id="page-wrapper">
    <section class="container-fluid">
<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header ">
					   Edit Time Slot
			</h3>
			<ol class="breadcrumb">
				
				<li class="active">
					<i class="fa fa-calendar"></i> <a href="TimeSlotController?action=list">Shipping Timeslot</a>
				 </li>
				 <li class="active">
					<i class="fa fa-pencil-square-o"></i> Edit Time Slot
				 </li>
			</ol>
		</div>
	</div>
   
        <form method="POST" action='TimeSlotController' class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-lg-4 insert">From Time : </label>
                <div class="col-lg-5">
                    <input name="fromTime" type="text" class="form-control" value="${timeSlot.fromTime}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4 insert">To Time : </label>
                <div class="col-lg-5">
                    <input name="toTime" type="text" class="form-control" value="${timeSlot.toTime}">
                </div>
            </div>
                    <input name="timeSlotID" type="hidden" value="${timeSlot.timeSlotID}">
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-5">
                    <input type="submit" value="Save" class="btn btn-primary"/>
			<a class="btn btn-default" href="TimeSlotController?action=list" role="button">Cancel</a>
             
                </div>
            </div>
        </form>
    </section>
</div>
</body>
</html>

