<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Add Time Slot
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-calendar"></i> <a href="TimeSlotController?action=list">Shipping Timeslot</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Add Time Slot
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='TimeSlotController' class="form-horizontal">
			<div class="form-group ">
				<label class="control-label col-lg-4 insert">From Time : </label>
				<div class="col-lg-5">
					<input name="fromTime" type="text" class="form-control requred">
					<p></p>
				</div>
				<label class="control-label col-lg-4 insert" >To Time : </label>
				<div class="col-lg-5">
					<input name="toTime" type="text" class="form-control" requred>
				</div>
			</div>
			<div class="form-group ">
				<div class="col-lg-offset-4 col-lg-5">
					<a class="btn btn-default  col-lg-3 save-btn" href="TimeSlotController?action=list" role="button">Cancel</a>
					<input type="submit" value="Add" class="btn btn-primary  col-lg-3 save-btn"/>

				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
