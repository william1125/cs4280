/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class CategoryDao {
    private Connection connection;
    
    public CategoryDao(){
        connection = DbUtil.getConnection();
    }
    
    public void addCategory(Category category) {
        try{
            String sql = "insert into CATEGORY(CategoryName) values(?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, category.getCategoryName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());        
        }
    }

    public void deleteCategory(int categoryID) {
        try {
            String sql = "delete from CATEGORY where CategoryID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, categoryID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
    }

    public void updateCategory(Category category) {
        try {
            String sql = "update CATEGORY set CategoryName=? where CategoryID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, category.getCategoryName());
            preparedStatement.setInt(2, category.getCategoryID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
    }

    public List<Category> getAllCategories(){
        List<Category> categories = new ArrayList<Category>();
        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from CATEGORY");
            while(rs.next()){
                Category category = new Category();
                category.setCategoryID(rs.getInt("CategoryID"));
                category.setCategoryName(rs.getString("CategoryName"));
                categories.add(category);
            }
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
        return categories;
    }

        public Category getCategoryById(int categoryID){
        Category category = new Category();
        try{
            String sql = "select * from CATEGORY where CategoryID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, categoryID);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                category.setCategoryID(rs.getInt("CategoryID"));
                category.setCategoryName(rs.getString("CategoryName"));
            }
        }   catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
        return category;
    }
    
    public Category getCategoryByName(String name){
        Category category = new Category();
        try{
            String sql = "select * from [CATEGORY] where CategoryName like ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, "%"+name+"%");
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()){
                category.setCategoryID(rs.getInt("CategoryID"));
                category.setCategoryName(rs.getString("CategoryName"));
            }
        } catch (SQLException ex) {
            System.out.println("getCategoryByName SQL Exception: "+ex.getMessage());
        }
        return category;
    }
}
