<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="review" type="model.Review" scope="request"></jsp:useBean>
<jsp:useBean id="userid" type="Integer" scope="session"></jsp:useBean>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Reply Comment
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-comments"></i> <a href="ReviewController?action=list">Product Comment</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Reply Comment
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='ReviewController' class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label col-lg-4">Item : </label>
				<div class="col-lg-5">
					<select name="" class="form-control" disabled>
						<c:forEach items="${items}" var="item">
							<option value="${item.itemID}"
								   <c:if test="${item.itemID==review.itemID}">selected</c:if>
									   >
								   ${item.itemName} [ID: ${item.itemID}]
							</option>
						</c:forEach>
					</select>
					<input type="hidden" name="itemID" value="${review.itemID}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4">User : </label>
				<div class="col-lg-5">
					<select disabled name="userID" class="form-control">
						<c:forEach items="${users}" var="user">
							<option value="${user.userID}"
								   <c:if test="${user.userID==userid}">selected</c:if>
									   >
								   <c:out value="${user.firstName} ${user.lastName} [ID: ${user.userID}]"/>
							</option>
						</c:forEach>
					</select>
					<input type="hidden" name="userID" value="${review.userID}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4">Reply ID : </label>
				<div class="col-lg-5">
					<input disabled type="number" name="" class="form-control" value="<c:if test="${replyID != 0}">${replyID}</c:if>">
					<input type="hidden" name="replyID" value="${replyID}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4">Title : </label>
				<div class="col-lg-5">
					<input type="text" name="title" class="form-control" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4">Content : </label>
				<div class="col-lg-5">
					<textarea name="content" class="form-control" required></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-5">
					<a class="btn btn-default col-lg-3 save-btn" href="ReviewController?action=list" role="button">Cancel</a>
					<input type="submit" value="Add" class="btn btn-primary col-lg-3 save-btn"/>
					
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
