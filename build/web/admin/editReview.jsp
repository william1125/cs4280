<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Edit Comment
				</h3>
				<ol class="breadcrumb">

					<li class="active">
						<i class="fa fa-comments"></i> <a href="ReviewController?action=list">Product Comment</a>
					</li>
					<li class="active">
						<i class="fa fa-pencil-square-o"></i> Edit Comment
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='ReviewController' class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Item : </label>
				<div class="col-lg-5">
					<select name="itemID" class="form-control">
						<c:forEach items="${items}" var="item">
							<option value="${item.itemID}"
								   <c:if test="${item.itemID==review.itemID}">selected</c:if>
									   >
								   ${item.itemName} [ID: ${item.itemID}]
							</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">User : </label>
				<div class="col-lg-5">
					<select name="userID" class="form-control">
						<c:forEach items="${users}" var="user">
							<option value="${user.userID}"
								   <c:if test="${user.userID==review.userID}">selected</c:if>
									   >
								   <c:out value="${user.firstName} ${user.lastName} [ID: ${user.userID}]"/>
							</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Reply ID : </label>
				<div class="col-lg-5">
					<input type="number" name="replyID" class="form-control" value="${review.replyID}" step="1">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Title : </label>
				<div class="col-lg-5">
					<input type="text" name="title" class="form-control" value="${review.title}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Content : </label>
				<div class="col-lg-5">
					<input type="text" name="content" class="form-control" value="${review.content}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Item Rating : </label>
				<div class="col-lg-5">
					<input type="number" name="itemRating" class="form-control" value="${review.itemRating}" step="1" min="1" max="5">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Review Image : </label>
				<div class="col-lg-5">
					<input type="file" name="reviewImage">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Review Image URL : </label>
				<div class="col-lg-5">
					<p class="form-control-static">${review.reviewImageURL}</p>
				</div>
			</div>
			<input type="hidden" name="reviewID" value="${review.reviewID}">
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="ReviewController?action=list" role="button">Cancel</a>
					<input type="submit" value="Save" class="btn btn-primary col-lg-3 save-btn"/>

				</div>
			</div>
		</form>

	</section>
</div>
</body>
</html>
