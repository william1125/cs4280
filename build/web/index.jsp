<jsp:include page="inc/head.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String scheme = request.getScheme();
	String serverName = request.getServerName();
	int serverPort = request.getServerPort();
	String contextPath = request.getContextPath();  // includes leading forward slash

	String resultPath = scheme + "://" + serverName + ":" + serverPort;

%>

<!--Main  Slider Start-->
<div class="main-slider">
	<ul class="cp-child-slider">
		<li><img src="images/gallery/slider1.jpg" alt="" style="width: 100%" />
			<div class="caption">
				<div class="container">
					<div class="slider-data">
						<h2>Play is the highest <br>
							form of research</h2>
						<a class="shopping-button" href="#">Shop Now</a> </div>
				</div>
			</div>
		</li>
		<li><img src="images/gallery/slider2.jpg" alt="" style="width: 100%" />
			<div class="caption">
				<div class="container">
					<div class="slider-data">
						<h2>Every Thing you can <br>
							imagine is real</h2>
						<a class="shopping-button" href="#">Shop Now</a> </div>
				</div>
			</div>
		</li>
		<li><img src="images/gallery/slider3.jpg" alt="" style="width: 100%" />
			<div class="caption">
				<div class="container">
					<div class="slider-data">
						<h2>Some first steps are <br>
							bigger than others.</h2>
						<a class="shopping-button" href="#">Shop Now</a> </div>
				</div>
			</div>
		</li>
	</ul>
</div>
<!--Main  Slider End--> 

<!--Main Content Start-->
<div class="cp-page-content"> 

	<!--CARING CHILDREN START-->
	<div class="cp-caring-product gap-80" style="padding-bottom:30px;">
		<div class="products-tabs">
			<h2 class="sec-title">New Arrivals</h2>
			<div role="tabpanel"> 

				<!-- Nav tabs -->

				<div class="container">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Dolls</a></li>
						<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">LEGO</a></li>
						<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Plush</a></li>
						<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Play Sets</a></li>
						<!-- 
						<li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Plush</a></li>
						<li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Play Sets</a></li> -->
					</ul>
				</div>
				<!-- Tab panes -->
				<div class="tab-content product-listing" style="    padding-top: 0px; ">
					<div role="tabpanel" class="tab-pane active" id="tab1">
						<div class="container">
							<ul class="cp-product-list row">
								<c:forEach items="${dolls}" var="doll">
									<li class="col-md-3 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${doll.itemID}"><img alt="" src="<%= resultPath%>/data/${doll.itemImageURL}"></a></div>
											<div class="text">
												<div class="pro-name">
													<h4>${doll.itemName}</h4>
													<p class="price">$ ${doll.price}</p>
												</div>
												<!--                        <p>Proin gravida nibh vel velit auctor...</p>-->
											</div>
											<div class="cart-options"> 
												<a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> 
												<form action="cart" id="item${doll.itemID}" method="POST">
													<a onclick="document.getElementById('item${doll.itemID}').submit();"style="    border-left: solid 1px #cccccc;"><i class="fa fa-shopping-cart"></i>Cart</a> 
													<input type="hidden" name="action" value="insert">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="itemID" value="${doll.itemID}">
												</form>
												<a href="product?pid=${doll.itemID}"><i class="fa fa-search"></i>Details</a>
												<!--<div class="rating">  </div>-->
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab2">
						<div class="container">
							<ul class="cp-product-list row">
								<c:forEach items="${lego}" var="l">
									<li class="col-md-3 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${l.itemID}"><img alt="" src="<%= resultPath%>/data/${l.itemImageURL}"></a></div>
											<div class="text">
												<div class="pro-name">
													<h4>${l.itemName}</h4>
													<p class="price">$ ${l.price}</p>
												</div>
												<!--<p>Proin gravida nibh vel velit auctor...</p>-->
											</div>
											<div class="cart-options"> 
												<a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> 
												<form action="cart" id="item${l.itemID}" method="POST">
													<a onclick="document.getElementById('item${l.itemID}').submit();" style="    border-left: solid 1px #cccccc;"><i class="fa fa-shopping-cart"></i>Cart</a> 
													<input type="hidden" name="action" value="insert">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="itemID" value="${l.itemID}">
												</form>
												<a href="product?pid=${l.itemID}"><i class="fa fa-search"></i>Details</a>
												<div class="rating">  </div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab3">
						<div class="container">
							<ul class="cp-product-list row">
								<c:forEach items="${plush}" var="p">
									<li class="col-md-3 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${p.itemID}"><img alt="" src="<%= resultPath%>/data/${p.itemImageURL}"></a></div>
											<div class="text">
												<div class="pro-name">
													<h4>${p.itemName}</h4>
													<p class="price">$ ${p.price}</p>
												</div>
												<!--<p>Proin gravida nibh vel velit auctor...</p>-->
											</div>
											<div class="cart-options"> 
												<a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> 
												<form action="cart" id="item${p.itemID}" method="POST">
													<a onclick="document.getElementById('item${p.itemID}').submit();"style="    border-left: solid 1px #cccccc;"><i class="fa fa-shopping-cart"></i>Cart</a> 
													<input type="hidden" name="action" value="insert">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="itemID" value="${p.itemID}">
												</form> 
												<a href="product?pid=${p.itemID}"><i class="fa fa-search"></i>Details</a> 
												<div class="rating">  </div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab4">
						<div class="container">
							<ul class="cp-product-list row">
								<c:forEach items="${playSets}" var="playset">
									<li class="col-md-3 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${playset.itemID}"><img alt="" src="<%= resultPath%>/data/${l.itemImageURL}"></a></div>
											<div class="text">
												<div class="pro-name">
													<h4>${playset.itemName}</h4>
													<p class="price">$ ${playset.price}</p>
												</div>
												<!--<p>Proin gravida nibh vel velit auctor...</p>-->
											</div>
											<div class="cart-options"> 
												<a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> 
												<form action="cart" id="item${playset.itemID}" method="POST">
													<a onclick="document.getElementById('item${playset.itemID}').submit();" style="    border-left: solid 1px #cccccc;"><i class="fa fa-shopping-cart"></i>Cart</a> 
													<input type="hidden" name="action" value="insert">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="itemID" value="${playset.itemID}">
												</form> 
												<a href="product?pid=${playset.itemID}"><i class="fa fa-search"></i>Details</a> 
												<div class="rating"> </div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab5">
						<div class="container">
							<ul class="cp-product-list row">
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro1.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro2.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro3.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro4.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab6">
						<div class="container">
							<ul class="cp-product-list row">
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro4.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro3.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro2.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
								<li class="col-md-3 col-sm-6">
									<div class="pro-list">
										<div class="thumb"><a href="#"><img alt="" src="images/hmpro1.jpg"></a></div>
										<div class="text">
											<div class="pro-name">
												<h4>Baby Sister</h4>
												<p class="price">$98.99</p>
											</div>
											<p>Proin gravida nibh vel velit auctor...</p>
										</div>
										<div class="cart-options"> <a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> <a href="#"><i class="fa fa-shopping-cart"></i>Cart</a> <a href="#"><i class="fa fa-search"></i>Details</a> <a href="#"><i class="fa fa-share-alt"></i>Share</a>
											<div class="rating"> <span>?</span><span>?</span><span>?</span><span>?</span><span>?</span> </div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--CARING CHILDREN End--> 

	<!--Home Paralx Start-->
	<div class="cp-home-peralax">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="caring-content">
						<img src="images/circle-logo.png">
						<img src="images/slogan.png" class="slogan">
						<a class="readmore" href="#">Know more</a> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Home Paralx End--> 

</div>
<!--Main Content End--> 




<jsp:include page="inc/foot.jsp"/>