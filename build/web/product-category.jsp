<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="inc/head.jsp"/>
<!--Main Content Start-->
<div class="cp-page-content bg"> 
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> Product		    
		</p>

		<h2 class="new-sec-title">Products Categories</h2>
		<div class="product-category-row">
			<label class="product-category-sub-head">Gender:</label>
			<div class="row">
				<div class="col-lg-3"><a class="boy" href="categories?action=list&cid=10"><img src="images/boys.png" onmouseover="this.src = 'images/boys2.png';"onmouseout="this.src = 'images/boys.png';"></a></div>
				<div class="col-lg-3"><a class="girl" href="categories?action=list&cid=9"><img src="images/girls.png" onmouseover="this.src = 'images/girls2.png';"onmouseout="this.src = 'images/girls.png';"></a></div>
			</div>            
		</div>
		<div class="product-category-row">
			<label class="product-category-sub-head">Age Range:</label>
			<div class="row">
				<div class="col-lg-3"><a class="baby" href="categories?action=list&cid=11"><img src="images/baby.png" onmouseover="this.src = 'images/baby2.png';"onmouseout="this.src = 'images/baby.png';"></a></div>
				<div class="col-lg-3"><a class="toddler" href="categories?action=list&cid=12"><img src="images/Toddler.png" onmouseover="this.src = 'images/Toddler2.png';"onmouseout="this.src = 'images/Toddler.png';"></a></div>
				<div class="col-lg-3"><a class="kids" href="categories?action=list&cid=13"><img src="images/kids.png" onmouseover="this.src = 'images/kids2.png';"onmouseout="this.src = 'images/kids.png';"></a></div>
				<div class="col-lg-3"><a class="teenager"href="categories?action=list&cid=14"><img src="images/teenager.png" onmouseover="this.src = 'images/teenager2.png';"onmouseout="this.src = 'images/teenager.png';"></a></div>
			</div>            
		</div>
		<div class="product-category-row">
			<label class="product-category-sub-head">Item Category:</label>
			<div class="row first-row-category">
				<div class="col-lg-4 figures"><a class="figures" href="categories?action=list&cid=15"><img src="images/Action Figures.png" onmouseover="this.src = 'images/Action Figures2.png';"onmouseout="this.src = 'images/Action Figures.png';""></a></div>
				<div class="col-lg-4"><a class="dolls"href="categories?action=list&cid=16"><img src="images/Dolls.png" onmouseover="this.src = 'images/Dolls2.png';"onmouseout="this.src = 'images/Dolls.png';"></a></div>
				<div class="col-lg-4"><a class="games"href="categories?action=list&cid=17"><img src="images/Games & Puzzle.png" onmouseover="this.src = 'images/Games and Puzzle2.png';"onmouseout="this.src = 'images/Games & Puzzle.png';"></a></div>
			</div>
			<div class="row second-row-category">
				<div class="col-lg-4"><a class="lego"href="categories?action=list&cid=18"><img src="images/lego.png" onmouseover="this.src = 'images/lego2.png';"onmouseout="this.src = 'images/lego.png';"></a></div>
				<div class="col-lg-4"><a class="playset"href="categories?action=list&cid=20"><img src="images/Play Sets.png" onmouseover="this.src = 'images/Play Sets2.png';"onmouseout="this.src = 'images/Play Sets.png';"></a></div>
				<div class="col-lg-4"><a class="plush"href="categories?action=list&cid=19"><img src="images/Plush.png" onmouseover="this.src = 'images/Plush2.png';"onmouseout="this.src = 'images/Plush.png';"></a></div>              
			</div>              
		</div>
	</div>
</div>


<jsp:include page="inc/foot.jsp"/>