<%@page import="java.util.ArrayList"%>
<%@page import="model.Item"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Recycle Toy Application
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-check-square-o"></i> Recycle Toy Application
					</li>
				</ol>
			</div>
		</div>

		<p >
			<a  class="pull-right btn btn-danger fa fa-check-square-o">     Approval</a>
			<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
			<a class="pull-right btn btn-success fa fa-search-plus">     View</a>
		</p>	


		<div class= table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-head">
					<tr>
						<th>ID</th>
						<th>User</th>
						<th>Toy Name</th>
						<th>Selling Price</th>
						<th>State</th>
						<th>Created Date</th>

						<th colspan=3>Action</th>
					</tr>
				</thead>
				<tbody>
					<%
						ArrayList<Item> items = (ArrayList<Item>) request.getAttribute("items");
						for (Item item : items) {
					%>
					<tr>
						<td><%= item.getItemID()%></td>
						<td><%= item.getUserID()%></td>
						<td><%= item.getItemName()%></td>
						<td><%= item.getPrice()%></td>
						<td><%= item.getState()%></td>
						<td><%= item.getCreatedDate()%></td>
						<td>
							<a href="ItemController?action=view&itemID=<%= item.getItemID()%>" class="btn btn-success fa fa-search-plus"></a>
							<a href="ItemController?action=edit&itemID=<%= item.getItemID()%>" class="btn btn-success fa fa-pencil-square-o"></a>
							<% if (item.getState().equalsIgnoreCase("Pending")) {%>
							<a href="RecycleController?action=approve&itemID=<%= item.getItemID()%>" class="btn btn-danger fa fa-check-square-o">Approval</a>
							<% } %>
						</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>