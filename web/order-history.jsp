
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="inc/head.jsp"/>  
<jsp:useBean id="user" type="model.User" scope="request"/>
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> My Account
		</p>
		<h2 class="new-sec-title">Order History</h2>
		<div class="category-form" >

			<div class="col-sm-3">
				<p ></p>
				<form action="profile"  method="POST"class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label" style="color: #888; font-size: 14px; font-family: Arial;">My Account</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="profile">Account Profile</a><br>
								<a href="payment">Payment Method</a><br>
								<a href="order">Order History</a><br>
								<a href="recycle">Recycle Toy</a>
							</p>
					</fieldset>
				</form>
			</div>

			<div class="col-sm-9">
				
				<form action="#" class="horizontal-form order-form">
					<fieldset class="category-border" style="margin-top: 10px;">
						<div class="history-form-heading form-control-label">Processing Order</div>
						
						<div class="history-inner-form row">
							<div class="col-sm-2" style="margin-left: 45px;">
								<p class="history-inner-heading" >Order Id</p>
							</div>
							<div class="col-sm-2">
								<p class="history-inner-heading">Order Status</p>
							</div>
							<div class="col-sm-2"style="margin-left: 20px;">
								<p class="history-inner-heading">Order Date</p>
							</div>						
							<div class="col-sm-2"style="margin-left: 20px;">
								<p class="history-inner-heading">Total Amount</p>
							</div>
							<div class="col-sm-2"style="margin-left: 20px;">
								<p class="history-inner-heading">Detail</p>
							</div>
						</div>
						<c:forEach items="${orders}" var="order">
						<div class="row">
							<div class="col-sm-2">
								<p class="history-inner-data" style="margin-left: 45px;">${order.orderID}</p>
							</div>
							<div class="col-sm-2">
								<p class="history-inner-data"style="margin-left: 46px;">${order.orderStatus}</p>
							</div>
							<div class="col-sm-3">
								<p class="history-inner-data"style="margin-left: 65px;">${order.orderDate}</p>
							</div>
							
							<div class="col-sm-2">
								<p class="history-inner-data"style="margin-left: 28px;">${order.totalAmount}</p>
							</div>
							<div class="col-sm-2">
								<p class="history-inner-data"style="margin-left: 46px;"><a href="order?action=view&orderID=${order.orderID}">Detail</a></p>
							</div>
						</div>
						</c:forEach>	
					</fieldset>
				</form>
				
			</div>  

		</div>  
	</div>        
</div>
</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>  