<%@page import="java.util.ArrayList"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">

	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Add User
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-users"></i> <a href="UserController?action=list">User</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Add User
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='UserController' class="form-horizontal">

			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Login Information</legend>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Login Email : </label>
					<div class="col-lg-5">
						<input name="loginEmail" type="email" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Password : </label>
					<div class="col-lg-5">
						<input name="password" type="password" class="form-control">
					</div>
				</div>
			</fieldset>
			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Default Payment Method</legend>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Bank Name : </label>
					<div class="col-lg-5 insert">
						<select name="defaultBankName" class="form-control">
							<option value="Bank of China (Hong Kong)">Bank of China (Hong Kong)</option>
							<option value="Bank of East Asia">Bank of East Asia</option>
							<option value="Hang Seng Bank">Hang Seng Bank</option>
							<option value="Hongkong and Shanghai Banking Corporation">Hongkong and Shanghai Banking Corporation</option>
							<option value="Industrial and Commercial Bank of China (Asia)">Industrial and Commercial Bank of China (Asia)</option>
							<option value="Standard Chartered Bank (Hong Kong)">Standard Chartered Bank (Hong Kong)</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Account Number : </label>
					<div class="col-lg-5">
						<input name="defaultAccountNumber" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Account Holder : </label>
					<div class="col-lg-5">
						<input name="defaultAccountHolder" type="text" class="form-control">
					</div>
				</div>
			</fieldset>
			<fieldset class="registration-border input-font">
				<legend class="registration-border" >Personal Information</legend>		   
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Last Name : </label>
					<div class="col-lg-5">
						<input name="lastName" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">First Name : </label>
					<div class="col-lg-5">
						<input name="firstName" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Gender : </label>
					<div class="col-lg-5">
						<select name="gender" class="form-control">
							<option value="M">Male</option>
							<option value="F">Female</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Contact Number : </label>
					<div class="col-lg-5">
						<input name="contactNumber" type="number" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-4 insert">Shipping Address 1 : </label>
					<div class="col-lg-5">
						<input name="shippingAddress1" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Shipping Address 2 : </label>
					<div class="col-lg-5">
						<input name="shippingAddress2" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-4 insert">Is Admin : </label>
					<div class="col-lg-5">
						<select name="isAdmin" class="form-control">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
					</div>
				</div>
			</fieldset>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-5">

					<a class="btn btn-default  col-lg-3 save-btn" href="UserController?action=list" role="button">Cancel</a>
					<input type="submit" value="Submit" class="btn btn-primary  col-lg-3 save-btn"/>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
