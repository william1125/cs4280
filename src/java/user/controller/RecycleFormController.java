/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import dao.StockDao;
import dao.UserDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Item;
import model.Stock;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Nam
 */
public class RecycleFormController extends HttpServlet {

    private static String LAND = "recycle-toy.jsp";
    private static String INDEX = "index.jsp";
    private ItemDao itemDAO;
    private UserDao userDAO;
    private StockDao stockDAO;
    private CategoryDao categoryDAO;

    public RecycleFormController() {
        super();
        itemDAO = new ItemDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        HttpSession session = request.getSession();
        if (session.getAttribute("userid") == null) {
            response.sendRedirect("login");
        } else {
            int userID = Integer.parseInt( session.getAttribute("userid").toString());
            forward = LAND;
            userDAO = new UserDao();
            categoryDAO = new CategoryDao();
            request.setAttribute("categories", categoryDAO.getAllCategories());
            request.setAttribute("user", userDAO.getUserById(userID));

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String> formFields = new HashMap<String, String>();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            File file;
            int maxFileSize = 5000 * 1024;
            int maxMemSize = 5000 * 1024;
            ServletContext context = this.getServletContext();
            String filePath = context.getInitParameter("file-upload");

            DiskFileItemFactory factory = new DiskFileItemFactory();
            // maximum size that will be stored in memory
            factory.setSizeThreshold(maxMemSize);
            // Location to save data that is larger than maxMemSize.
            factory.setRepository(new File("c:\\tomcat\temp"));

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            // maximum file size to be uploaded.
            upload.setSizeMax(maxFileSize);
            try {
                // Parse the request to get file items.
                List fileItems = upload.parseRequest(request);

                // Process the uploaded file items
                Iterator i = fileItems.iterator();

                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        // Get the uploaded file parameters
                        String fieldName = fi.getFieldName();
                        int idx = fi.getName().lastIndexOf(".");
                        String format = "jpg";
                        if (idx > 0) {
                            format = fi.getName().substring(idx + 1);
                            format = format.toLowerCase();
                        }
                        String fileName = UUID.randomUUID().toString() + "." + format;
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fi.write(file);
                        formFields.put("itemImageURL", fileName);
                    } else {
//                        handleFormFields(request);
                        System.out.println(fi.getFieldName() + " " + fi.getString());
                        String name = fi.getFieldName();
                        String value = fi.getString();
                        if (formFields.get("categories") != null && name.equalsIgnoreCase("categories")) {
                            value = formFields.get("categories") + "," + fi.getString();
                        }
                        formFields.put(name, value);
                        if (name.equalsIgnoreCase("itemID")) {
                            System.out.println("test itemID: " + formFields);
                        }
                    }
                }
                handleFormFields(request, formFields);
            } catch (Exception ex) {
                System.out.println(ex);
            }

            RequestDispatcher view = request.getRequestDispatcher(INDEX);
            request.setAttribute("items", itemDAO.getAllItems());
            view.forward(request, response);
        }
    }

    private void handleFormFields(HttpServletRequest request, Map<String, String> fields) {
        System.out.println(fields);
        Item item = new Item();
        HttpSession session = request.getSession();
        int userID = Integer.parseInt(session.getAttribute("userid").toString());
        item.setUserID(userID);
        item.setItemName(fields.get("itemName"));
        item.setPrice(Double.parseDouble(fields.get("price")));
        item.setAvailableQuantity(1);
        item.setFullDescription(fields.get("fullDescription"));
        item.setMagicDescription("");
        item.setBareNecessities("");
        item.setDeliveryDetail("");
        item.setItemImageURL(fields.get("itemImageURL"));
        item.setState("Pending");
        item.setItemType("Recycle");
        item.setColor(fields.get("color"));
        item.setWidth(Float.parseFloat(fields.get("width")));
        item.setHeight(Float.parseFloat(fields.get("height")));
        item.setMaterial(fields.get("material"));
        String categories[] = fields.get("categories").split(",");
        String itemID = fields.get("itemID");
        if (itemID == null || itemID.isEmpty()) {
            System.out.println("---POST: Insert---");
            int itemId = itemDAO.addItem(item);
            System.out.println("item ID: " + itemId + " availableQuantity: " + item.getAvailableQuantity());
            if (categories != null && categories.length != 0) {
                for (int j = 0; j < categories.length; j++) {
                    itemDAO.addCategory(itemId, Integer.parseInt(categories[j]));
                }
            }
            if (itemId > 0) {
                for (int k = 0; k < item.getAvailableQuantity(); k++) {
                    Stock stock = new Stock();
                    stock.setItemID(itemId);
                    stock.setSerialNumber(UUID.randomUUID().toString());
                    stock.setSalesStatus("Available");
                    stockDAO = new StockDao();
                    stockDAO.addStock(stock);
                }
            }
            ServletContext context = getServletContext();
            String filePath = context.getInitParameter("file-upload");
            String contentType = request.getContentType();
        } else {
            System.out.println("---POST: update---");
            item.setItemID(Integer.parseInt(itemID));
            itemDAO.updateItem(item);
            itemDAO.deleteCategory(item.getItemID());
            if (categories != null && categories.length != 0) {
                for (int j = 0; j < categories.length; j++) {
                    itemDAO.addCategory(item.getItemID(), Integer.parseInt(categories[j]));
                }
            }
        }
    }

}
