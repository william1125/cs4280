/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ItemDao;
import dao.ReviewDao;
import dao.UserDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Review;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Nam
 */
public class ReviewController extends HttpServlet {

    private static String INSERT = "admin/insertReview.jsp";
    private static String EDIT = "admin/editReview.jsp";
    private static String LIST = "admin/listReview.jsp";
    private static String VIEW = "admin/viewReview.jsp";
    private ReviewDao reviewDAO;
    private UserDao userDAO;
    private ItemDao itemDAO;

    public ReviewController() {
        super();
        reviewDAO = new ReviewDao();
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("delete")) {
                int reviewID = Integer.parseInt(request.getParameter("reviewID"));
                reviewDAO.deleteReview(reviewID);
                forward = LIST;
                request.setAttribute("reviews", reviewDAO.getAllReviews());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                int reviewID = Integer.parseInt(request.getParameter("reviewID"));
                Review review = reviewDAO.getReviewById(reviewID);
                userDAO = new UserDao();
                itemDAO = new ItemDao();
                request.setAttribute("review", review);
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("items", itemDAO.getAllItems());
            } else if (action.equalsIgnoreCase("view")) {
                forward = VIEW;
                int reviewID = Integer.parseInt(request.getParameter("reviewID"));
                Review review = reviewDAO.getReviewById(reviewID);
                userDAO = new UserDao();
                itemDAO = new ItemDao();
                request.setAttribute("review", review);
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("items", itemDAO.getAllItems());
            } else if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                request.setAttribute("reviews", reviewDAO.getAllReviews());
            } else {
                forward = INSERT;
                userDAO = new UserDao();
                itemDAO = new ItemDao();
                int replyID = 0;
                if (request.getParameter("reviewID") != null) {
                    replyID = Integer.parseInt(request.getParameter("reviewID"));
				
		Review review = reviewDAO.getReviewById(replyID);
		request.setAttribute("review", review);
                }

                request.setAttribute("replyID", replyID);
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("items", itemDAO.getAllItems());
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String> formFields = new HashMap<String, String>();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            File file;
            int maxFileSize = 5000 * 1024;
            int maxMemSize = 5000 * 1024;
            ServletContext context = this.getServletContext();
            String filePath = context.getInitParameter("file-upload");

            DiskFileItemFactory factory = new DiskFileItemFactory();
            // maximum size that will be stored in memory
            factory.setSizeThreshold(maxMemSize);
            // Location to save data that is larger than maxMemSize.
            factory.setRepository(new File("c:\\tomcat\temp"));

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            // maximum file size to be uploaded.
            upload.setSizeMax(maxFileSize);
            try {
                // Parse the request to get file items.
                List fileItems = upload.parseRequest(request);

                // Process the uploaded file items
                Iterator i = fileItems.iterator();

                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        // Get the uploaded file parameters
                        String fieldName = fi.getFieldName();
                        int idx = fi.getName().lastIndexOf(".");
                        String format = "jpg";
                        if (idx > 0) {
                            format = fi.getName().substring(idx + 1);
                            format = format.toLowerCase();
                        }
                        String fileName = UUID.randomUUID().toString() + "." + format;
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fi.write(file);
                        formFields.put("reviewImageURL", fileName);
                    } else {
                        //                        handleFormFields(request);
                        System.out.println(fi.getFieldName() + " " + fi.getString());
                        String name = fi.getFieldName();
                        String value = fi.getString();
                        formFields.put(name, value);
                    }
                }
                handleFormFields(request, formFields);
            } catch (Exception ex) {
                System.out.println(ex);
            }
            RequestDispatcher view = request.getRequestDispatcher(LIST);
            request.setAttribute("reviews", reviewDAO.getAllReviews());
            view.forward(request, response);
        }
    }

    private void handleFormFields(HttpServletRequest request, Map<String, String> fields) {
        System.out.println(fields);
        Review review = new Review();
        review.setItemID(Integer.parseInt(fields.get("itemID")));
        review.setUserID(Integer.parseInt(fields.get("userID")));
        if (fields.get("replyID").length() > 0) {
            review.setReplyID(Integer.parseInt(fields.get("replyID")));
        }
        review.setTitle(fields.get("title"));
        review.setContent(fields.get("content"));
        if (fields.get("replyID").length() == 0) {
            review.setItemRating(Float.parseFloat(fields.get("itemRating")));
            review.setReviewImageURL(fields.get("reviewImageURL"));
        }

        String reviewID = null;
        if (fields.get("reviewID") != null) {
            reviewID = fields.get("reviewID");
        }

        if (reviewID == null || reviewID.isEmpty()) {
            System.out.println("---POST: Insert---");
            reviewDAO.addReview(review);
        } else {
            System.out.println("---POST: update---");
            review.setReviewID(Integer.parseInt(reviewID));
            reviewDAO.updateReview(review);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
