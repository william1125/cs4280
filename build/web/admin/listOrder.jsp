<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Order
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-newspaper-o"></i> Order
					</li>
				</ol>
			</div>
		</div>
		<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
		<a class="pull-right btn btn-success fa fa-search-plus">     View</a>
		<div class= table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-head">
					<tr>
						<th>ID</th>
						<th>User</th>
						<th>Order Status</th>			 
						<th>Created Date</th>
						<th>Delivery Date</th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${orders}" var="order">
						<tr>
							<td><c:out value="${order.orderID}"/></td>
							<td><c:out value="${order.username}"/></td>
							<td><c:out value="${order.orderStatus}"/></td>
							<td><c:out value='${order.orderDate}'/></td>
							<td><c:out value='${order.deliveryDate} ${order.fromTime}-${order.toTime}'/></td>
							<td>
								<a href="OrderController?action=view&orderID=${order.orderID}" class="btn btn-success fa fa-search-plus"></a>
								<a href="OrderController?action=edit&orderID=${order.orderID}" class="btn btn-info fa fa-pencil-square-o"></a>

							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</body>
</html>