<%@page import="model.Stock"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Product's Stock
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-list"></i> <a href="ItemController?action=list">Product</a>
					</li>
					<li class="active">
						<i class="fa fa-list-ol"></i> Product's Stock
					</li>
				</ol>
			</div>
		</div>	
<a href="StockController?action=insert&itemID=${itemID}" class="btn btn-primary fa fa-plus"> Add Product's Stock</a>
		<p>

			<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
			<a class="pull-right btn btn-success fa fa-search-plus">     View</a>
		</p>

		<div class= table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Serial Number</th>
						<th>Sales Status</th>
						<th>Remarks</th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${stocks}" var="stock">
						<tr>
							<td>${stock.stockID}</td>
							<!--<td>${stock.itemID}</td>-->
							<!--<td>${stock.orderID}</td>-->
							<td>${stock.serialNumber}</td>
							<td>${stock.salesStatus}</td>
							<td><c:if test="${stock.remarks != null}">${stock.remarks}</c:if></td>
								<td>
									<a href="StockController?action=view&stockID=${stock.stockID}" class="btn btn-success fa fa-search-plus"></a>
								<a href="StockController?action=edit&stockID=${stock.stockID}" class="btn btn-info fa fa-pencil-square-o"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>