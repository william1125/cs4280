<jsp:include page="inc/head.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!--Main Content Start-->
<div class="cp-page-content inner-page-content woocommerce" style="padding-top: 0px">
    <div class="container">
        <p class="navigation">
            <a href="LandingController">Home</a>
            > Shopping Cart
        </p>
        <h2 class="new-sec-title">Shopping Cart</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="cart-table">
                    <div>
                        <ul>
                            <!--TABLE CAPS START-->
                            <li class="table-caps">
                                <div class="product">product</div>
                                <div class="detail">detail</div>
                                <div class="price">price</div>
                                <div class="quantity">Quantity</div>
                                <div class="total">Total</div>
                                <div class="del">Del</div>
                            </li>
                            <!--TABLE CAPS END--> 
                            <c:forEach items="${items}" var="item">
                                <!--CART LIST START-->
                                <li>
                                    <div class="product"><a href="product?pid=${item.itemID}"><img alt="" src="data/${item.itemImageURL}"></a></div>
                                    <div class="detail">
                                        <c:set var="description" value="${item.fullDescription}"/>
                                        <p><c:out value="${fn:substring(description, 0, 30)}..."/></p>
                                        <!--<div class="rating"> <i class="fa fa-star"></i><i class="fa fa-star"></i> </div>-->
                                        <p>Product code: ${item.itemID}</p>
                                    </div>
                                    <div class="price">
                                        <p>$ ${item.price}</p>
                                    </div>

                                    <div class="quantity">
                                        <form method='POST' action="cart">
                                            <input type="hidden" value="${item.cartID}" name="cartID">
                                            <input type="hidden" value="edit" name="action">
                                            <input type="hidden" name="itemID" value="${item.itemID}">
                                            <input type='number' name='quantity' value='${item.quantity}' class='qty' min="1" max="${item.availableQuantity}"/>
                                            <input type="submit" value="update" class="btn btn-info">
                                        </form>
                                    </div>

                                    <div class="total">
                                        <p><fmt:formatNumber value="${item.quantity * item.price}" type="number" maxFractionDigits="1"></fmt:formatNumber></p>
                                        </div>
                                        <div class="del">
                                            <a class="fa fa-trash" href="cart?action=delete&cartID=${item.cartID}"></a>
                                    </div>

                                </li>
                                <!--CART LIST END--> 
                            </c:forEach>
                        </ul>
                        <!--					<div class="update-cart">
                                                                        
                                                                </div>-->
                    </div>
                    <div>
                        <form method="POST" action="cart?action=checkout" class="registration-form horizontal-form" style="width: 70%; margin-top: 40px">
                            <fieldset class="account-profile-border registration-border">
                                <legend class="recycle-border">Delivery Detail</legend>

                                <div class="form-group row">
                                    <label class="form-control-label col-sm-4" >Choose a Shipping Address</label>
                                    <!--PROBLEM HERE check out use radio button-->
<!--                                    <div class="form-control-label col-sm-8" >-->
                                        <select name="shippingAddress" class="form-control" style="font-weight: normal; width: 65%">
                                            <option value="${user.shippingAddress1}">${user.shippingAddress1}</option>
                                            <option value="${user.shippingAddress2}">${user.shippingAddress2}</option>
                                        </select>
                                    <!--</div>-->
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-sm-4" >Delivery Schedule Date</label>
                                    <input class="form-control col-sm-8 "  type="text" name="deliveryDate" placeholder="delivery date format:  yyyy-mm-dd" style="font-weight: normal; width: 65%" required>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-sm-4" >Delivery Schedule Timeslot</label>
                                    <select name="timeSlot"  class="form-control col-sm-8 "style="font-weight: normal; width: 65%">
                                        <c:forEach items="${timeSlots}" var="timeSlot">
                                            <option value="${timeSlot.timeSlotID}">${timeSlot.fromTime} - ${timeSlot.toTime}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <input type="submit" class="pull-right btn btn-primary" value="Check Out">

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>