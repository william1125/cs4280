/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Category;
import model.User;

/**
 *
 * @author KaYee
 */
public class PaymentMethodController extends HttpServlet {

    private static String LAND = "payment-method.jsp";
    private static String INDEX = "index.jsp";
    private UserDao userDAO;

    public PaymentMethodController() {
        super();
        userDAO = new UserDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        forward = LAND;
        HttpSession session = request.getSession();
        if (session.getAttribute("userid") == null) {
            response.sendRedirect("login");
        } else {
            int userID = Integer.parseInt(session.getAttribute("userid").toString());
            User user;
            user = userDAO.getUserById(userID);
            request.setAttribute("user", user);
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = new User();
        System.out.println(user.getUserID());
        user.setLoginEmail(request.getParameter("loginEmail"));
        user.setLastName(request.getParameter("lastName"));
        user.setFirstName(request.getParameter("firstName"));
        user.setGender(request.getParameter("gender"));
        user.setContactNumber(Integer.parseInt(request.getParameter("contactNumber")));
        user.setPassword(request.getParameter("pwd"));
        user.setShippingAddress1(request.getParameter("shippingAddress1"));
        user.setShippingAddress2(request.getParameter("shippingAddress2"));

        user.setDefaultBankName(request.getParameter("bankName"));
        user.setDefaultAccountHolder(request.getParameter("defaultAccountHolder"));
        user.setDefaultAccountNumber(request.getParameter("msg2") + request.getParameter("msg3") + request.getParameter("msg4"));

        user.setIsAdmin(0);
        String userID = request.getParameter("userID");

        System.out.println("---POST: update---");
        user.setUserID(Integer.parseInt(userID));
        userDAO.updateUser(user);

        user = userDAO.getUserById(Integer.parseInt(userID));
        request.setAttribute("user", user);
        RequestDispatcher view = request.getRequestDispatcher(LAND);
        view.forward(request, response);

    }
}
