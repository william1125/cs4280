/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.ItemDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Item;
import model.User;

/**
 *
 * @author KaYee
 */
public class SearchController extends HttpServlet {

	private static String LAND = "search.jsp";
	private ItemDao itemDAO;
	private UserDao userDAO;

	public SearchController() {
		super();
		itemDAO = new ItemDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		forward = LAND;
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
			response.sendRedirect("login");
		} else {

			String keywords = request.getParameter("keywords");

			ArrayList<Item> products = (ArrayList<Item>) itemDAO.getItemsByKeywords(keywords);

			request.setAttribute("itemNum", products.size());
			request.setAttribute("products", itemDAO.getItemsByKeywords(keywords));
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		User user = new User();
		user.setLoginEmail(request.getParameter("loginEmail"));
		user.setLastName(request.getParameter("lastName"));
		user.setFirstName(request.getParameter("firstName"));
		user.setGender(request.getParameter("gender"));
		user.setContactNumber(Integer.parseInt(request.getParameter("contactNumber")));
		if (request.getParameter("password") == "" || request.getParameter("password") == null) {
			user.setPassword(request.getParameter("oldpwd"));

		} else {
			user.setPassword(request.getParameter("password"));
		}

		user.setShippingAddress1(request.getParameter("shippingAddress1"));
		user.setShippingAddress2(request.getParameter("shippingAddress2"));
		user.setDefaultAccountHolder(request.getParameter("holder"));
		user.setDefaultAccountNumber(request.getParameter("account"));
		user.setDefaultBankName(request.getParameter("bank"));
		user.setIsAdmin(0);
		String userID = request.getParameter("userID");

		System.out.println("---POST: update---");
		user.setUserID(Integer.parseInt(userID));
		userDAO.updateUser(user);

		user = userDAO.getUserById(Integer.parseInt(userID));
		request.setAttribute("user", user);
		RequestDispatcher view = request.getRequestDispatcher(LAND);
		view.forward(request, response);

	}

}
