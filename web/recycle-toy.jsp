<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="inc/head.jsp"/>  
<jsp:useBean id="user" type="model.User" scope="request"/>

<!--Main Content Start-->
<div class="cp-page-content bg">
	<div class="container" style="margin-bottom: 30px;">
		<p class="navigation">
			<a href="LandingController">Home</a>
			 > 
			<a href="categories?action=list&cid=22">Recycle Toys</a>	
			 > Recycle Toy Application
		</p>
		<h2 class="new-sec-title" id="recycle-form-top">Recycle Toys Application Form</h2>

		<form action="recycle" method="POST" class="recycle-form horizontal-form" enctype="multipart/form-data">
			<!-- STEP 1 Start -->
			<div target="1" class="steps">
				<fieldset class="recycle-border">
					<legend class="recycle-border">Product Name</legend>
					<p>Please provide the product name (Example: Star War 2015 Action Figure)</p>
					<input type="text" name="itemName" class="input-field" placeholder="Input item name here" required style="font-weight: normal; display: block; width: 100%; height: 34px;  padding: 6px 12px;  
						  font-size: 14px; line-height: 1.42857143; color: #555; background-color: #fff; background-image: none;  border: 1px solid #ccc;  border-radius: 4px;"></input>
				</fieldset>
				<fieldset class="recycle-border">
					<legend class="recycle-border">Product Category</legend>
					<div class="row">
						<label class="recycle-label-1">Gender:</label>

						<div class="row">
							<div class="col-lg-3"><img src="images/boys2.png" class="gender-img category-img" target="10" cat="boys"></div>
							<div class="col-lg-3"><img src="images/girls2.png" class="gender-img category-img" target="9" cat="girls"></div>
						</div>            
					</div>
					<div class="row">
						<label class="recycle-label-1">Age Range:</label>
						<div class="row">
							<div class="col-lg-3"><img src="images/baby2.png" class="age-range-img category-img" target="11" cat="baby"></div>
							<div class="col-lg-3"><img src="images/Toddler2.png" class="age-range-img category-img" target="12" cat="toddler"></div>
							<div class="col-lg-3"><img src="images/kids2.png" class="age-range-img category-img" target="13" cat="kids"></div>
							<div class="col-lg-3"><img src="images/teenager2.png" class="age-range-img category-img" target="14" cat="teenager"></div>
						</div>            
					</div>
					<div class="row">
						<label class="recycle-label-1">Item Category:</label>
						<div class="row first-row-category">
							<div class="col-lg-4"><img src="images/Action Figures2.png" class="age-range-img category-img" target="15" cat="action figure"></div>
							<div class="col-lg-4"><img src="images/Dolls2.png" class="age-range-img category-img" target="16" cat="dolls"></div>
							<div class="col-lg-4"><img src="images/Games and Puzzle2.png" class="age-range-img category-img" target="17" cat="games and puzzle"></div>
						</div>
						<div class="row">
							<div class="col-lg-4"><img src="images/lego2.png" class="age-range-img category-img" target="18" cat="lego"></div>
							<div class="col-lg-4"><img src="images/Play Sets2.png" class="age-range-img category-img" target="19" cat="play set"></div>
							<div class="col-lg-4"><img src="images/Plush2.png" class="age-range-img category-img" target="20" cat="plush"></div>              
						</div>              
					</div>
				</fieldset>
				<div style="display: none">
					<c:forEach items="${categories}" var="category">
						<input type="checkbox" name="categories" value="${category.categoryID}">${category.categoryName}
					</c:forEach>
					<input type="checkbox" name="categories" value="22" checked>Recycle
				</div>
				<div class="row pull-right">
					<div class="col-lg-12">
						<input type="button" class="btn btn-lg reg-btn" style="font-size: 14px" onclick="location.href='profile'" value="Cancel">

						<button class="btn btn-primary btn-lg next-btn " step="2" style="font-size: 14px">Next</button>
					</div>
				</div>
			</div>
			<!-- STEP 1 End -->

			<!-- STEP 2 Start -->
			<div target="2" class="steps" style="display: none;">
				<fieldset style="width: 80%; margin-left: 80px" class="recycle-border2 clear" >
					<legend class="recycle-border">Product Description</legend>
					<div class="form-group row">
						<label for="color" class="col-sm-3 form-control-label" >Color:</label>
						<div class="col-sm-9">
							<select name="color" class="form-control" style="width: 100%">  
								<option value="Black">Black</option>
								<option value="White">White</option>
								<option value="Red">Red</option>
								<option value="Yellow">Yellow</option>
								<option value="Orange">Orange</option>
								<option value="Green">Green</option>
								<option value="Blue">Blue</option>
								<option value="Purple">Purple</option>
							</select>
						</div>
					</div> 
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">Width:</label>
						<div class="col-sm-9">
							<input type="number" style="font-weight: normal"class="form-control" name="width" ></input>              
						</div>
					</div> 
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">Height:</label>
						<div class="col-sm-9">
							<input type="number" style="font-weight: normal" class="form-control" name="height" ></input>              
						</div>
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">Material:</label>
						<div class="col-sm-9">
							<input style="font-weight: normal" type="text" class="form-control" name="material" ></input>              
						</div>
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">*Description:</label>
						<div class="col-sm-9">
							<textarea style="font-weight: normal" class="form-control" name="fullDescription" required="" rows="3"></textarea>             
						</div>
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">*Price:</label>
						<div class="col-sm-9">
							<div class="input-group">
								<div class="input-group-addon">$</div>
								<input style="font-weight: normal" type="number" class="form-control" name="price" required min="1" max="100" step="0.1"></input>    
							</div>          
						</div>
					</div>            
					<div class="form-group row">
						<label for="size" class="col-sm-3 form-control-label">*Photos:</label>
						<div class="col-sm-9">
							<input type="file" class="" name="itemImage" required style="font-weight: normal"></input>              
						</div>
					</div> 
				</fieldset>
				<fieldset style="width: 80%; margin-left: 80px; margin-top: 30px;" class="recycle-border2 " >
					<legend class="recycle-border">Payment Method</legend>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Bank Name:</label>
						<div class="col-sm-8">
							<input name="bankName" type="text" class="form-control" readonly value="${user.defaultBankName}" style="font-weight: normal"></input>            
						</div>            
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Account Number:</label>
						<div class="col-sm-8">
							<input name="accountNumber" type="number" class="form-control" readonly value="${user.defaultAccountNumber}" style="font-weight: normal"></input>            
						</div>            
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Account Holder:</label>
						<div class="col-sm-8">
							<input name="accountHolder" type="text" class="form-control" readonly value="${user.defaultAccountHolder}" style="font-weight: normal"></input>            
						</div>            
					</div>
				</fieldset>
				<div class="row pull-right">
					<div class="col-lg-12" style="margin-right: 95px; margin-top: 20px">
						<button class="btn btn-lg next-btn" step="1"style="font-size: 14px">Back</button>
						<button  class="btn btn-primary btn-lg next-btn" step="3"style="font-size: 14px; margin-left: 10px;">Next</button>
					</div>
				</div>
			</div>
			<!-- STEP 2 End -->

			<!-- REVIEW -->
			<div target="3" class="steps" style="display: none;">
				<fieldset style="width: 80%; margin-left: 80px" class="recycle-border2">
					<legend class="recycle-border">Review</legend>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Product Name:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-product-name" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div>       
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Product Categories:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-categories" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div>  
					<hr>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Color:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-color" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div>    
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Size:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-size" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div> 
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Material:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-material" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div> 
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Product Description:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-description" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div>  
					<hr>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Selling Price:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-price" style="font-weight: normal; font-size: 14px; font-family: Arial"></p>            
						</div>
					</div>     
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Bank Name:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-bank-name" style="font-weight: normal; font-size: 14px; font-family: Arial">${user.defaultBankName}</p>            
						</div>
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Account Number:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-account-number" style="font-weight: normal; font-size: 14px; font-family: Arial">${user.defaultAccountNumber}</p>            
						</div>
					</div>
					<div class="form-group row">
						<label for="size" class="col-sm-4 form-control-label">Account Holder:</label>
						<div class="col-sm-8">
							<p class="form-control-static" id="review-account-holder" style="font-weight: normal; font-size: 14px; font-family: Arial">${user.defaultAccountHolder}</p>            
						</div>
					</div>         
				</fieldset>
				<div class="row pull-right">
					<div class="col-lg-12" style="margin-right: 95px; margin-top: 20px">
						<button class="btn btn-lg next-btn" step="2"style="font-size: 14px" onclick="location.href='profile'">Cancel</button>
						<button class="btn btn-warning btn-lg next-btn" step="1"style="font-size: 14px; margin-left: 10px;">Edit</button>
						<input type="submit" class="btn btn-primary btn-lg" value="Confirm"style="font-size: 14px; margin-left: 10px;">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>  