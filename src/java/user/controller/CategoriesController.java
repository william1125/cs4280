/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Item;

/**
 *
 * @author Nam
 */
public class CategoriesController extends HttpServlet {
    private static String LAND = "product-category2.jsp";
    private ItemDao itemDAO;
    private CategoryDao categoryDAO;
    
    public CategoriesController(){
        super();
        itemDAO = new ItemDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        String sub_cid = request.getParameter("sub_cid");
        
        if(action == null) action = "list";
        int categoryID = Integer.parseInt(request.getParameter("cid"));

         System.out.println("$_GET category ID: "+categoryID);
         if (action.equalsIgnoreCase("list")){
             categoryDAO = new CategoryDao();
            forward = LAND;
            if(sub_cid != null) {
                int subCategoryID = Integer.parseInt(request.getParameter("sub_cid"));
                ArrayList<Item> products = (ArrayList<Item>)itemDAO.getApprovedItemsByCategories(categoryID, subCategoryID);
                request.setAttribute("itemNum", products.size());
                request.setAttribute("products", products);
                request.setAttribute("category", categoryDAO.getCategoryById(categoryID));
            } else {
                ArrayList<Item> products = (ArrayList<Item>)itemDAO.getApprovedItemsByCategory(categoryID, -1);
                request.setAttribute("itemNum", products.size());
                request.setAttribute("products", products);
                request.setAttribute("category", categoryDAO.getCategoryById(categoryID));
			 
            }
		  request.setAttribute("cid", categoryID);
         } 
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
