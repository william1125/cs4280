/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CartDao;
import dao.ItemDao;
import dao.TimeSlotDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Cart;
import model.User;

/**
 *
 * @author KaYee
 */
public class WishlistController extends HttpServlet {

	private static String LAND = "wishlist.jsp";
	private static String INDEX = "index.jsp";
	private ItemDao itemDAO;
	private UserDao userDAO;
	private CartDao cartDAO;
	private TimeSlotDao timeSlotDAO;

	public WishlistController() {
		super();
		cartDAO = new CartDao();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		String action = request.getParameter("action");
		if (action == null) {
			action = "list";
		}
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
			response.sendRedirect("login");
		} else {
			int userID = Integer.parseInt(session.getAttribute("userid").toString());
			if (action.equalsIgnoreCase("delete")) {
				int cartID = Integer.parseInt(request.getParameter("cartID"));
				cartDAO.deleteCart(cartID);
			}
			request.setAttribute("items", cartDAO.getAllCartsByUserID(userID));
			System.out.println(cartDAO.getAllCartsByUserID(userID));

			forward = LAND;
			userDAO = new UserDao();
			timeSlotDAO = new TimeSlotDao();
			request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
			request.setAttribute("user", userDAO.getUserById(userID));

			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		String action = request.getParameter("action");
		if (action == null) {
			action = "list";
		}
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
			response.sendRedirect("login");
		} else {
			int userID = Integer.parseInt(session.getAttribute("userid").toString());
                                                    timeSlotDAO = new TimeSlotDao();
			request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
			if (action.equalsIgnoreCase("insert")) {

				int itemID = Integer.parseInt(request.getParameter("itemID"));
				Cart cart = new Cart();
				cart.setUserID(userID);
				cart.setItemID(itemID);
				cart.setQuantity(Integer.parseInt(request.getParameter("quantity")));
				cartDAO.addCart(cart);
			} else if (action.equalsIgnoreCase("checkout")) {
				userDAO = new UserDao();
				Date deliveryDate = null;
				try {
					deliveryDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("deliveryDate"));
				} catch (ParseException e) {
					System.out.println("ParseException: " + e.getMessage());
				}
				int timeSlotID = Integer.parseInt(request.getParameter("timeSlot"));
                                String shippingAddress = request.getParameter("shippingAddress");
				User user = userDAO.getUserById(userID);
				System.out.println("checkout controller: "+user.getDefaultBankName()+" userid: "+user.getDefaultAccountNumber());
				cartDAO.checkOut(userID, deliveryDate, timeSlotID, user.getDefaultBankName(), user.getDefaultAccountNumber(), user.getDefaultAccountHolder(), shippingAddress);
			} else {
				int itemID = Integer.parseInt(request.getParameter("itemID"));
				Cart cart = new Cart();
				cart.setUserID(userID);
				cart.setItemID(itemID);
				cart.setQuantity(Integer.parseInt(request.getParameter("quantity")));
				cart.setCartID(Integer.parseInt(request.getParameter("cartID")));
				cartDAO.updateCart(cart);
			}

			forward = LAND;
			userDAO = new UserDao();
			request.setAttribute("user", userDAO.getUserById(userID));
			request.setAttribute("items", cartDAO.getAllCartsByUserID(userID));
			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
	}
}
