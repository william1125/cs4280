<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Product Comment
				</h3>
				<ol class="breadcrumb">
				
					<li class="active">
						<i class="fa fa-comments"></i> Product Comment
					</li>
				</ol>
			</div>
		</div>	

		<p>
			<a class="pull-right btn btn-warning fa fa-comments">     Reply Comment</a>
			<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
			<a class="pull-right btn btn-success fa fa-search-plus">     View</a>

		</p>

		<div class= table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-head">
					<tr>
						<th>ID</th>
						<th>User</th>
						<th>Review Product</th>
						<th>Product Rating</th>
						<th>Title</th>
						<th>Created Date</th>
						<th colspan=3>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${reviews}" var="review">
						<tr>
							<td>${review.reviewID}</td>
							<td>${review.firstName} ${review.lastName} [ID: ${review.userID}]</td>
							<td>${review.itemName} [ID: ${review.itemID}]</td>
							<td>${review.itemRating}</td>
							<td>${review.title}</td>
							<td>${review.createdDate}</td>
							<td>

								<a href="ReviewController?action=view&reviewID=${review.reviewID}" class="btn btn-success fa fa-search-plus"></a>
								<a href="ReviewController?action=edit&reviewID=${review.reviewID}" class="btn btn-info fa fa-pencil-square-o"></a>
								<c:if test="${review.replyID == 0}">
									<a href="ReviewController?action=insert&reviewID=${review.reviewID}" class="btn btn-warning fa fa-comments"></a>	
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>