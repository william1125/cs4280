<%@page import="model.Category"%>
<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Add Product
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-list"></i> <a href="ItemController?action=list">Product</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Add Product
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='ItemController' class="form-horizontal" enctype="multipart/form-data">
			<div class="col-lg-6 left-fieldset">
				<fieldset class="registration-border input-font">
					<legend class="registration-border" >Product Information</legend>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Name :</label>
						<div class="col-lg-8">
							<input name="itemName" type="text" class="form-control" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Price:</label>
						<div class="col-lg-8">
							<input name="price" type="number" class="form-control" step="0.1" min="1" max="9999" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Available Quantity:</label>
						<div class="col-lg-8">
							<input name="availableQuantity" type="number" class="form-control" required step="1" min="1" max="9999">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Full Description:</label>
						<div class="col-lg-8">
							<textarea rows="6" name="fullDescription" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Magic Description:</label>
						<div class="col-lg-8">
							<textarea rows="5" name="magicDescription" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Bare Necessities:</label>
						<div class="col-lg-8">
							<textarea rows="2" name="bareNecessities" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Delivery Detail:</label>
						<div class="col-lg-8">
							<textarea  rows="6"name="deliveryDetail" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Image:</label>
						<div class="col-lg-8">
							<input type="file" name="itemImage">
							<!--<input type="text" name="itemImageURL" class="form-control" required>-->
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">State:</label>
						<div class="col-lg-8">
							<select name="state" class="form-control">
								<option value="Pending">Pending</option>
								<option value="Approved">Approved</option>
								<option value="Rejected">Rejected</option>
								<option value="Unavailable">Unavailable</option>
							</select>
						</div>
					</div>

				</fieldset>
			</div>

			<div class="col-lg-6">
				<fieldset class="registration-border input-font">
					<legend class="registration-border" >Recycle Toy Information</legend>	   
					
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Type:</label>
						<div class="col-lg-8">
							<select name="itemType" class="form-control">
								<option value="Sales">Sales Item</option>
								<option id="Recycle" value="Recycle">Recycle Item</option>
							</select>
						</div>
					</div>
					<div class="form-group ">
						<label class="control-label col-lg-4 insert ">User:</label>
						<div class="col-lg-8">
							<select name="userID" class="form-control">
								<option value="0">  </option>
								<%
									ArrayList<User> users = (ArrayList<User>) request.getAttribute("users");
									for (User user : users) {
								%>
								<option value="<%= user.getUserID()%>"><%= user.getFirstName()%> <%=user.getLastName()%></option>
								<%
									}
								%>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Color:</label>
						<div class="col-lg-8">
							<select name="color" class="form-control">
								<option value="N/A"> </option>
								<option value="Black">Black</option>
								<option value="White">White</option>
								<option value="Red">Red</option>
								<option value="Yellow">Yellow</option>
								<option value="Orange">Orange</option>
								<option value="Green">Green</option>
								<option value="Blue">Blue</option>
								<option value="Purple">Purple</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Width:</label>
						<div class="col-lg-8">
							<input type="number" class="form-control" required name="width" step="0.1" min="1" max="999" value="0">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Height:</label>
						<div class="col-lg-8">
							<input type="number" class="form-control" required name="height" step="0.1" min="1" max="999" value="0">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Material:</label>
						<div class="col-lg-8">
							<input type="text"value="N/A" class="form-control"  name="material">
						</div>
					</div>

				</fieldset>

				<fieldset class="registration-border input-font">
					<legend class="registration-border" >Product Category</legend>	
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Category:</label>
						<div class="col-lg-8">
							<%
								ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute("categories");
								for (Category category : categories) {
							%>
							<div class="col-lg-12  insert insert">
								<input type="checkbox" name="categories" value="<%= category.getCategoryID()%>">
								<%= category.getCategoryName()%>
							</div>
							<%
								}
							%>
						</div>
					</div>
				</fieldset>		 
			</div>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="ItemController?action=list" role="button">Cancel</a>
					<input type="submit" value="Add" class="btn btn-primary col-lg-3 save-btn"/>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
