/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nam
 */
public class RecycleController extends HttpServlet {

    private static String APPROVE = "admin/approveRecycle.jsp";
    private static String LIST = "admin/listRecycle.jsp";
    private ItemDao itemDAO;
    private UserDao userDAO;
    private CategoryDao categoryDAO;

    public RecycleController() {
        super();
        itemDAO = new ItemDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                request.setAttribute("items", itemDAO.getAllRecycleItems());
            } else {
                forward = APPROVE;
                int itemID = Integer.parseInt(request.getParameter("itemID"));
                userDAO = new UserDao();
                categoryDAO = new CategoryDao();
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("item", itemDAO.getItemById(itemID));
                request.setAttribute("categories", categoryDAO.getAllCategories());
                request.setAttribute("selectedCategories", itemDAO.getCategories(itemID));
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int itemID = Integer.parseInt(request.getParameter("itemID"));
        String state = request.getParameter("state");
        itemDAO.approveRecycleItem(itemID, state);
        RequestDispatcher view = request.getRequestDispatcher(LIST);
        request.setAttribute("items", itemDAO.getAllRecycleItems());
        view.forward(request, response);
    }

}
