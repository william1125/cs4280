/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nam
 */
public class Stock {
    private int StockID;
    private int ItemID;
    private String ItemName;
    private int OrderID;
    private String SerialNumber;
    private String SalesStatus;
    private String Remarks;
    private float Price;

    /**
     * @return the StockID
     */
    public int getStockID() {
        return StockID;
    }

    /**
     * @param StockID the StockID to set
     */
    public void setStockID(int StockID) {
        this.StockID = StockID;
    }

    /**
     * @return the ItemID
     */
    public int getItemID() {
        return ItemID;
    }

    /**
     * @param ItemID the ItemID to set
     */
    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    /**
     * @return the OrderID
     */
    public int getOrderID() {
        return OrderID;
    }

    /**
     * @param OrderID the OrderID to set
     */
    public void setOrderID(int OrderID) {
        this.OrderID = OrderID;
    }

    /**
     * @return the SerialNumber
     */
    public String getSerialNumber() {
        return SerialNumber;
    }

    /**
     * @param SerialNumber the SerialNumber to set
     */
    public void setSerialNumber(String SerialNumber) {
        this.SerialNumber = SerialNumber;
    }

    /**
     * @return the SalesStatus
     */
    public String getSalesStatus() {
        return SalesStatus;
    }

    /**
     * @param SalesStatus the SalesStatus to set
     */
    public void setSalesStatus(String SalesStatus) {
        this.SalesStatus = SalesStatus;
    }

    /**
     * @return the Remarks
     */
    public String getRemarks() {
        return Remarks;
    }

    /**
     * @param Remarks the Remarks to set
     */
    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    /**
     * @return the ItemName
     */
    public String getItemName() {
        return ItemName;
    }

    /**
     * @param ItemName the ItemName to set
     */
    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

	/**
	 * @return the Price
	 */
	public float getPrice() {
		return Price;
	}

	/**
	 * @param Price the Price to set
	 */
	public void setPrice(float Price) {
		this.Price = Price;
	}
}
