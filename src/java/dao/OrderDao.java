/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class OrderDao {

	private Connection connection;

	public OrderDao() {
		connection = DbUtil.getConnection();
	}

	public int addOrder(Order order) {
		try {
			String sql = "insert into [ORDER](UserID, TimeSlotID, OrderStatus, OrderDate, DeliveryDate, BankName, AccountNumber, AccountHolder, ShippingAddress) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, order.getUserID());
			preparedStatement.setInt(2, order.getTimeSlotID());
			preparedStatement.setString(3, order.getOrderStatus());
			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
			preparedStatement.setDate(4, timeNow);
			preparedStatement.setDate(5, new java.sql.Date(order.getDeliveryDate().getTime()));
			preparedStatement.setString(6, order.getBankName());
			preparedStatement.setString(7, order.getAccountNumber());
			preparedStatement.setString(8, order.getAccountHolder());
			preparedStatement.setString(9, order.getShippingAddress());
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException ex) {
			System.out.println("addOrder SQL Exception: " + ex.getMessage());
		}
		return 0;
	}

	public void deleteOrder(int orderID) {
		try {
			String sql = "delete from [ORDER] where OrderID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, orderID);
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("deleteOrder SQL Exception: " + ex.getMessage());
		}
	}

	public void updateOrder(Order order) {
		try {
			String sql = "update [ORDER] set UserID=?, TimeSlotID=?, OrderStatus=?, DeliveryDate=?, BankName=?, AccountNumber=?, AccountHolder=?, ShippingAddress=? where OrderID=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, order.getUserID());
			preparedStatement.setInt(2, order.getTimeSlotID());
			preparedStatement.setString(3, order.getOrderStatus());
			preparedStatement.setDate(4, new java.sql.Date(order.getDeliveryDate().getTime()));
			preparedStatement.setString(5, order.getBankName());
			preparedStatement.setString(6, order.getAccountNumber());
			preparedStatement.setString(7, order.getAccountHolder());
			preparedStatement.setString(8, order.getShippingAddress());
			preparedStatement.setInt(9, order.getOrderID());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			System.out.println("updateOrder SQL Exception: " + ex.getMessage());
		}
	}

	public List<Order> getAllOrders() {
		List<Order> orders = new ArrayList<Order>();
		try {
			Statement statement = connection.createStatement();
			String sql = "select [ORDER].*, [USER].FirstName, [USER].LastName, [TIMESLOT].FromTime, [TIMESLOT].ToTime"
				   + " from [ORDER] inner join [USER] on [ORDER].UserID = [USER].UserID "
				   + " inner join [TIMESLOT] on [ORDER].TimeSlotID=[TIMESLOT].timeSlotID";
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				Order order = new Order();
				order.setOrderID(rs.getInt("OrderID"));
				order.setUserID(rs.getInt("UserID"));
				order.setUsername(rs.getString("FirstName") + " " + rs.getString("LastName"));
				order.setTimeSlotID(rs.getInt("TimeSlotID"));
				order.setFromTime(rs.getString("FromTime"));
				order.setToTime(rs.getString("ToTime"));
				order.setOrderStatus(rs.getString("OrderStatus"));
				order.setOrderDate(rs.getDate("OrderDate"));
				order.setDeliveryDate(rs.getDate("DeliveryDate"));
				order.setBankName(rs.getString("BankName"));
				order.setAccountNumber(rs.getString("AccountNumber"));
				order.setAccountHolder(rs.getString("AccountHolder"));
				order.setShippingAddress(rs.getString("ShippingAddress"));
				order.setTotalAmount(getTotalAmount(rs.getInt("OrderID")));
				orders.add(order);
			}
		} catch (SQLException ex) {
			System.out.println("getAllOrders SQL Exception: " + ex.getMessage());
		}
		return orders;
	}

	public Order getOrderById(int orderID) {
		Order order = new Order();
		try {
			String sql = "select [ORDER].*, [USER].FirstName, [USER].LastName, [TIMESLOT].FromTime, [TIMESLOT].ToTime"
				   + " from [ORDER] inner join [USER] on [ORDER].UserID = [USER].UserID "
				   + " inner join [TIMESLOT] on [ORDER].TimeSlotID=[TIMESLOT].timeSlotID"
				   + " where [ORDER].OrderID = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, orderID);
			ResultSet rs = preparedStatement.executeQuery();

			if (rs.next()) {
				order.setOrderID(rs.getInt("OrderID"));
				order.setUserID(rs.getInt("UserID"));
				order.setUsername(rs.getString("FirstName") + " " + rs.getString("LastName"));
				order.setTimeSlotID(rs.getInt("TimeSlotID"));
				order.setFromTime(rs.getString("FromTime"));
				order.setToTime(rs.getString("ToTime"));
				order.setOrderStatus(rs.getString("OrderStatus"));
				order.setOrderDate(rs.getDate("OrderDate"));
				order.setDeliveryDate(rs.getDate("DeliveryDate"));
				order.setBankName(rs.getString("BankName"));
				order.setAccountNumber(rs.getString("AccountNumber"));
				order.setAccountHolder(rs.getString("AccountHolder"));
				order.setShippingAddress(rs.getString("ShippingAddress"));
				order.setTotalAmount(getTotalAmount(rs.getInt("OrderID")));
			}
		} catch (SQLException ex) {
			System.out.println("getOrderById SQL Exception: " + ex.getMessage());
		}
		return order;
	}

	public ArrayList<Order> getOrdersByUserId(int userID) {
		ArrayList<Order> orders = new ArrayList<Order>();
		try {
			String sql = "select [ORDER].*, [USER].FirstName, [USER].LastName, [TIMESLOT].FromTime, [TIMESLOT].ToTime"
				   + " from [ORDER] inner join [USER] on [ORDER].UserID = [USER].UserID "
				   + " inner join [TIMESLOT] on [ORDER].TimeSlotID=[TIMESLOT].timeSlotID"
				   + " where [ORDER].UserID = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, userID);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setOrderID(rs.getInt("OrderID"));
				order.setUserID(rs.getInt("UserID"));
				order.setUsername(rs.getString("FirstName") + " " + rs.getString("LastName"));
				order.setTimeSlotID(rs.getInt("TimeSlotID"));
				order.setFromTime(rs.getString("FromTime"));
				order.setToTime(rs.getString("ToTime"));
				order.setOrderStatus(rs.getString("OrderStatus"));
				order.setOrderDate(rs.getDate("OrderDate"));
				order.setDeliveryDate(rs.getDate("DeliveryDate"));
				order.setBankName(rs.getString("BankName"));
				order.setAccountNumber(rs.getString("AccountNumber"));
				order.setAccountHolder(rs.getString("AccountHolder"));
				order.setShippingAddress(rs.getString("ShippingAddress"));
				order.setTotalAmount(getTotalAmount(rs.getInt("OrderID")));
				orders.add(order);
			}
		} catch (SQLException ex) {
			System.out.println("getOrdersByUserId SQL Exception: " + ex.getMessage());
		}
		return orders;
	}
	
	public float getTotalAmount(int orderID){
		float totalAmount = 0;
		try{
		String sql = "select SUM(price) AS totalAmount from item where itemid in(" 
			   +" select STOCK.ItemID from STOCK" + " where stock.orderID=?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setInt(1, orderID);
		ResultSet rs = preparedStatement.executeQuery();
		
		if(rs.next()){
			totalAmount = rs.getFloat("totalAmount");
		}
		} catch (SQLException ex) {
			System.out.println("getTotalAmount SQL Exception: "+ex.getMessage());
		}
		return totalAmount;
	}
}
