/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Nam
 */
public class Review {
    private int ReviewID;
    private int ItemID;
    private String ItemName;
    private int UserID;
    private String FirstName;
    private String LastName;
    private int ReplyID;
    private Date CreatedDate;
    private String Title;
    private String Content;
    private float ItemRating;
    private String ReviewImageURL;

    /**
     * @return the ReviewID
     */
    public int getReviewID() {
        return ReviewID;
    }

    /**
     * @param ReviewID the ReviewID to set
     */
    public void setReviewID(int ReviewID) {
        this.ReviewID = ReviewID;
    }

    /**
     * @return the ItemID
     */
    public int getItemID() {
        return ItemID;
    }

    /**
     * @param ItemID the ItemID to set
     */
    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    /**
     * @return the UserID
     */
    public int getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the CreatedDate
     */
    public Date getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate the CreatedDate to set
     */
    public void setCreatedDate(Date CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     * @return the Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title the Title to set
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return the Content
     */
    public String getContent() {
        return Content;
    }

    /**
     * @param Content the Content to set
     */
    public void setContent(String Content) {
        this.Content = Content;
    }

    /**
     * @return the ItemRating
     */
    public float getItemRating() {
        return ItemRating;
    }

    /**
     * @param ItemRating the ItemRating to set
     */
    public void setItemRating(float ItemRating) {
        this.ItemRating = ItemRating;
    }

    /**
     * @return the ReviewImageURL
     */
    public String getReviewImageURL() {
        return ReviewImageURL;
    }

    /**
     * @param ReviewImageURL the ReviewImageURL to set
     */
    public void setReviewImageURL(String ReviewImageURL) {
        this.ReviewImageURL = ReviewImageURL;
    }

    /**
     * @return the ReplyID
     */
    public int getReplyID() {
        return ReplyID;
    }

    /**
     * @param ReplyID the ReplyID to set
     */
    public void setReplyID(int ReplyID) {
        this.ReplyID = ReplyID;
    }

    /**
     * @return the FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     * @param FirstName the FirstName to set
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     * @return the LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName the LastName to set
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return the ItemName
     */
    public String getItemName() {
        return ItemName;
    }

    /**
     * @param ItemName the ItemName to set
     */
    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }
}
