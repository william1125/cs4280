/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Item;

/**
 *
 * @author Nam
 */
public class LandingController extends HttpServlet {
    private static String LAND = "index.jsp";
    private ItemDao itemDAO;
    private CategoryDao categoryDAO;
    
    public LandingController(){
        super();
        itemDAO = new ItemDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if(action == null) action = "list";
        
         if (action.equalsIgnoreCase("list")){
            forward = LAND;
            Category category;
            categoryDAO = new CategoryDao();
            
            category = categoryDAO.getCategoryByName("LEGO");
            request.setAttribute("lego", itemDAO.getApprovedItemsByCategoryWithLimit(category.getCategoryID(), 4));
            ArrayList<Item>items = (ArrayList<Item>) itemDAO.getApprovedItemsByCategoryWithLimit(category.getCategoryID(), 4);
            System.out.println(items.get(0).getItemID());
            category = categoryDAO.getCategoryByName("Dolls");
            request.setAttribute("dolls", itemDAO.getApprovedItemsByCategoryWithLimit(category.getCategoryID(), 4));
            
            category = categoryDAO.getCategoryByName("Plush");
            request.setAttribute("plush", itemDAO.getApprovedItemsByCategoryWithLimit(category.getCategoryID(), 4));      
       
            category = categoryDAO.getCategoryByName("Play Sets");
            request.setAttribute("playSets", itemDAO.getApprovedItemsByCategoryWithLimit(category.getCategoryID(), 4));
         } 
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }
}
