<%@page import="model.Category"%>
<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="item" type="model.Item" scope="request"/>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					View Product
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-list"></i> <a href="ItemController?action=list">Product</a>
					</li>
					<li class="active">
						<i class="fa fa-search-plus"></i> View Product
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='ItemController' class="form-horizontal" enctype="multipart/form-data">
			<div class="col-lg-6 left-fieldset">
				<fieldset class="registration-border input-font">
					<legend class="registration-border" >Product Information</legend>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Name : </label>
						<div class="col-lg-8">
							<input disabled name="itemName" type="text" class="form-control" required value="<%=item.getItemName()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Price : </label>
						<div class="col-lg-8">
							<input disabled name="price" type="number" class="form-control" step="0.1" min="1" max="9999" required value="<%= item.getPrice()%>">
						</div>
					</div>	
					<input name="availableQuantity" type="hidden" class="form-control" required value="<%= item.getAvailableQuantity()%>">
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Full Description : </label>
						<div class="col-lg-8">
							<textarea rows="6" disabled name="fullDescription" class="form-control" required><%= item.getFullDescription()%></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Magic Description : </label>
						<div class="col-lg-8">
							<textarea rows="5" disabled name="magicDescription" class="form-control" required><%= item.getMagicDescription()%></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Bare Necessities : </label>
						<div class="col-lg-8">
							<textarea rows="2" disabled name="bareNecessities" class="form-control" required><%= item.getBareNecessities()%></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Delivery Detail : </label>
						<div class="col-lg-8">
							<textarea rows="6" disabled name="deliveryDetail" class="form-control" required><%= item.getDeliveryDetail()%></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Image : </label>
						<div class="col-lg-8">
							<input rows="6" disabled name="itemImageURL" class="form-control" required value="<%= item.getItemImageURL() %>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">State : </label>
						<div class="col-lg-8">
							<select disabled name="state" class="form-control">
								<option value="Pending" <% if (item.getState().equalsIgnoreCase("Pending")) {%>selected<%}%>>Pending</option>
								<option value="Approved" <% if (item.getState().equalsIgnoreCase("Approved")) {%>selected<%}%>>Approved</option>
								<option value="Rejected" <% if (item.getState().equalsIgnoreCase("Rejected")) {%>selected<%}%>>Rejected</option>
								<option value="Unavailable" <% if (item.getState().equalsIgnoreCase("Unavailable")) {%>selected<%}%>>Unavailable</option>
							</select>
						</div>
					</div>		
				</fieldset>
			</div>	
			<div class="col-lg-6">
				<fieldset class="registration-border input-font">
					<legend class="registration-border" >Recycle Toy Information</legend>

					<div class="form-group">
						<label class="control-label col-lg-4 insert">Item Type : </label>
						<div class="col-lg-8">
							<select disabled name="itemType" class="form-control">
								<option value="Sales" <% if (item.getItemType().equalsIgnoreCase("Sales")) {%>selected<%}%>>Sales Item</option>
								<option value="Recycle" <% if (item.getItemType().equalsIgnoreCase("Recycle")) {%>selected<%}%>>Recycle Item</option>
							</select>
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-lg-4 insert">User : </label>
						<div class="col-lg-8">
							<select disabled name="userID" class="form-control">
								<option>N/A</option>
								<%
									ArrayList<User> users = (ArrayList<User>) request.getAttribute("users");
									for (User user : users) {
								%>
								<option value="<%= user.getUserID()%>"
									   <% if (user.getUserID() == item.getUserID()) { %> selected <% }%>
									   >
									<%= user.getFirstName()%> <%=user.getLastName()%>
								</option>
								<%
									}
								%>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Color : </label>
						<div class="col-lg-8">
							<select disabled name="color" class="form-control">
								<option value="Black" <% if (item.getColor() == "Black") {%>selected<%}%>>Black</option>
								<option value="White" <% if (item.getColor() == "White") {%>selected<%}%>>White</option>
								<option value="Red" <% if (item.getColor() == "Red") {%>selected<%}%>>Red</option>
								<option value="Yellow" <% if (item.getColor() == "Yellow") {%>selected<%}%>>Yellow</option>
								<option value="Orange" <% if (item.getColor() == "Orange") {%>selected<%}%>>Orange</option>
								<option value="Green" <% if (item.getColor() == "Green") {%>selected<%}%>>Green</option>
								<option value="Blue" <% if (item.getColor() == "Blue") {%>selected<%}%>>Blue</option>
								<option value="Purple" <% if (item.getColor() == "Purple") {%>selected<%}%>>Purple</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Width : </label>
						<div class="col-lg-8">
							<input disabled type="number" class="form-control" required name="width" step="0.1" min="1" max="999" value="<%= item.getWidth()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Height : </label>
						<div class="col-lg-8">
							<input disabled type="number" class="form-control" required name="height" step="0.1" min="1" max="999" value="<%= item.getHeight()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Material : </label>
						<div class="col-lg-8">
							<input disabled type="text" class="form-control" required name="material" value="<%= item.getMaterial()%>">
						</div>
					</div>
				</fieldset>

				<fieldset class="registration-border  input-font">
					<legend class="registration-border" >Product Category</legend>			 
					<div class="form-group">
						<label class="control-label col-lg-4 insert">Category : </label>
						<div class="col-lg-8">
							<%
								ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute("categories");
								for (Category category : categories) {
							%>
							<div class="col-lg-12">
								<input disabled type="checkbox" name="categories" value="<%= category.getCategoryID()%>" 
									  <%
										  ArrayList<Integer> selectedCategories = (ArrayList<Integer>) request.getAttribute("selectedCategories");
										  for (int i = 0; i < selectedCategories.size(); i++) {
									  %>
									  <%if (category.getCategoryID() == selectedCategories.get(i)) {%>checked<%}%>
									  <% }%>
									  >
								<%= category.getCategoryName()%>

							</div>
							<%
								}
							%>
						</div>
					</div>
				</fieldset>
			</div>
			<input type="hidden" name="itemID" value="<%= item.getItemID()%>">
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="ItemController?action=list" role="button">Back</a>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
