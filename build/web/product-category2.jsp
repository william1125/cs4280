<jsp:include page="inc/head.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
    String scheme = request.getScheme();
    String serverName = request.getServerName();
    int serverPort = request.getServerPort();
    String contextPath = request.getContextPath();  // includes leading forward slash

    String resultPath = scheme + "://" + serverName + ":" + serverPort;

%>
<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> Product		    
		</p>
		<h2 class="new-sec-title">"${category.categoryName}" Products</h2>
		<input type="hidden" class="cat_id" value="${category.categoryID}">
		<div class="category-form">

			<div class="col-sm-3">
				<p ></p>
				<form action="#" class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label">Categories</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="categories?action=list&cid=10">Boys</a><br>
								<a href="categories?action=list&cid=9">Girls</a><br>
								<a href="categories?action=list&cid=11">Baby</a><br>
								<a href="categories?action=list&cid=12">Toddler</a><br>
								<a href="categories?action=list&cid=13">Kids</a><br>
								<a href="categories?action=list&cid=14">Teenager</a><br>
								<a href="categories?action=list&cid=15">Action Figures</a><br>
								<a href="categories?action=list&cid=16">Dolls</a><br>
								<a href="categories?action=list&cid=17">Games &amp; Puzzle</a><br>
								<a href="categories?action=list&cid=18">LEGO</a><br>
								<a href="categories?action=list&cid=19">Plush</a><br>
								<a href="categories?action=list&cid=20">Play Sets</a></p>
					</fieldset>
				</form>

				<form action="" class="refine-form horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label">Refine By</div>
						<div class="category-inner-form">
							<div>
								<div class="refine-border">
								</div>

								<div class="refine-border">
									<p  class="refine-item-border selection-form-text ">Category</p>
									<select id="category" class="selection form-control " max-length="4">
										<!--<option value="10">Boys</option>
										<option value="9">Girls</option>
										<option value="11">Baby</option>
										<option value="12">Toddler</option>
										<option value="13">Kids</option>
										<option value="14">Teenager</option>-->
										<option></option>
										<option value="15">Action Figures</option>
										<option value="16">Dolls</option>
										<option value="17">Games &amp; Puzzle</option>
										<option value="18">LEGO</option>
										<option value="19">Plush</option>
										<option value="20">Play Sets</option>
									</select>
								</div>

								<div class="refine-border">
									<p  class="refine-item-border selection-form-text ">Gender</p>
									<div class="selection btn-group1" data-toggle="buttons">
										<label class="btn-gender btn-primary1 btn">
											<input type="checkbox" class="gender-checkbox" checked autocomplete="off" value="10"> Boys
										</label>
										<label class="btn-gender btn-primary1 btn">
											<input type="checkbox" class="gender-checkbox" checked autocomplete="off" value="9"> Girls
										</label>
									</div>
								</div>
								<!--QUESTION HERE press button then update-->
								<div class="refine-border">
									<p  class="refine-item-border selection-form-text ">Age Range</p>
									<div class="selection btn-group1" data-toggle="buttons">
										<label class="btn-primary1 btn age-range-btn">
											<input type="checkbox" checked autocomplete="off" value="11"> Babys
										</label>
										<label class="btn-primary1 btn age-range-btn">
											<input type="checkbox" checked autocomplete="off" value="12"> Toddler
										</label>
									</div>
									<div class="selection btn-group1" data-toggle="buttons">
										<label class="btn-primary1 btn age-range-btn">
											<input type="checkbox" checked autocomplete="off" value="13"> Kids
										</label>
										<label class="btn-age btn-primary1 btn age-range-btn">
											<input type="checkbox" checked autocomplete="off" value="14"> Teenager
										</label>
									</div>
								</div>

								<div style="text-align: center; margin-bottom: 10px">
									<a href="categories?action=list&cid=${cid}"class="btn btn-primary" style="width:80%" >Refine	</a>
								</div>
							</div>        
					</fieldset>
				</form>
			</div>

			<div class="col-sm-9">
				<div>
					<div class="category-bar ">
						<p ><c:out value="${itemNum}"/> items</p>
					</div>  
				</div>
				<div role="tabpanel" class="" id="tab1">
					<div class="">
						<div class="product-listing" style="padding-top:0px">
							<ul class="cp-product-list row">
								<c:forEach items="${products}" var="product">
									<li class="col-md-4 col-sm-6">
										<div class="pro-list">
											<div class="thumb"><a href="product?pid=${product.itemID}"><img alt="" src="<%= resultPath%>/data/${product.itemImageURL}"></a></div>
											<div class="text">
												<!--QUESTION HERE same as index-->
												<div class="pro-name">
													<h4>${product.itemName}</h4>
													<p class="price">$<c:out value="${product.price}"/></p>
												</div>

											</div>

											<div class="cart-options"> 
												<a href="#"><i class="fa fa-heart-o"></i>Wishlist</a> 
												<form action="cart" id="item${product.itemID}" method="POST">
													<a onclick="document.getElementById('item${product.itemID}').submit();"style="    border-left: solid 1px #cccccc;"><i class="fa fa-shopping-cart"></i>Cart</a> 
													<input type="hidden" name="action" value="insert">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="itemID" value="${product.itemID}">
												</form>
												<a href="product?pid=${product.itemID}"><i class="fa fa-search"></i>Details</a>
												<!--<div class="rating">  </div>-->
											</div>

										</div>
									</li>
								</c:forEach>
							</ul>              
						</div>
					</div>
				</div>
			</div>  
		</div>        
	</div>
</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>