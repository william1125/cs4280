/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.OrderDao;
import dao.TimeSlotDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Order;
import model.TimeSlot;

/**
 *
 * @author Nam
 */
public class OrderController extends HttpServlet {

    private static String INSERT = "admin/insertOrder.jsp";
    private static String EDIT = "admin/editOrder.jsp";
    private static String LIST = "admin/listOrder.jsp";
    private static String VIEW = "admin/viewOrder.jsp";
    private OrderDao orderDAO;
    private UserDao userDAO;
    private TimeSlotDao timeSlotDAO;

    public OrderController() {
        super();
        orderDAO = new OrderDao();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("delete")) {
                int orderID = Integer.parseInt(request.getParameter("orderID"));
                orderDAO.deleteOrder(orderID);
                forward = LIST;
                request.setAttribute("orders", orderDAO.getAllOrders());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                int orderID = Integer.parseInt(request.getParameter("orderID"));
                Order order = orderDAO.getOrderById(orderID);
                userDAO = new UserDao();
                timeSlotDAO = new TimeSlotDao();
                request.setAttribute("order", order);
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
            } else if (action.equalsIgnoreCase("view")) {
                forward = VIEW;
                int orderID = Integer.parseInt(request.getParameter("orderID"));
                Order order = orderDAO.getOrderById(orderID);
                userDAO = new UserDao();
                timeSlotDAO = new TimeSlotDao();
                request.setAttribute("order", order);
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
            } else if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                request.setAttribute("orders", orderDAO.getAllOrders());
            } else {
                forward = INSERT;
                userDAO = new UserDao();
                timeSlotDAO = new TimeSlotDao();
                request.setAttribute("users", userDAO.getAllUsers());
                request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Order order = new Order();
        order.setUserID(Integer.parseInt(request.getParameter("userID")));
        order.setTimeSlotID(Integer.parseInt(request.getParameter("timeSlotID")));
        order.setOrderStatus(request.getParameter("orderStatus"));
        try {
            Date deliveryDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("deliveryDate"));
            order.setDeliveryDate(deliveryDate);
        } catch (ParseException e) {
            System.out.println("ParseException: " + e.getMessage());
        }
        order.setBankName(request.getParameter("bankName"));
        order.setAccountNumber(request.getParameter("accountNumber"));
        order.setAccountHolder(request.getParameter("accountHolder"));
        order.setShippingAddress(request.getParameter("shippingAddress")); 
        String orderID = request.getParameter("orderID");
        if (orderID == null || orderID.isEmpty()) {
            System.out.println("---POST: Insert---");
            orderDAO.addOrder(order);
        } else {
            System.out.println("---POST: upate---");
            order.setOrderID(Integer.parseInt(orderID));
            orderDAO.updateOrder(order);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST);
        request.setAttribute("orders", orderDAO.getAllOrders());
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
