/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CategoryDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Category;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class CategoryController extends HttpServlet {
    private static String INSERT = "admin/insertCategory.jsp";
    private static String EDIT = "admin/editCategory.jsp";
    private static String LIST = "admin/listCategory.jsp";
    private CategoryDao categoryDAO;
    
    public CategoryController(){
        super();
        categoryDAO = new CategoryDao();
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if(action == null) action = "list";
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")){
            response.sendRedirect("login");
        }else {
        if(action.equalsIgnoreCase("delete")){
            int categoryID = Integer.parseInt(request.getParameter("categoryID"));
            categoryDAO.deleteCategory(categoryID);
            forward = LIST;
            request.setAttribute("categories", categoryDAO.getAllCategories());
        } else if (action.equalsIgnoreCase("edit")){
            forward = EDIT;
            int categoryID = Integer.parseInt(request.getParameter("categoryID"));
            Category category = categoryDAO.getCategoryById(categoryID);
            request.setAttribute("category", category);
        } else if (action.equalsIgnoreCase("list")){
            forward = LIST;
            request.setAttribute("categories", categoryDAO.getAllCategories());
        } else {
            forward = INSERT;
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Category category = new Category();
        category.setCategoryName(request.getParameter("categoryName"));
        String categoryID = request.getParameter("categoryID");
        if(categoryID == null || categoryID.isEmpty()){
            System.out.println("---POST: Insert---");
            categoryDAO.addCategory(category);
        } else {
            System.out.println("---POST: upate---");
            category.setCategoryID(Integer.parseInt(categoryID));
            categoryDAO.updateCategory(category);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST);
        request.setAttribute("categories", categoryDAO.getAllCategories());
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
