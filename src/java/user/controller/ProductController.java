/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import dao.ReviewDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Category;
import model.Item;
import model.Review;

/**
 *
 * @author Nam
 */
public class ProductController extends HttpServlet {

	private static String LAND = "products-detail.jsp";
	private ItemDao itemDAO;
	private CategoryDao categoryDAO;
	private ReviewDao reviewDAO;

	public ProductController() {
		super();
		itemDAO = new ItemDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") != null) {
		    request.setAttribute("userID", session.getAttribute("userid"));
		} 
		int productID = Integer.parseInt(request.getParameter("pid"));

		categoryDAO = new CategoryDao();
		reviewDAO = new ReviewDao();
		forward = LAND;
		ArrayList<Category> categories = itemDAO.getCategoryNames(productID);
		int categoryID = categories.get(0).getCategoryID();

		request.setAttribute("reviews", reviewDAO.getAllReviewsByItemID(productID));
		request.setAttribute("similar_products", itemDAO.getItemsByCategory(categoryID, 4));
		request.setAttribute("product", itemDAO.getItemById(productID));
		request.setAttribute("categories", categories);

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
		    response.sendRedirect("login");
		} else {
		Review review = new Review();
		review.setItemID(Integer.parseInt(request.getParameter("ItemID")));
		review.setUserID(Integer.parseInt(request.getParameter("UserID")));

		review.setTitle(request.getParameter("Title"));
		review.setContent(request.getParameter("Content"));
		review.setItemRating(Integer.parseInt(request.getParameter("ItemRating")));
		review.setReviewImageURL(request.getParameter("ReviewImageURL"));

		System.out.println("---POST: Insert---");
		reviewDAO.addReview(review);
		
		response.sendRedirect("product?pid="+request.getParameter("ItemID"));
//		RequestDispatcher view = request.getRequestDispatcher(LAND);
//		request.setAttribute("users", reviewDAO.getAllReviews());
//		view.forward(request, response);
		}
	}

}
