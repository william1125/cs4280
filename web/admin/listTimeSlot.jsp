<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	
<div class="container-fluid">

	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">
					   Shipping Timeslot
			</h3>
			<ol class="breadcrumb">
				
				<li class="active">
					<i class="fa fa-calendar"></i> Shipping Timeslot
				 </li>
			</ol>
		</div>
	</div>	

<p >
	<a href="TimeSlotController?action=insert" class="btn btn-primary fa fa-plus">     Add Time Slot</a>
	<a  class="pull-right btn btn-danger fa fa-trash-o">     Delete</a>
	<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
</p>

<div class= table-responsive">
<table class="table table-striped table-hover">
        <thead class="table-head">
            <tr>
                <th>ID</th>
                <th>From Time</th>
                <th>To Time</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${timeSlots}" var="timeSlot">
                <tr>
                    <td><c:out value="${timeSlot.timeSlotID}"/></td>
                    <td><c:out value='${timeSlot.fromTime}'/></td>
                    <td><c:out value='${timeSlot.toTime}'/></td>
                    <td>
                        <a href="TimeSlotController?action=edit&timeSlotID=${timeSlot.timeSlotID}" class="btn btn-info fa fa-pencil-square-o"></a>
                        <a href="TimeSlotController?action=delete&timeSlotID=${timeSlot.timeSlotID}" class="btn btn-danger fa fa-trash-o"></a>
	    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
</div>
</div>
</body>
</html>