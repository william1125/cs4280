<%@page import="model.Item"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="stock" type="model.Stock" scope="request"/>
<jsp:include page="../inc/header.jsp"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					View Product's Stock
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-list"></i> <a href="ItemController?action=list">Product</a>
					</li>
					<li class="active">
						<i class="fa fa-list-ol"></i> <a href="StockController?action=list&itemID=${stock.itemID}">Product's Stock</a>
					</li>
					<li class="active">
						<i class="fa fa-search-plus"></i> View Product's Stock
					</li>
				</ol>
			</div>
		</div>

		<form method="POST" action='StockController' class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Item ID : </label>
				<div class="col-lg-5 ">
					<p class="form-control-static">${stock.itemID}</p>
					<input type="hidden" name="itemID" value="${stock.itemID}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Order ID : </label>
				<div class="col-lg-5">
					<input disabled name="orderID" type="number" class="form-control" value="${stock.orderID}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Serial Number : </label>
				<div class="col-lg-5">
					<p class="form-control-static">${stock.serialNumber}</p>
					<input name="serialNumber" type="hidden" value="${stock.serialNumber}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Sales Status : </label>
				<div class="col-lg-5">
					<select disabled name="salesStatus" class="form-control">
						<option value="Available" <% if (stock.getSalesStatus().equalsIgnoreCase("Available")) {%> selected <%}%> >Available</option>
						<option value="Holding" <% if (stock.getSalesStatus().equalsIgnoreCase("Holding")) {%> selected <%}%>>Holding</option>
						<option value="Sold" <% if (stock.getSalesStatus().equalsIgnoreCase("Sold")) {%> selected <%}%>>Sold</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Remark(s) : </label>
				<div class="col-lg-5">
					<textarea disabled name="remarks" class="form-control"><% if (stock.getRemarks() != null) { %>${stock.remarks}<%}%></textarea>
				</div>
			</div>
			<input type="hidden" name="stockID" value="${stock.stockID}">
			<hr>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="StockController?action=list&itemID=${stock.itemID}" role="button">Back</a>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
