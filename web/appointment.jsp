<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="inc/head.jsp"/>

<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container ">
		<p class="navigation">
			<a href="LandingController">Home</a>
			 > 
			<a href="categories?action=list&cid=22">Recycle Toys</a>	
			 > Recycle Toy Application
		</p>
		<h2 class="new-sec-title">Toy Collection Appointment</h2>



		<form action="#" class="registration-form horizontal-form">
			<fieldset class="registration-border">
				<legend class="registration-border">Appointment Detail</legend>
				<div class="form-group row">
					<p class="require-text2">* Please bring this form and your toy to our office for the selling approval.</p>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">Appointment Number:</label>
					<div class="col-sm-8 data">
						Appointment Number
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Appointment Period:</label>
					<div class="col-sm-8 data">
						Appointment Period
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Office Hour:</label>
					<div class="col-sm-8 data">
						09:00am to 07:00 pm
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Location:</label>
					<div class="col-sm-8 data">
						Unit G, Floor 2,<br>Silverfield Industrial Building,<br>123-188 Tai Lin Pai Road,<br>sKwai Chung, N.T., Hong Kong
					</div>
				</div>
			</fieldset>

			<fieldset class="registration-border">
				<legend class="registration-border">Applicant Detail</legend>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">Member Id:</label>
					<div class="col-sm-8 data">
						Member Id
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Member Name:</label>
					<div class="col-sm-8 data">
						Member Name
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Contact Number:</label>
					<div class="col-sm-8 data">
						Contact Number
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Recycle Toy Id:</label>
					<div class="col-sm-8 data">
						Recycle Toy Id
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Toy Name:</label>
					<div class="col-sm-8 data">
						Toy Name
					</div>
				</div>
			</fieldset>

			<div class="row pull-right ">
				<input type="submit" value="Finish" class="btn btn-primary btn-lg reg-btn">          
			</div>
		</form>

	</div>
</div>
<!--Main Content End--> 

<jsp:include page="inc/foot.jsp"/>