<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="inc/head.jsp"/>  
<jsp:useBean id="user" type="model.User" scope="request"/>

<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> My Account
		</p>
		<h2 class="new-sec-title">Payment Method (Recycle Toy Selling)</h2>
		<div class="category-form">

			<div class="col-sm-3">
				<p ></p>
				<form action="#" class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label">My Account</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="profile">Account Profile</a><br>
								<a href="payment">Payment Method</a><br>
								<a href="order">Order History</a><br>
								<a href="recycle">Recycle Toy</a>
							</p>
						</div>
					</fieldset>
				</form>
			</div>

			<div class="col-sm-9">
				<p class="require-text2" style="margin-top: 10px;">* The bank account information is only shared when the buyer checks out, to compete the payment.</p>
				<form action="payment" method="POST" class="registration-form horizontal-form" style="width: 100%">
					<fieldset class="account-profile-border registration-border">
						<legend class="registration-border" >Payment Method</legend>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-4 form-control-label">Bank Name:</label>

							<label value="<%= user.getDefaultBankName()%>"></label>

							<div class="col-sm-8">
								<select id="bankName" class="form-control" name="bankName"> 
									<option value="Bank of China (Hong Kong)" <% if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Bank of China (Hong Kong)"))) {%> selected <% }  %>>Bank of China (Hong Kong)</option>
									<option value="Bank of East Asia" <% if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Bank of East Asia"))) {%> selected <% }  %>>Bank of East Asia</option>
									<option value="Hang Seng Bank" <% if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Hang Seng Bank"))) {%> selected <% }  %>>Hang Seng Bank</option>
									<option value="Hongkong and Shanghai Banking Corporation" <% if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Hongkong and Shanghai Banking Corporation"))) {%> selected <% }  %>>Hongkong and Shanghai Banking Corporation</option>
									<option value="Industrial and Commercial Bank of China (Asia)" <% if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Industrial and Commercial Bank of China (Asia)"))) {%> selected <% }  %>>Industrial and Commercial Bank of China (Asia)</option>
									<option value="Standard Chartered Bank (Hong Kong)" <%if ((user.getDefaultBankName() != null) && (user.getDefaultBankName().equalsIgnoreCase("Standard Chartered Bank (Hong Kong)"))) {%> selected <% }%>>Standard Chartered Bank (Hong Kong)</option>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label">Account Holder:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputEmail3"  value="<%= user.getDefaultAccountHolder()%>"  name="defaultAccountHolder" placeholder="Account Holder"style="font-weight: normal">
							</div>
						</div>

						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label">Account Number:</label>
							<div class="col-sm-2"style="font-weight: normal">
								<c:set var="msg" value="<%= user.getDefaultAccountNumber()%>"/>
								<c:set var="msg2" value= "${fn:substring(msg,0,4)}"/>
								<c:set var="msg3" value= "${fn:substring(msg,4,8)}"/>
								<c:set var="msg4" value= "${fn:substring(msg,8,12)}"/>
								<input type="text" class="form-control col-sm-4" id="inputEmail3" maxlength="4" value="${msg2}"  name="msg2">
							</div>
							<div class="col-sm-2"style="font-weight: normal">  
								<input type="text" class="form-control col-sm-4" id="inputEmail3" maxlength="4" value="${msg3}" name="msg3">
							</div>

							<div class="col-sm-2"style="font-weight: normal">  
								<input type="text" class="form-control col-sm-4" id="inputEmail3" maxlength="4" value="${msg4}" name="msg4">
							</div>
						</div>

					</fieldset>
					<input type="hidden" name="userID" value="<%= user.getUserID()%>">
					<input type="hidden" name="loginEmail" value="<%= user.getLoginEmail()%>">
					<input type="hidden" name="pwd" value="<%= user.getPassword()%>">
					<input type="hidden" name="lastName" value="<%= user.getLastName()%>">
					<input type="hidden" name="firstName" value="<%= user.getFirstName()%>">
					<input type="hidden" name="gender" value="<%= user.getGender()%>">
					<input type="hidden" name="contactNumber" value="<%= user.getContactNumber()%>">
					<input type="hidden" name="shippingAddress1" value="<%= user.getShippingAddress1()%>">
					<input type="hidden" name="shippingAddress2" value="<%= user.getShippingAddress2()%>">

					<div class="row pull-right "> 
						<input type="reset" value="Cancel" class="btn btn-lg reg-btn" style="font-size: 14px">
						<input type="submit" value="Save" class="btn btn-primary btn-lg reg-btn" style="font-size: 14px">          
					</div>
				</form>
			</div>  

		</div>
	</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>  