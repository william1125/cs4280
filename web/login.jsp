<jsp:include page="inc/head.jsp"/>
<!--Main Content Start-->
  <div class="cp-page-content background">
    <div class="container">
      <p class="navigation">Home > Login</p>
      <h2 class="new-sec-title">Login or Registration</h2>

      <div class="login-form">
        <form action="" method="" class="col-sm-6 horizontal-form">
          <fieldset class="login-border">
            <label class="login-form-heading form-control-label">NEW CUSTOMER</label>
            <div class="login-inner-form">
              <p class="form-text reg-form-text">By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
            </div>  
            <div class="row pull-right login-form-base">
                <a href="registration.jsp" class="btn btn-primary reg-btn">register</a>        
            </div>                 
          </fieldset>
        </form>

        <form action="login" method="POST" class="col-sm-6 horizontal-form pull-right">
          <fieldset class=" login-border">
            <label class="login-form-heading form-control-label">REGISTERED CUSTOMER</label>
            <div class="login-inner-form">
              <p class="form-text">If you have an account with us, please log in.<br></p>
              <label for="inputEmail3" class="login-form-content form-control-label">Email Address</label>
              <input type="email" class="form-control" name="loginEmail" placeholder="Email Address" required>
              <label for="inputEmail3" class="login-form-content form-control-label "><br>Password</label>
              <input type="password" class="form-control" name="password" placeholder="Password" required>
              <p><% if(request.getParameter("error")!=null){%><%= request.getParameter("error") %><%}%><br></p>
            </div>  
            <div class="row pull-right login-form-base">
              <input type="submit" value="Login" class="btn btn-primary reg-btn">          
            </div>                 
          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <!--Main Content End--> 
  <jsp:include page="inc/foot.jsp"/>