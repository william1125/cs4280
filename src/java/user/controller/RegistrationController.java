/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.User;

/**
 *
 * @author KaYee
 */
public class RegistrationController extends HttpServlet {
    private static String LAND = "registration.jsp";
    private static String INDEX = "index.jsp";
    private UserDao userDAO;
    private CategoryDao categoryDAO;
    
    public RegistrationController(){
        super();
        userDAO = new UserDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
            forward = LAND;
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = new User();
        user.setLoginEmail(request.getParameter("loginEmail"));
        user.setLastName(request.getParameter("lastName"));
        user.setFirstName(request.getParameter("firstName"));
        user.setGender(request.getParameter("gender"));
        user.setContactNumber(Integer.parseInt(request.getParameter("contactNumber")));
        user.setPassword(request.getParameter("password"));
        user.setDefaultBankName(request.getParameter("defaultBankName"));
       user.setDefaultAccountNumber(request.getParameter("defaultAccountNumber1")+request.getParameter("defaultAccountNumber2")+request.getParameter("defaultAccountNumber3"));
        user.setDefaultAccountHolder(request.getParameter("defaultAccountHolder"));
        user.setShippingAddress1(request.getParameter("shippingAddress1"));
        user.setShippingAddress2(request.getParameter("shippingAddress2"));
        user.setIsAdmin(0);
        String userID = request.getParameter("userID");
	   
            System.out.println("---POST: Insert---");
            userDAO.addUser(user);

            response.sendRedirect("LandingController");
    }
}
