/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.User;
import util.DbUtil;

/**
 *
 * @author wnpoon4
 */
public class UserDao {
    private Connection connection;
    
    public UserDao(){
        connection = DbUtil.getConnection();
    }

    public void addUser(User user) {
        try{
            String sql = "insert into [USER] (LoginEmail, LastName, FirstName, Gender, ContactNumber, Password, DefaultBankName, DefaultAccountNumber, DefaultAccountHolder, ShippingAddress1, ShippingAddress2, IsAdmin) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getLoginEmail());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getGender());
            preparedStatement.setInt(5, user.getContactNumber());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setString(7, user.getDefaultBankName());
            preparedStatement.setString(8, user.getDefaultAccountNumber());
            preparedStatement.setString(9, user.getDefaultAccountHolder());
            preparedStatement.setString(10, user.getShippingAddress1());
            preparedStatement.setString(11, user.getShippingAddress2());
            preparedStatement.setInt(12, user.getIsAdmin());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("addUser SQL Exception: "+ex.getMessage());        
        }
    } 
    
    public void deleteUser(int userID) {
        try {
            String sql = "delete from [USER] where UserID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
    }

    public void updateUser(User user) {
        try {
            String sql = "update [USER] set LoginEmail=?, LastName=?, FirstName=?, Gender=?, ContactNumber=?, Password=?, DefaultBankName=?, DefaultAccountNumber=?, DefaultAccountHolder=?, ShippingAddress1=?, ShippingAddress2=?, IsAdmin=? where UserID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getLoginEmail());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getGender());
            preparedStatement.setInt(5, user.getContactNumber());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setString(7, user.getDefaultBankName());
            preparedStatement.setString(8, user.getDefaultAccountNumber());
            preparedStatement.setString(9, user.getDefaultAccountHolder());
            preparedStatement.setString(10, user.getShippingAddress1());
            preparedStatement.setString(11, user.getShippingAddress2());
            preparedStatement.setInt(12, user.getIsAdmin());
            preparedStatement.setInt(13, user.getUserID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
    }
    public List<User> getAllUsers(){
        List<User> users = new ArrayList<User>();
        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from [USER]");
            while(rs.next()){
                User user = new User();
                user.setUserID(rs.getInt("UserID"));
                user.setLoginEmail(rs.getString("LoginEmail"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setGender(rs.getString("Gender"));
                user.setContactNumber(rs.getInt("ContactNumber"));
                user.setPassword(rs.getString("Password"));
                user.setDefaultBankName(rs.getString("DefaultBankName"));
                user.setDefaultAccountNumber(rs.getString("DefaultAccountNumber"));
                user.setDefaultAccountHolder(rs.getString("DefaultAccountHolder"));
                user.setShippingAddress1(rs.getString("ShippingAddress1"));
                user.setShippingAddress2(rs.getString("ShippingAddress2"));
                user.setIsAdmin(rs.getInt("IsAdmin"));
                users.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
        return users;
    }

    public User getUserById(int userID){
        User user = new User();
        try{
            String sql = "select * from [USER] where UserID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()){
                user.setUserID(rs.getInt("UserID"));
                user.setLoginEmail(rs.getString("LoginEmail"));
                user.setLastName(rs.getString("LastName"));
                user.setFirstName(rs.getString("FirstName"));
                user.setGender(rs.getString("Gender"));
                user.setContactNumber(rs.getInt("ContactNumber"));
                user.setPassword(rs.getString("Password"));
                user.setDefaultBankName(rs.getString("DefaultBankName"));
                user.setDefaultAccountNumber(rs.getString("DefaultAccountNumber"));
                user.setDefaultAccountHolder(rs.getString("DefaultAccountHolder"));
                user.setShippingAddress1(rs.getString("ShippingAddress1"));
                user.setShippingAddress2(rs.getString("ShippingAddress2"));
                user.setIsAdmin(rs.getInt("IsAdmin"));
            }
        } catch (SQLException ex) {
            System.out.println("SQL Exception: "+ex.getMessage());
        }
        return user;
    }
}
