<%@page import="java.util.ArrayList"%>
<%@page import="model.Category"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">

<div class="container-fluid">
<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">
					   Product Category
			</h3>
			<ol class="breadcrumb">
				
				<li class="active">
					<i class="fa fa-book"></i> Product Category
				 </li>
			</ol>
		</div>
	</div>

<p>
	<a href="CategoryController?action=insert" class="btn btn-primary fa fa-plus">     Add Category</a>
	<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
</p>
<div class= table-responsive">
	<table class="table table-striped table-hover">
	<thead class="table-head">
            <tr>
                <th>ID</th>
                <th>Category Name</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <%
                ArrayList<Category> categories = (ArrayList<Category>)request.getAttribute("categories");
                for(Category category: categories) {
            %>
                <tr>
                    <td><%= category.getCategoryID() %></td>
                    <td><%= category.getCategoryName() %></td>
                    <td>
                 
		<a href="CategoryController?action=edit&categoryID=<%= category.getCategoryID()%>" class="btn btn-info fa fa-pencil-square-o"></a>
   
                    </td>
                </tr>
            <%
                }
            %>
        </tbody>
    </table>
	   </div>
	   </div>
</div>
</body>
</html>