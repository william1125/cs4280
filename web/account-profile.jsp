
<jsp:include page="inc/head.jsp"/>  
<jsp:useBean id="user" type="model.User" scope="request"/>
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">
			<a href="LandingController">Home</a>
			> My Account
		</p>
		<h2 class="new-sec-title">Account Profile</h2>
		<div class="category-form">

			<div class="col-sm-3">
				<p ></p>
				<form action="profile"  method="POST"class="horizontal-form form-background">
					<fieldset class="category-border">
						<div class="category-form-heading form-control-label" style="color: #888; font-size: 14px; font-family: Arial;">My Account</div>
						<div class="category-inner-form">
							<p class="category-item-border form-text ">
								<a href="profile">Account Profile</a><br>
								<a href="payment">Payment Method</a><br>
								<a href="order">Order History</a><br>
								<a href="recycle">Recycle Toy</a>
							</p>
					</fieldset>
				</form>
			</div>

			<div class="col-sm-9">
				<form action="profile" method="POST" class="account-profile-form registration-form horizontal-form">
					<fieldset class="account-profile-border registration-border ">
						<legend class="registration-border" >Login Information</legend>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label" style="padding-bottom: 5px;padding-top:  5px;">Login Email Address:</label>
							<div class="col-sm-8 data">
								<!--<label style="padding-bottom: 5px;padding-top:  5px; font-weight: normal"><%=user.getLoginEmail()%></label>-->
								<input type="text" disabled class="form-control" id="inputPassword3" value="<%=user.getLoginEmail()%>">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-4 form-control-label">Current Password:</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="inputPassword3" placeholder="Current Password" style="font-weight: normal">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-4 form-control-label">New Password:</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="inputPassword3" placeholder="New Password" name="password" style="font-weight: normal">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-4 form-control-label">Confirm Password:</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="inputPassword3" placeholder="Confirm Password"  style="font-weight: normal">
							</div>
						</div>
					</fieldset>

					<fieldset class="account-profile-border registration-border">
						<legend class="registration-border" >Personal Information</legend>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label">Last Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputEmail3"  value="<%= user.getLastName()%>" name="lastName" placeholder="Last Name"  style="font-weight: normal">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label">First Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputEmail3"  value="<%= user.getFirstName()%>" name="firstName" placeholder="First Name"  style="font-weight: normal">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword3" class="col-sm-4 form-control-label">Gender:</label>
							<div class="col-sm-8">
								<select id="gender" class="form-control"name="gender" style="width: 100%;">
									<option value="M"<% if (user.getGender().equalsIgnoreCase("M")) {%> selected <% } %>>Male</option>
									<option value="F" <% if (user.getGender().equalsIgnoreCase("F")) {%> selected <% }%>>Female</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-4 form-control-label">Contact Number:</label>
							<div class="col-sm-8">
								<input type="number" class="form-control" id="inputEmail3" name="contactNumber"  value="<%= user.getContactNumber()%>" placeholder="Contact Number" step="1" min="1" max="99999999"  style="font-weight: normal">
							</div>
						</div>
					</fieldset>

					<fieldset class="account-profile-border registration-border">
						<legend class="registration-border" >Shipping Address</legend>
						<div class="shipping-address form-group ">
							<label for="exampleTextarea">Shipping Address 1</label>
							<textarea class="form-control"name="shippingAddress1" value="" id="exampleTextarea" rows="3"  style="font-weight: normal"><%if(user.getShippingAddress1() != null){%><%= user.getShippingAddress1()%><%}%></textarea>
						</div>
						<div class="shipping-address form-group ">
							<label for="exampleTextarea">Shipping Address 2</label>
							<textarea class="form-control" name="shippingAddress2" value="" id="exampleTextarea" rows="3"  style="font-weight: normal"><%if(user.getShippingAddress2() != null){%><%= user.getShippingAddress2()%><%}%></textarea>
						</div>
					</fieldset>

					<input type="hidden" name="userID" value="<%= user.getUserID()%>">
					<input type="hidden" name="loginEmail" value="<%= user.getLoginEmail()%>">
					<input type="hidden" name="oldpwd" value="<%= user.getPassword()%>">
					<input type="hidden" name="holder" value="<%= user.getDefaultAccountHolder() %>">
					<input type="hidden" name="account" value="<%= user.getDefaultAccountNumber() %>">
					<input type="hidden" name="bank" value="<%= user.getDefaultBankName() %>">
					<div class="row pull-right ">
						<input type="reset" value="Cancel" class="btn btn-lg reg-btn" style="font-size: 14px">
						<input type="submit" value="Save" class="btn btn-primary btn-lg reg-btn" style="font-size: 14px">          
					</div>
				</form>

			</div>  
		</div>        
	</div>
</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>  