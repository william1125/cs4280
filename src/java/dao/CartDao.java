/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cart;
import model.Order;
import util.DbUtil;

/**
 *
 * @author Nam
 */
public class CartDao {
    
    private Connection connection;
    
    public CartDao() {
        connection = DbUtil.getConnection();
    }
    
    public void addCart(Cart cart) {
        try {
            String sql = "insert into CART(UserID, ItemID, Quantity) values(?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cart.getUserID());
            preparedStatement.setInt(2, cart.getItemID());
            preparedStatement.setInt(3, cart.getQuantity());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Add Cart SQL Exception: " + ex.getMessage());
        }
    }
    
    public void deleteCart(int cartID) {
        try {
            String sql = "delete from CART where CartID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cartID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("delete SQL Exception: " + ex.getMessage());
        }
    }
    
    public void updateCart(Cart cart) {
        try {
            String sql = "update CART set UserID=?, ItemID=?, Quantity=? where CartID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cart.getUserID());
            preparedStatement.setInt(2, cart.getItemID());
            preparedStatement.setInt(3, cart.getQuantity());
            preparedStatement.setInt(4, cart.getCartID());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("updateCart SQL Exception: " + ex.getMessage());
        }
    }
    
    public ArrayList<Cart> getAllCartsByUserID(int uid) {
        ArrayList<Cart> carts = new ArrayList<Cart>();
        try {
            String sql = "select [ITEM].*, [CART].CartID, [CART].Quantity from [CART] inner join [ITEM] on [ITEM].ItemID=[CART].ItemID where [CART].UserID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, uid);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Cart cart = new Cart();
                cart.setCartID(rs.getInt("CartID"));
                cart.setUserID(rs.getInt("UserID"));
                cart.setItemID(rs.getInt("ItemID"));
                cart.setQuantity(rs.getInt("Quantity"));
                cart.setPrice(rs.getFloat("Price"));
                cart.setAvailableQuantity(rs.getInt("AvailableQuantity"));
                cart.setItemImageURL(rs.getString("ItemImageURL"));
//                cart.setItemRating(rs.getInt("ItemRating"));
                cart.setFullDescription(rs.getString("FullDescription"));
                carts.add(cart);
            }
        } catch (SQLException ex) {
            System.out.println("get all carts by user ID SQL Exception: " + ex.getMessage());
        }
        return carts;
    }
    
    public void checkOut(int userID, Date deliveryDate, int timeSlotID, String bankName, String accountNumber, String accountHolder, String shippingAddress) {
        try {
            Order order = new Order();
            order.setUserID(userID);
            order.setTimeSlotID(timeSlotID);
            order.setOrderStatus("Pending");
            order.setDeliveryDate(deliveryDate);
            order.setBankName(bankName);
            order.setAccountNumber(accountNumber);
            order.setAccountHolder(accountHolder);
            order.setShippingAddress(shippingAddress);
            OrderDao orderDAO = new OrderDao();
            int orderID = orderDAO.addOrder(order);
            System.out.println("orderID:"+orderID);
            String sql = "select [ITEM].*, [CART].CartID, [CART].Quantity from [CART] inner join [ITEM] on [ITEM].ItemID=[CART].ItemID where [CART].UserID=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();            

            while (rs.next()) {
                System.out.println("checkout added order:"+orderID);
                // hold some stocks
                sql = "select TOP "+rs.getInt("Quantity")+" * from STOCK where ItemID=? and SalesStatus='Available'";
                PreparedStatement preStmt = connection.prepareStatement(sql);
                preStmt.setInt(1, rs.getInt("ItemID"));
                ResultSet resultset = preStmt.executeQuery();
                while(resultset.next()){
                    System.out.println("loop through stock");
                    sql = "update [Stock] set OrderID=?, SalesStatus=? where StockID=?";
                    PreparedStatement preStmt1 = connection.prepareStatement(sql);
                    preStmt1.setInt(1, orderID);
                    preStmt1.setString(2, "Holding");
                    preStmt1.setInt(3, resultset.getInt("StockID"));
                    preStmt1.executeUpdate();
                }
//                minus available quantity
                System.err.println("minus available quantity");
                sql = "update [ITEM] set AvailableQuantity = AvailableQuantity - "+rs.getInt("Quantity")+" where [ITEM].ItemID=?";
                PreparedStatement pstmt = connection.prepareStatement(sql);
                pstmt.setInt(1, rs.getInt("ItemID"));
                pstmt.executeUpdate();
            }
            sql = "delete from CART where UserID=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
