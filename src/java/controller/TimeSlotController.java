/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.TimeSlotDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.TimeSlot;

/**
 *
 * @author Nam
 */
public class TimeSlotController extends HttpServlet {

    private static String INSERT = "admin/insertTimeSlot.jsp";
    private static String EDIT = "admin/editTimeSlot.jsp";
    private static String LIST = "admin/listTimeSlot.jsp";
    private TimeSlotDao timeSlotDAO;

    public TimeSlotController() {
        super();
        timeSlotDAO = new TimeSlotDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        HttpSession session = request.getSession();
        if (session.getAttribute("isAdmin") == null) {
            response.sendRedirect("login");
        } else if (session.getAttribute("isAdmin").toString().equalsIgnoreCase("0")) {
            response.sendRedirect("login");
        } else {
            if (action.equalsIgnoreCase("delete")) {
                int timeSlotID = Integer.parseInt(request.getParameter("timeSlotID"));
                timeSlotDAO.deleteTimeSlot(timeSlotID);
                forward = LIST;
                request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
            } else if (action.equalsIgnoreCase("edit")) {
                forward = EDIT;
                int timeSlotID = Integer.parseInt(request.getParameter("timeSlotID"));
                TimeSlot timeSlot = timeSlotDAO.getTimeSlotById(timeSlotID);
                request.setAttribute("timeSlot", timeSlot);
            } else if (action.equalsIgnoreCase("list")) {
                forward = LIST;
                request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
            } else {
                forward = INSERT;
            }
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setFromTime(request.getParameter("fromTime"));
        timeSlot.setToTime(request.getParameter("toTime"));
        String timeSlotID = request.getParameter("timeSlotID");
        if (timeSlotID == null || timeSlotID.isEmpty()) {
            System.out.println("---POST: Insert---");
            timeSlotDAO.addTimeSlot(timeSlot);
        } else {
            System.out.println("---POST: update---");
            timeSlot.setTimeSlotID(Integer.parseInt(timeSlotID));
            timeSlotDAO.updateTimeSlot(timeSlot);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST);
        request.setAttribute("timeSlots", timeSlotDAO.getAllTimeSlots());
        view.forward(request, response);
    }
}
