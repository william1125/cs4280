/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.controller;

import dao.OrderDao;
import dao.StockDao;
import dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author KaYee
 */
public class OrderHistoryController extends HttpServlet {

	private static String LAND = "order-history.jsp";
	private static String INDEX = "index.jsp";
	private static String VIEW = "order-detail.jsp";
	private OrderDao orderDAO;
	private UserDao userDAO;
	private StockDao stockDAO;

	public OrderHistoryController() {
		super();
		orderDAO = new OrderDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		String forward = "";
		forward = LAND;
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
			response.sendRedirect("login");
		} else {
						int userID = Integer.parseInt(session.getAttribute("userid").toString());
			userDAO = new UserDao();
			User user = userDAO.getUserById(userID);
			request.setAttribute("user", user);
			String action = request.getParameter("action");
			if(action == null) action = "list";
			if(action.equalsIgnoreCase("view")){
				forward = VIEW;
				stockDAO = new StockDao();
				int orderID = Integer.parseInt(request.getParameter("orderID"));
				request.setAttribute("order", orderDAO.getOrderById(orderID));
				request.setAttribute("stocks", stockDAO.getAllStocksByOrderId(orderID));
			} else {
				forward = LAND;
				request.setAttribute("orders", orderDAO.getOrdersByUserId(userID));
			}

			RequestDispatcher view = request.getRequestDispatcher(forward);
			view.forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		   throws ServletException, IOException {
		User user = new User();
		user.setLoginEmail(request.getParameter("loginEmail"));
		user.setLastName(request.getParameter("lastName"));
		user.setFirstName(request.getParameter("firstName"));
		user.setGender(request.getParameter("gender"));
		user.setContactNumber(Integer.parseInt(request.getParameter("contactNumber")));
		if (request.getParameter("password") != null) {
			user.setPassword(request.getParameter("password"));
		} else {
			user.setPassword(request.getParameter("oldpwd"));
		}

		user.setShippingAddress1(request.getParameter("shippingAddress1"));
		user.setShippingAddress2(request.getParameter("shippingAddress2"));
		user.setIsAdmin(0);
		String userID = request.getParameter("userID");

		System.out.println("---POST: update---");
		user.setUserID(Integer.parseInt(userID));
		userDAO.updateUser(user);

		user = userDAO.getUserById(Integer.parseInt(userID));
		request.setAttribute("user", user);
		RequestDispatcher view = request.getRequestDispatcher(LAND);
		view.forward(request, response);

	}
}
