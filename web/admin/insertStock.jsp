<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Add Product's Stock
				</h3>
				<ol class="breadcrumb">

					<li class="active">
						<i class="fa fa-list"></i> <a href="ItemController?action=list">Product</a>
					</li>
					<li class="active">
						<i class="fa fa-list-ol"></i> <a href="StockController?action=list&itemID=${itemID}">Product's Stock</a>
					</li>
					<li class="active">
						<i class="fa fa-check-square-o"></i> Add Product's Stock
					</li>
				</ol>
			</div>
		</div>
		<form method="POST" action='StockController' class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Stock Amount : </label>
				<div class="col-lg-5">
					<input type="number" name="amount" required min="1" required class="form-control">
				</div>
			</div>
			<input type="hidden" name="itemID" value="${itemID}">
			<hr>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-5">
					<a class="btn btn-default col-lg-3 save-btn" href="StockController?action=list&itemID=${itemID}" role="button">Cancel</a>
					<input type="submit" value="Add" class="btn btn-primary col-lg-3 save-btn"/>

				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>
