<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<%--<jsp:useBean id="category" type="model.Category" scope="request"/>--%>
	<section class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header ">
					Edit Category
				</h3>
				<ol class="breadcrumb">

					<li class="active">
						<i class="fa fa-book"></i> <a href="CategoryController?action=list">Product Category</a>
					</li>
					<li class="active">
						<i class="fa fa-pencil-square-o"></i> Edit Category
					</li>
				</ol>
			</div>
		</div>
		<form method="POST" action='CategoryController' class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-lg-4 insert">Category Name : </label>
				<div class="col-lg-5">
					<input name="categoryName" type="text" class="form-control" value="${category.categoryName}">
				</div>
			</div>
			<input name="categoryID" type="hidden" value="${category.categoryID}">
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-6">
					<a class="btn btn-default col-lg-3 save-btn" href="CategoryController?action=list" role="button">Cancel</a>
					<input type="submit" value="Save" class="btn btn-primary col-lg-3 save-btn"/>
				</div>
			</div>
		</form>
	</section>
</div>
</body>
</html>

