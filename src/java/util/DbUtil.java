/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Nam
 */
public class DbUtil {
    private static Connection connection = null;
    
    public static Connection getConnection() {
        if(connection != null)
            return connection;
        else {
            try {
                String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                String url = "jdbc:sqlserver://w2ksa.cs.cityu.edu.hk:1433;databaseName=aiad009_db";
                String user = "aiad009";
                String password = "aiad009";
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                System.out.println("Class Not Found: "+e.getMessage());
            } catch (SQLException e) {
                System.out.println("SQL Exception: "+e.getMessage());
            }   
            return connection;
        }
        
    }
}
