/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nam
 */
public class Cart {
    private int CartID;
    private int UserID;
    private int ItemID;
    private int Quantity;
    private int AvailableQuantity;
    private float Price;
    private String ItemImageURL;
    private int ItemRating;
    private String FullDescription;

    /**
     * @return the CartID
     */
    public int getCartID() {
        return CartID;
    }

    /**
     * @param CartID the CartID to set
     */
    public void setCartID(int CartID) {
        this.CartID = CartID;
    }

    /**
     * @return the UserID
     */
    public int getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the ItemID
     */
    public int getItemID() {
        return ItemID;
    }

    /**
     * @param ItemID the ItemID to set
     */
    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    /**
     * @return the Quantity
     */
    public int getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity the Quantity to set
     */
    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    /**
     * @return the Price
     */
    public float getPrice() {
        return Price;
    }

    /**
     * @param Price the Price to set
     */
    public void setPrice(float Price) {
        this.Price = Price;
    }

    /**
     * @return the ItemImageURL
     */
    public String getItemImageURL() {
        return ItemImageURL;
    }

    /**
     * @param ItemImageURL the ItemImageURL to set
     */
    public void setItemImageURL(String ItemImageURL) {
        this.ItemImageURL = ItemImageURL;
    }

    /**
     * @return the ItemRating
     */
    public int getItemRating() {
        return ItemRating;
    }

    /**
     * @param ItemRating the ItemRating to set
     */
    public void setItemRating(int ItemRating) {
        this.ItemRating = ItemRating;
    }

    /**
     * @return the FullDescription
     */
    public String getFullDescription() {
        return FullDescription;
    }

    /**
     * @param FullDescription the FullDescription to set
     */
    public void setFullDescription(String FullDescription) {
        this.FullDescription = FullDescription;
    }

    /**
     * @return the AvailableQuantity
     */
    public int getAvailableQuantity() {
        return AvailableQuantity;
    }

    /**
     * @param AvailableQuantity the AvailableQuantity to set
     */
    public void setAvailableQuantity(int AvailableQuantity) {
        this.AvailableQuantity = AvailableQuantity;
    }
}
