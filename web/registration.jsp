<jsp:include page="inc/head.jsp"/>  
<!--Main Content Start-->
<div class="cp-page-content background">
	<div class="container">
		<p class="navigation">Home > Registration</p>
		<h2 class="new-sec-title">Registration Form</h2>

		<p class="require-text">* Required Field</p>

		<form action="registration" method="POST" class="registration-form horizontal-form">
			<fieldset class="registration-border">
				<legend class="registration-border" >Login Information</legend>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">*Login Email Address:</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" id="inputEmail3" name="loginEmail" placeholder="Login Email Address" required style="font-weight: normal">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">*Password:</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password" required style="font-weight: normal">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">*Confirm Password:</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Confirm Password" required style="font-weight: normal">
					</div>
				</div>
			</fieldset>

			<fieldset class="registration-border">
				<legend class="registration-border" >Personal Information</legend>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">*Last Name:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inputEmail3" name="lastName" placeholder="Last Name" required style="font-weight: normal">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">*First Name:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inputEmail3" name="firstName" placeholder="First Name" required style="font-weight: normal">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">*Gender:</label>
					<div class="col-sm-8">
						<select id="gender" class="form-control" name="gender" style="font-weight: normal; width: 100%">
							<option value="M">Male</option>
							<option value="F">Female</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">*Contact Number:</label>
					<div class="col-sm-8">
						<input type="number" class="form-control" id="inputEmail3" name="contactNumber" placeholder="Contact Number" step="1" min="1" max="99999999" required style="font-weight: normal">
					</div>
				</div>
			</fieldset>

			<fieldset class=" registration-border">
				<legend class="registration-border" >Shipping Address</legend>
				<div class="shipping-address form-group ">
					<label for="exampleTextarea"name="shippingAddress1">Shipping Address 1</label>
					<textarea class="form-control" id="exampleTextarea" name="shippingAddress1" rows="3"  style="font-weight: normal; width: 100%"></textarea>
				</div>
				<div class="shipping-address form-group ">
					<label for="exampleTextarea"name="shippingAddress2">Shipping Address 2</label>
					<textarea class="form-control" id="exampleTextarea" name="shippingAddress2" rows="3"  style="font-weight: normal;width: 100%"></textarea>
				</div>
			</fieldset>

			<fieldset class="registration-border">
				<legend class="registration-border" >Payment Method</legend>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-4 form-control-label">Bank Name:</label>
					<div class="col-sm-8">
						<select id="gender" class="form-control"name="defaultBankName"style="font-weight: normal;width: 100%">
							<option>Bank of China (Hong Kong)</option>
							<option>Bank of East Asia</option>
							<option>Hang Seng Bank</option>
							<option>Hongkong and Shanghai Banking Corporation</option>
							<option>Industrial and Commercial Bank of China (Asia)</option>
							<option>Standard Chartered Bank (Hong Kong)</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">Account Holder:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="inputEmail3" name="defaultAccountHolder" placeholder="Account Holder"style="font-weight: normal">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-4 form-control-label">Account Number:</label>
					<div class="col-sm-2">
						<input type="text" class="form-control col-sm-4" id="inputEmail3" name="defaultAccountNumber1" maxlength="4"style="font-weight: normal">
					</div>

					<div class="col-sm-2">  
						<input type="text" class="form-control col-sm-4" id="inputEmail3" name="defaultAccountNumber2"  maxlength="4"style="font-weight: normal">
					</div>

					<div class="col-sm-2">  
						<input type="text" class="form-control col-sm-4" id="inputEmail3" name="defaultAccountNumber3" maxlength="4"style="font-weight: normal">
					</div>
				</div>
			</fieldset>
			<div class="row pull-right ">
				<input type="reset" value="Cancel" class="btn btn-lg reg-btn" style="font-size: 14px">
				<input type="submit" value="Submit" class="btn btn-primary btn-lg reg-btn"style="font-size: 14px">    

			</div>
		</form>

	</div>
</div>
<!--Main Content End--> 
<jsp:include page="inc/foot.jsp"/>  