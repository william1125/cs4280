/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nam
 */
public class TimeSlot {
    private int TimeSlotID;
    private String FromTime;
    private String ToTime;

    /**
     * @return the TimeSlotID
     */
    public int getTimeSlotID() {
        return TimeSlotID;
    }

    /**
     * @param TimeSlotID the TimeSlotID to set
     */
    public void setTimeSlotID(int TimeSlotID) {
        this.TimeSlotID = TimeSlotID;
    }

    /**
     * @return the FromTime
     */
    public String getFromTime() {
        return FromTime;
    }

    /**
     * @param FromTime the FromTime to set
     */
    public void setFromTime(String FromTime) {
        this.FromTime = FromTime;
    }

    /**
     * @return the ToTime
     */
    public String getToTime() {
        return ToTime;
    }

    /**
     * @param ToTime the ToTime to set
     */
    public void setToTime(String ToTime) {
        this.ToTime = ToTime;
    }
}
