<%@page import="java.util.ArrayList"%>
<%@page import="model.Item"%>
<jsp:include page="../inc/header.jsp"/>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Product
				</h3>
				<ol class="breadcrumb">
					
					<li class="active">
						<i class="fa fa-list"></i> Product
					</li>
				</ol>
			</div>
		</div>

		<p><a href="ItemController?action=insert" class="btn btn-primary fa fa-plus">Add Product</a></p>

		<a  class="pull-right btn btn-info fa fa-pencil-square-o">     Edit</a>
		<a class="pull-right btn btn-success fa fa-search-plus">     View</a>
		<a class="pull-right btn btn-warning fa fa-comments">     Product's Stock</a>
		<div class= table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-head">
					<tr>
						<th>ID</th>
						<th>Product Type</th>
						<th>Toy Name</th>
						<th>Selling Price</th>
						<th>Available Quantity</th>
						<th>State</th>
						<th>Created Date</th>

						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<%
						ArrayList<Item> items = (ArrayList<Item>) request.getAttribute("items");
						for (Item item : items) {
					%>
					<tr>
						<td><%= item.getItemID()%></td>
						<td><%= item.getItemType()%></td>
						<td><%= item.getItemName()%></td>
						<td><%= item.getPrice()%></td>
						<td><%= item.getAvailableQuantity()%></td>
						<td><%= item.getState()%></td>
						<td><%= item.getCreatedDate()%></td>

						<td>
							<a href="StockController?action=list&itemID=<%= item.getItemID()%>" class="btn btn-warning fa fa-comments"></a>
							<a href="ItemController?action=view&itemID=<%= item.getItemID()%>" class="btn btn-success fa fa-search-plus"></a>
							<a href="ItemController?action=edit&itemID=<%= item.getItemID()%>" class="btn btn-info fa fa-pencil-square-o"></a>
						</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>